import sys
import pandas as pd
import numpy as np
import math

pd.set_option("display.precision", 11)

df_Cartesian_reference = pd.read_csv("tmp_datafiles/pnm_reference_pressures_for_table_pnm.csv")
df_Cartesian = pd.read_csv("tmp_datafiles/pnm_unrefined_pressures_for_table_pnm.csv")
df_quadtree = pd.read_csv("tmp_datafiles/pnm_refined_pressures_for_table_pnm.csv")

def latex_float(f):
    float_str = f
    if "e" in float_str:
        base, exponent = float_str.split("e")
        return r"{0}$\cdot 10^{{{1}}}$".format(base, int(exponent))
    else:
        return float_str

def myformat(f):
       if abs(f)<1e-15:
              return '{0:1.2e}'.format(0)
       else:
              return '{0:1.2e}'.format(f)

def lineContinuation(i):
    return "& " + latex_float(myformat(df_Cartesian_reference["value"][i])) + " & " + latex_float(myformat(df_Cartesian["value"][i])) + " & " + latex_float(myformat(df_quadtree["value"][i])) + " \\\\"

file1 = open(sys.argv[1], "w")
file1.write("$p\\left(x_1,y_1\\right)$"+lineContinuation(0) + '\n')
file1.write("$p\\left(x_1,y_2\\right)$"+lineContinuation(1) + '\n')
file1.write("$p\\left(x_1,y_3\\right)$"+lineContinuation(2) + '\n')
file1.write("$p\\left(x_2,y_1\\right)$"+lineContinuation(3) + '\n')
file1.write("$p\\left(x_2,y_2\\right)$"+lineContinuation(4) + '\n')
file1.write("$p\\left(x_2,y_3\\right)$"+lineContinuation(5) + '\n')
file1.write("$p\\left(x_3,y_1\\right)$"+lineContinuation(6) + '\n')
file1.write("$p\\left(x_3,y_2\\right)$"+lineContinuation(7) + '\n')
file1.write("$p\\left(x_3,y_3\\right)$"+lineContinuation(8)+"\\hline" + '\n')
file1.write("$\\sqrt{\\begin{aligned}&\\sum_{i=1}^3\\sum_{j=1}^{3}\\\\&\\quad\\left(p_\\text{grid}\\left(x_i,y_j\\right)-p_\\text{ reference}\\left(x_i,y_j\\right)\\right)\\end{aligned}}$ &  & "+latex_float(myformat(np.sqrt(((df_Cartesian_reference.value-df_Cartesian.value)*(df_Cartesian_reference.value-df_Cartesian.value)).sum())))+" &"+ latex_float(myformat(np.sqrt(((df_Cartesian_reference.value-df_quadtree.value)*(df_Cartesian_reference.value-df_quadtree.value)).sum())))+"\\\\\\hline")
file1.write("\n \\end{tabular}")
file1.close()
