#!/bin/bash

#dumux commit 2ec8f9159a
#dune releases/2.9
#Example of successful use: dumux1/dumux-adaptivestaggered/build-cmake/appl/mixeddimension/boundary/pnmstokes$ bash ./produce_solutionError_datafiles.sh


###### obstacles
cd 1p_1p
make test_pnm_stokes_1p
./test_pnm_stokes_1p params_uniform.input -Problem.UseScvfLineAveraging true -FreeFlow.EnableUnsymmetrizedVelocityGradient true -PNM.Grid.LowerLeft '-0.005 -0.0025' -Stokes.Problem.InletVelocity 0.001  -LinearSolver.Verbosity 2
cp test_md_boundary_pnm1p_stokes1p_stokes_midcellvelocity-00000.csv ../../../../datafiles/pnm_unrefined_velocities_stokes.csv
cp test_md_boundary_pnm1p_stokes1p_pnm_midcellvelocity-00000.csv ../../../../datafiles/pnm_unrefined_velocities_pnm.csv
cp test_md_boundary_pnm1p_stokes1p_stokes_numericalP-00000.csv ../../../../datafiles/pnm_unrefined_pressures.csv
cp test_md_boundary_pnm1p_stokes1p_pnm_numericalP-00000.csv ../../../../tmp_datafiles/pnm_unrefined_pressures_for_table_pnm.csv

./test_pnm_stokes_1p params_localRefine.input -Problem.UseScvfLineAveraging true -FreeFlow.EnableUnsymmetrizedVelocityGradient true -PNM.Grid.LowerLeft '-0.005 -0.0025' -Stokes.Problem.InletVelocity 0.001 -LinearSolver.Verbosity 2
cp test_md_boundary_pnm1p_stokes1p_pnm_midcellvelocity-00000.csv ../../../../datafiles/pnm_refined_velocities_pnm.csv
cp test_md_boundary_pnm1p_stokes1p_stokes_midcellvelocity-00000.csv ../../../../datafiles/pnm_refined_velocities_stokes.csv
cp test_md_boundary_pnm1p_stokes1p_stokes_numericalP-00000.csv ../../../../datafiles/pnm_refined_pressures.csv
cp test_md_boundary_pnm1p_stokes1p_pnm_numericalP-00000.csv ../../../../tmp_datafiles/pnm_refined_pressures_for_table_pnm.csv

cd ../1p_1p_unrefined
make test_pnm_stokes_1p_unrefined
./test_pnm_stokes_1p_unrefined params.input -Problem.UseScvfLineAveraging true -FreeFlow.EnableUnsymmetrizedVelocityGradient true -PNM.Grid.LowerLeft '-0.005 -0.0025' -Stokes.Problem.InletVelocity 0.001 -LinearSolver.Verbosity 2
cp test_md_boundary_pnm1p_stokes1p_pnm_numericalP-00000.csv ../../../../tmp_datafiles/pnm_reference_pressures_for_table_pnm.csv
cd ..
