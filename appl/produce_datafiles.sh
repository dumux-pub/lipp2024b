cd freeflow/navierstokes
bash ./produce_localTruncErrExplanation_datafiles.sh

cd ../../mixeddimension/boundary/pnmstokes/
bash ./produce_pnm_datafiles.sh
cd ../../../

python3 ./write_pnm_pore_grid.py
