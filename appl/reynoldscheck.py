import sys
import pandas as pd
import numpy as np
import math

pd.set_option("display.precision", 11)

df_Cartesian = pd.read_csv("datafiles/pnm_unrefined_velocities_pnm.csv")
df_quadtree = pd.read_csv("datafiles/pnm_refined_velocities_pnm.csv")

df_Cartesian["absValueX"] = df_Cartesian.valueX.abs()
df_Cartesian["absValueY"] = df_Cartesian.valueY.abs()

df_quadtree["absValueX"] = df_quadtree.valueX.abs()
df_quadtree["absValueY"] = df_quadtree.valueY.abs()

if ((df_Cartesian[["absValueX","absValueY"]].max(axis=1)*5e-4/1e-6).max() >= 1):
    print("ERROR: Maximum Reynolds Cartesian:", format((df_Cartesian[["absValueX","absValueY"]].max(axis=1)*5e-4/1e-6).max(), ".2f"), ". Should be < 1 to have the creeping-flow assumption of PNM fulfilled!")
else:
    print("Reynolds Cartesian:", format((df_Cartesian[["absValueX","absValueY"]].max(axis=1)*5e-4/1e-6).max(), ".2f"))

if ((df_quadtree[["absValueX","absValueY"]].max(axis=1)*5e-4/1e-6).max() >= 1):
    print("ERROR: Maximum Reynolds quadtree:", format((df_quadtree[["absValueX","absValueY"]].max(axis=1)*5e-4/1e-6).max(), ".2f"), ". Should be < 1 to have the creeping-flow assumption of PNM fulfilled!")
else:
    print("Reynolds quadtree:", format((df_quadtree[["absValueX","absValueY"]].max(axis=1)*5e-4/1e-6).max(), ".2f"))
