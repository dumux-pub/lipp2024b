#!/bin/bash
mkdir -p datafiles
mkdir -p tmp_datafiles

touch tmp_datafiles/fileproduction.log
bash ./produce_datafiles.sh > tmp_datafiles/fileproduction.log

cd datafiles
bash ../sort_datafiles.sh
cd ..

cd tmp_datafiles
bash ../sort_tmp_datafiles.sh
cd ..

bash ./produce_texfiles.sh
