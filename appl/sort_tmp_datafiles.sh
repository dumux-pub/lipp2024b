python3 ../sort_poreValues.py pnm_reference_pressures_for_table_pnm.csv
python3 ../sort_poreValues.py pnm_unrefined_pressures_for_table_pnm.csv
python3 ../sort_poreValues.py pnm_refined_pressures_for_table_pnm.csv
python3 ../sort.py solErr_donea_U_10x10_projCoarseOnCVOptD_100x100unrefined.csv
python3 ../sort.py solErr_donea_U_10x10_projCVOptDOnCoarse_100x100refined.csv
python3 ../sort.py solErr_donea_Uexact_10x10_projCoarseOnCVOptD_100x100unrefined.csv
python3 ../sort.py solErr_donea_Uexact_10x10_projCVOptDOnCoarse_100x100refined.csv
