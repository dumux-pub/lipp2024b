import sys
import pandas as pd

pd.set_option("display.precision", 17)

# DataFrame to read our input CS file
dataFrame = pd.read_csv(sys.argv[1])

dataFrame.drop_duplicates(inplace=True)

# sorting according to multiple columns
dataFrame.sort_values(["x","y"],axis=0, ascending=True,inplace=True,na_position='first')

dataFrame.to_csv(sys.argv[1], index=False)
