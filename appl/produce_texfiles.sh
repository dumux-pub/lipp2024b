#Norms
python3 increase_decrease_localTruncErr.py datafiles/localTruncErrExplanation_MomX_projCoarseOnCVOptionD_WithConserv.csv datafiles/localTruncErrExplanation_MomX_projectionCoarseOnCVOptionD.csv "datafiles/Fig11aL1norm.tex" "datafiles/Fig11bL1norm.tex"
python3 increase_decrease_localTruncErr.py datafiles/localTruncErrExplanation_MomX_projCoarseOnCVOptionD_WithConserv_0307.csv datafiles/localTruncErrExplanation_MomX_projectionCoarseOnCVOptionD_0307.csv "datafiles/Fig12aL1norm.tex" "datafiles/Fig12bL1norm.tex"
python3 increase_decrease_localTruncErr.py datafiles/localTruncErrExplanation_MomX_projCoarseOnCVOptionD_WithConserv_rightFine.csv datafiles/localTruncErrExplanation_MomX_projectionCoarseOnCVOptionD_rightFine.csv "datafiles/Fig13aL1norm.tex" "datafiles/Fig13bL1norm.tex"
python3 increase_decrease_localTruncErr.py datafiles/localTruncErrExplanation_MomX_projCoarseOnCVOptionD_WithConserv_100x100.csv datafiles/localTruncErrExplanation_MomX_projectionCoarseOnCVOptionD_100x100.csv "datafiles/Fig14aL1norm.tex" "datafiles/Fig14bL1norm.tex"
python3 increase_decrease_localTruncErr.py datafiles/localTruncErrExplanation_MomX_projCoarseOnCVOptionD_WithConserv_gauss.csv datafiles/localTruncErrExplanation_MomX_projectionCoarseOnCVOptionD_gauss.csv "datafiles/Fig15aL1norm.tex" "datafiles/Fig15bL1norm.tex"
python3 increase_decrease_solErr.py datafiles/solErr_donea_U_10x10_projCVOptDOnCoarse_0406refined.csv datafiles/solErr_donea_U_10x10_projCoarseOnCVOptD_0406unrefined.csv datafiles/solErr_donea_Uexact_10x10_projCVOptDOnCoarse_0406refined.csv datafiles/solErr_donea_Uexact_10x10_projCoarseOnCVOptD_0406unrefined.csv "datafiles/Fig17aL1norm.tex" "datafiles/Fig17bL1norm.tex"
python3 increase_decrease_solErr.py tmp_datafiles/solErr_donea_U_10x10_projCVOptDOnCoarse_100x100refined.csv tmp_datafiles/solErr_donea_U_10x10_projCoarseOnCVOptD_100x100unrefined.csv tmp_datafiles/solErr_donea_Uexact_10x10_projCVOptDOnCoarse_100x100refined.csv tmp_datafiles/solErr_donea_Uexact_10x10_projCoarseOnCVOptD_100x100unrefined.csv "datafiles/100x100_decrease_L1norm.tex" "datafiles/100x100_increase_L1norm.tex"

#Tab. 1
python3 Tab_1_and_2.py ' L1LocalTruncErr(p)' ' L1LocalTruncErr(u)' ' L1LocalTruncErr(v)' '\localTruncationErrorSymbol_{\text{ma}}' '\localTruncationErrorSymbol_{\text{mo},x}' '\localTruncationErrorSymbol_{\text{mo},y}' "datafiles/Tab1.tex"

#Tab. 2
python3 Tab_1_and_2.py ' L1Abs(p)' ' L1Abs(u)' ' L1Abs(v)' '\localSolutionErrorSymbol\left(p\right)' '\localSolutionErrorSymbol\left(u\right)' '\localSolutionErrorSymbol\left(v\right)' "datafiles/Tab2.tex"

#Tab. 3
python3 Tab_3.py "datafiles/Tab3.tex"

#Reynold check for Sec 3.3.2 Improved approximation of quantities of interest
python3 reynoldscheck.py
