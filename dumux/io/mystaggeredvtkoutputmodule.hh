// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \ingroup InputOutput
 * \brief A VTK output module to simplify writing dumux simulation data to VTK format. Specialization for staggered grids with dofs on faces.
 */
#ifndef DUMUX_MY_STAGGERED_VTK_OUTPUT_MODULE_HH
#define DUMUX_MY_STAGGERED_VTK_OUTPUT_MODULE_HH

#include <dune/common/fvector.hh>

#include <dumux/io/myvtkoutputmodule.hh>
#include <dumux/io/pointcloudvtkwriter.hh>
#include <dumux/io/vtksequencewriter.hh>
#include <dumux/io/mymystaggeredvtkoutputmodule.hh>
#include <dumux/discretization/staggered/freeflow/myvelocityoutput.hh>

namespace Dumux {
/*!
 * \ingroup InputOutput
 * \brief A VTK output module to simplify writing dumux simulation data to VTK format
 *        Specialization for staggered grids with dofs on faces.
 *
 * \tparam GridVariables The grid variables
 * \tparam SolutionVector The solution vector
 */
template<class GridVariables, class SolutionVector>
class MyStaggeredVtkOutputModule
: public MyMyStaggeredVtkOutputModule<GridVariables, SolutionVector>
{
    using ParentType = MyMyStaggeredVtkOutputModule<GridVariables, SolutionVector>;
    using GridGeometry = typename GridVariables::GridGeometry;
    using GridView = typename GridGeometry::GridView;
    using Scalar = typename GridVariables::Scalar;

    enum { dim = GridView::dimension };

    using DimVector = Dune::FieldVector<Scalar, dim>;
public:

    MyStaggeredVtkOutputModule(const GridVariables& gridVariables,
                             const SolutionVector& sol,
                             const std::string& name,
                             const std::string& paramGroup = "",
                             Dune::VTK::DataMode dm = Dune::VTK::conforming,
                             bool verbose = true)
    : ParentType(gridVariables, sol, name, paramGroup, dm, verbose)
    {}

    //////////////////////////////////////////////////////////////////////////////////////////////
    //! Methods to conveniently add face variables
    //! Do not call these methods after initialization
    //////////////////////////////////////////////////////////////////////////////////////////////

    //! Add a scalar valued field
    //! \param v The field to be added
    //! \param name The name of the vtk field
    void addFaceField(const std::vector<Scalar>& v, const std::string& name)
    {
        if (v.size() == this->gridGeometry().numIntersections())
            this->faceFieldScalarDataInfo_.emplace_back(v, name);
        else
            DUNE_THROW(Dune::RangeError, "Size mismatch of added field!");
    }

    //! Add a vector valued field
    //! \param v The field to be added
    //! \param name The name of the vtk field
    void addFaceField(const std::vector<DimVector>& v, const std::string& name)
    {
        if (v.size() == this->gridGeometry().numIntersections())
            this->faceFieldVectorDataInfo_.emplace_back(v, name);
        else
            DUNE_THROW(Dune::RangeError, "Size mismatch of added field!");
    }
};

} // end namespace Dumux

#endif
