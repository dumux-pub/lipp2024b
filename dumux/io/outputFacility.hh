// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 3 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \ingroup NavierStokesModel
 */

#ifndef DUMUX_IO_OUTPUTFACILITY_HH
#define DUMUX_IO_OUTPUTFACILITY_HH

#include <dumux/common/timeloop.hh>
#include <dune/grid/io/file/vtk/vtkwriter.hh>
#include <dumux/io/grid/controlvolumegrids.hh>
#include <dumux/discretization/staggered/freeflow/mystaggeredgeometryhelper.hh>
#include <dumux/discretization/staggered/freeflow/mysubcontrolvolumeface.hh>
#include <dumux/freeflow/navierstokes/analyticalSolutionIntegration.hh>
#include <dumux/io/readdofbasedresult.hh>
#include <dumux/freeflow/navierstokes/staggered/residualcalcgenericmethods.hh>

namespace Dumux{
template<class TypeTag>
class CVOutputFacility
{
    using GridGeometry = GetPropType<TypeTag, Properties::GridGeometry>;
    using GridView = typename GridGeometry::GridView;
    using Scalar = GetPropType<TypeTag, Properties::Scalar>;
    using SolutionVector = GetPropType<TypeTag, Properties::SolutionVector>;
    using CellCenterSolutionVector = GetPropType<TypeTag, Properties::CellCenterSolutionVector>;
    using FaceSolutionVector = GetPropType<TypeTag, Properties::FaceSolutionVector>;
    using MLGTraits = typename MyFreeFlowStaggeredDefaultScvfGeometryTraits<GridView>::template ScvfMLGTraits<Scalar> ;

    using Indices = typename GetPropType<TypeTag, Properties::ModelTraits>::Indices;
    using Element = typename GridGeometry::GridView::template Codim<0>::Entity;
    using GlobalPosition = typename Element::Geometry::GlobalCoordinate;
    using Problem = GetPropType<TypeTag, Properties::Problem>;

    using HostGrid = Dune::ALUGrid<2,2,Dune::cube,Dune::nonconforming >;
    using HostGridManager = GridManager<HostGrid>;
    using CVGridGeometry = GetPropType<TypeTag, Properties::CVGridGeometry>;
    using CVElement = typename CVGridGeometry::GridView::template Codim<0>::Entity;
    using SubControlVolumeFace = typename GridGeometry::SubControlVolumeFace;

    using GridVariables = GetPropType<TypeTag, Properties::GridVariables>;
    using Assembler = StaggeredRefinedFVAssembler<TypeTag, DiffMethod::numeric>;

public:
    /*!
     * \brief The Constructor
     */
    CVOutputFacility(std::shared_ptr<const Problem> problem)
    : problem_(problem)
    {}

    void iniCVGridManagers(std::array<HostGridManager, 4>& gridManagers,
                           std::array<std::map<GlobalPosition, std::vector<unsigned int>, ContainerCmpClass<GlobalPosition>>,4>& cvCenterScvfIndicesMaps,
                           const std::array<std::string, 4>& paramGroups) const
    {
        // generate dgf files, initialize grid managers
        cvCenterScvfIndicesMaps[0] = generateCVsDGFfile<GridGeometry, GlobalPosition>(paramGroups[0], (*problem_).gridGeometry(), 0, false);
        cvCenterScvfIndicesMaps[1] = generateCVsDGFfile<GridGeometry, GlobalPosition>(paramGroups[1], (*problem_).gridGeometry(), 1, false);
        cvCenterScvfIndicesMaps[2] = generateCVsDGFfile<GridGeometry, GlobalPosition>(paramGroups[2], (*problem_).gridGeometry(), 0, true);
        cvCenterScvfIndicesMaps[3] = generateCVsDGFfile<GridGeometry, GlobalPosition>(paramGroups[3], (*problem_).gridGeometry(), 1, true);

        std::string testString = getParam<std::string>("finexCVs.Grid.File", "");
        if (testString == "")
            DUNE_THROW(Dune::InvalidStateException, "Please set finexCVs.Grid.File.");

        for (unsigned int i = 0; i < 4; ++i)
            gridManagers[i].init(paramGroups[i]);
    }

    template<class LambdaBC, class LambdaC, class LambdaD, class LambdaE, class LambdaF>
    void onCVsOutputResidualsAndPrimVars(std::shared_ptr<const GridGeometry> gridGeometry,
        std::array<HostGridManager, 4>& cvGridManagers,
        const std::array<std::map<GlobalPosition, std::vector<unsigned int>, ContainerCmpClass<GlobalPosition>>,4>& cvCenterScvfIndicesMaps,
        const std::array<std::string, 4>& paramGroups,
        const SolutionVector& sol,
        unsigned int timeIndex,
        const FaceSolutionVector& faceResidualWithAnalyticalSol,
        const std::optional<std::array<std::vector<Scalar>, 4>>& perfectInterpolationFaceResiduals,
        bool readDofBasedValues,
        const LambdaBC& fineToCoarseInside,
        const LambdaC& coarseToFineInside,
        const LambdaD& coarseToFineLeft,
        const LambdaE& coarseToFineRight,
        const LambdaF& coarseToFineRightEdgeDualGrid) const
    {
        std::array<std::vector<std::vector<Scalar>>,4> dualIndexedOutputVecsArray = {};
        std::vector<std::string> fineOutputVecNames;
        std::vector<std::string> coarseOutputVecNames;

        std::vector<FaceSolutionVector> finePrimalIndexOutputVecs;

        const FaceSolutionVector& vel = sol[GridGeometry::faceIdx()];
        finePrimalIndexOutputVecs.push_back(vel);
        fineOutputVecNames.push_back("numericalVelocity");

        const FaceSolutionVector& anaVel = problem_->getAnalyticalFaceSolutionVector();
        finePrimalIndexOutputVecs.push_back(anaVel);
        fineOutputVecNames.push_back("analyticalVelocity");
/*
        FaceSolutionVector absVelocityError;
        absVelocityError.resize((*problem_).gridGeometry().numIntersections());
        for (unsigned int i = 0; i < vel.size(); ++i)
        {
            absVelocityError[i] = std::abs(vel[i] - anaVel[i]);
        }
        finePrimalIndexOutputVecs.push_back(absVelocityError);
        fineOutputVecNames.push_back("absVelocityError");*/

        if (readDofBasedValues)
        {
            FaceSolutionVector IBAMRvel;
            IBAMRvel.resize((*problem_).gridGeometry().numFaceDofs());

            ReadDofBasedResult<TypeTag> reader(problem_);
            reader.readDofBasedResult(IBAMRvel, "velocity");
            finePrimalIndexOutputVecs.push_back(IBAMRvel);
            fineOutputVecNames.push_back("IBAMRvel");
        }

        finePrimalIndexOutputVecs.push_back(faceResidualWithAnalyticalSol);
        fineOutputVecNames.push_back("localTruncErrorMom");

        std::array<std::vector<std::vector<Scalar>>,2> fineDualIndexOutputVec = onePrimalIndexedVectorToTwoDualIndexedVectors_(finePrimalIndexOutputVecs, cvGridManagers, cvCenterScvfIndicesMaps);
        for (unsigned int i = 0; i < 2; ++i)
            for (unsigned int j = 0; j < fineDualIndexOutputVec[i].size(); ++j)
                dualIndexedOutputVecsArray[i].push_back(fineDualIndexOutputVec[i][j]);

        if (perfectInterpolationFaceResiduals)
        {
            for (unsigned int i = 0; i < 4; ++i)
                dualIndexedOutputVecsArray[i].push_back((*perfectInterpolationFaceResiduals)[i]);
            fineOutputVecNames.push_back("localTruncErrorMomWithoutInterpol");
            coarseOutputVecNames.push_back("localTruncErrorMomWithoutInterpol");
        }

        std::array<std::vector<std::string>, 4> outputVecNames;
        for (unsigned int i = 0; i < fineOutputVecNames.size(); ++i)
        {
            outputVecNames[0].push_back(fineOutputVecNames[i]+"_x");
            outputVecNames[1].push_back(fineOutputVecNames[i]+"_y");
        }
        for (unsigned int i = 0; i < coarseOutputVecNames.size(); ++i)
        {
            outputVecNames[2].push_back(coarseOutputVecNames[i]+"_x");
            outputVecNames[3].push_back(coarseOutputVecNames[i]+"_y");
        }

        for (unsigned int i = 0; i < 4; ++i)
        {
            using CVsGridView = std::decay_t<decltype(cvGridManagers[i].grid().leafGridView())>;
            Dune::VTKWriter<CVsGridView> writer (cvGridManagers[i].grid().leafGridView());
            for (unsigned int j = 0; j < dualIndexedOutputVecsArray[i].size(); ++j)
                writer.addCellData(dualIndexedOutputVecsArray[i][j], outputVecNames[i][j]);

            std::string timeIndexString = std::to_string(timeIndex);
            writer.write(paramGroups[i]+"-0000"+timeIndexString);
        }

        for (unsigned int i = 0; i < finePrimalIndexOutputVecs.size(); ++i)
        {
            this->printDualDataFiles_(outputVecNames[0][i]+ seqName_(timeIndex), finePrimalIndexOutputVecs[i],false,0, fineToCoarseInside, coarseToFineInside, coarseToFineLeft, coarseToFineRight, coarseToFineRightEdgeDualGrid, timeIndex);
            this->printDualDataFiles_(outputVecNames[1][i]+ seqName_(timeIndex), finePrimalIndexOutputVecs[i],false,1, fineToCoarseInside, coarseToFineInside, coarseToFineLeft, coarseToFineRight, coarseToFineRightEdgeDualGrid, timeIndex);
        }
    }

    void printPvdFiles(const std::array<std::string, 4>& paramGroups,
        const std::vector<std::pair<unsigned int, Scalar>> pvdFileInfos)
    {
        for (unsigned int i = 0; i < 4; ++i)
        {
            std::string name = paramGroups[i];

            std::ofstream dgfFile;
            dgfFile.open(name + ".pvd");
            dgfFile << "<?xml version=\"1.0\"?>"  << std::endl;
            dgfFile << "<VTKFile type=\"Collection\" version=\"0.1\" byte_order=\"LittleEndian\">"  << std::endl;
            dgfFile << "<Collection>" << std::endl;

            for (const auto& pvdFileInfo : pvdFileInfos)
                dgfFile << "<DataSet timestep=\"" << pvdFileInfo.second << "\" group=\"\" part=\"0\" name=\"\" file=\""<< name << "-0000" << pvdFileInfo.first << ".vtu\"/>"  << std::endl;
            dgfFile << "</Collection>" << std::endl;
            dgfFile << "</VTKFile>" << std::endl;
            dgfFile.close();
        }
    }

private:
    std::string seqName_(unsigned int count) const
    {
      std::stringstream n;
      n.fill('0');
      n << "-" << std::setw(5) << count;
      return n.str();
    }

    template<class SomeFieldType,class LambdaBC, class LambdaC, class LambdaD, class LambdaE, class LambdaF>
    void printDualDataFiles_(const std::string& nameA, const SomeFieldType& field, bool hangingNodesAreCVCenters, unsigned int dirIdx,
        const LambdaBC& fineToCoarseInside,
        const LambdaC& coarseToFineInside,
        const LambdaD& coarseToFineLeft,
        const LambdaE& coarseToFineRight,
        const LambdaF& coarseToFineRightEdgeDualGrid,
        unsigned int timeIndex) const
    {
        std::ofstream dualDataFileX;
        dualDataFileX.open(nameA + ".csv");
        dualDataFileX << "xMin" << "," << "yMin" << "," << "xMax" << "," << "yMax" << "," << "value" << std::endl;

        std::ofstream dualDataFileXCavityOnly;
        dualDataFileXCavityOnly.open(nameA + "_cavityOnly" + ".csv");
        dualDataFileXCavityOnly << "xMin" << "," << "yMin" << "," << "xMax" << "," << "yMax" << "," << "value" << std::endl;

        std::ofstream projectionCVOptionDOnCoarse;
        std::string tmpName;
        if (dirIdx == 0)
            tmpName = "projCVOptDOnCoarse_" + nameA +".csv";
        else if (dirIdx == 1)
            tmpName = "projCVOptDOnCoarse_" + nameA +".csv";
        else
            DUNE_THROW(Dune::InvalidStateException, "");

        projectionCVOptionDOnCoarse.open(tmpName);
        projectionCVOptionDOnCoarse << "xMin" << "," << "yMin" << "," << "xMax" << "," << "yMax" << "," << "dof option g" << std::endl;

        std::ofstream projectionCoarseOnCVOptionD;
        if (dirIdx == 0)
            tmpName = "projCoarseOnCVOptD_" + nameA + ".csv";
        else if (dirIdx == 1)
            tmpName = "projCoarseOnCVOptD_" + nameA + ".csv";
        else
            DUNE_THROW(Dune::InvalidStateException, "");

        projectionCoarseOnCVOptionD.open(tmpName);
        projectionCoarseOnCVOptionD << "xMin" << "," << "yMin" << "," << "xMax" << "," << "yMax" << "," << "dof option g" << std::endl;

        for (const auto& element : elements((*problem_).gridGeometry().gridView(), Dune::Partitions::interior))
        {
            auto fvGeometry = localView((*problem_).gridGeometry());
            fvGeometry.bind(element);

            for (auto&& scvf : scvfs(fvGeometry))
            {
                if (scvf.boundary())
                {
                    using Indices = typename GetPropType<TypeTag, Properties::ModelTraits>::Indices;

                    auto bcTypes = (*problem_).boundaryTypesAtPos(scvf.center());
                    if (bcTypes.isDirichlet(Indices::velocity(scvf.directionIndex())) && nameA != "numericalVelocity" && nameA != "analyticalVelocity" && nameA != "IBAMRvel") //then the output if for truncation errors which make sense on control volumes
                        continue;
                }

                const auto& corners = localCVVertices<GlobalPosition>(scvf, (*problem_).gridGeometry(), hangingNodesAreCVCenters);

                if (corners.size() != 4)
                    DUNE_THROW(Dune::InvalidStateException, "invalid state");

                std::vector<Scalar> xValues;
                std::vector<Scalar> yValues;
                for (unsigned int i=0; i < corners.size(); ++i)
                {
                    xValues.push_back(corners[i][0]);
                    yValues.push_back(corners[i][1]);
                }

                typename std::vector<Scalar>::iterator xMinIter = std::min_element(xValues.begin(), xValues.end());
                typename std::vector<Scalar>::iterator xMaxIter = std::max_element(xValues.begin(), xValues.end());
                typename std::vector<Scalar>::iterator yMinIter = std::min_element(yValues.begin(), yValues.end());
                typename std::vector<Scalar>::iterator yMaxIter = std::max_element(yValues.begin(), yValues.end());

                Scalar xMin = *xMinIter;
                Scalar xMax = *xMaxIter;
                Scalar yMin = *yMinIter;
                Scalar yMax = *yMaxIter;

                if (scvf.directionIndex()==dirIdx)
                {
                    dualDataFileX << xMin << "," << yMin << "," << xMax << "," << yMax << "," << std::setprecision(10) << field[scvf.dofIndex()] << std::endl;

                    Scalar cavityLeft = 2.1;
                    Scalar cavityRight = 2.2;
                    Scalar cavityDown = -0.1;
                    Scalar cavityUp = 0.;

                    if ( cavityLeft < scvf.center()[0] && scvf.center()[0] < cavityRight && cavityDown < scvf.center()[1] && scvf.center()[1] < cavityUp )
                        dualDataFileXCavityOnly << xMin << "," << yMin << "," << xMax << "," << yMax << "," << std::setprecision(10) << field[scvf.dofIndex()] << std::endl;
                }

                if (scvf.directionIndex()==dirIdx)
                {
                    GlobalPosition globalPos;
                    globalPos[0]=scvf.center()[0];
                    globalPos[1]=.5*(yMin+yMax);

                    projectionCVOptionDOnCoarseFunc(xMin, xMax, yMin, yMax, globalPos, projectionCVOptionDOnCoarse, dirIdx, fineToCoarseInside, field[scvf.dofIndex()]);
                    projectionCoarseOnCVOptionDFunc(xMin, xMax, yMin, yMax, globalPos, projectionCoarseOnCVOptionD, dirIdx, coarseToFineInside, coarseToFineLeft, coarseToFineRight, coarseToFineRightEdgeDualGrid, field[scvf.dofIndex()]);
                }
            }
        }

        projectionCoarseOnCVOptionD.close();
        projectionCVOptionDOnCoarse.close();
        dualDataFileX.close();
        dualDataFileXCavityOnly.close();
    }

    /*!
     * \brief scvfDofIndex from control volume
     *
     * \param cv The control volume
     */
    unsigned int scvfDofIndexFromCv_(const CVElement& cv,
        const std::map<GlobalPosition, std::vector<unsigned int>, ContainerCmpClass<GlobalPosition>>& cvCenterScvfIndicesMap) const
    {
        auto it = cvCenterScvfIndicesMap.find(cv.geometry().center());
        std::vector<unsigned int> scvfIndices = (*it).second;
        SubControlVolumeFace scvf = (*problem_).gridGeometry().scvf(scvfIndices[0]);

        return scvf.dofIndex();
    }

    /*!
     * \brief control volume index from control volume
     *
     * \param cv The control volume
     */
    unsigned int cvIndexFromCv_(const CVElement& cv,
        const HostGrid::LeafGridView& cvLeafGridView) const
    {
        return cvLeafGridView.indexSet().index(cv);
    }

    template<class OutputVecs>
    std::vector<std::vector<Scalar>> onePrimalIndexedVectorToOneDualIndexedVectorInOneDirection_(unsigned int dirIdx,
        OutputVecs& finePrimalIndexOutputVecs,
        std::array<HostGridManager, 4>& gridManagers,
        const std::array<std::map<GlobalPosition, std::vector<unsigned int>, ContainerCmpClass<GlobalPosition>>,4>& cvCenterScvfIndicesMaps) const
    {
        const HostGrid::LeafGridView& cVsLeafGridView = (gridManagers[dirIdx]).grid().leafGridView();

        std::vector<std::vector<Scalar>> modifiedfinePrimalIndexOutputVecs;
        std::vector<Scalar> dummyVec;
        dummyVec.resize(cVsLeafGridView.size(0));
        for (unsigned int i = 0; i < finePrimalIndexOutputVecs.size(); ++i)
            modifiedfinePrimalIndexOutputVecs.push_back(dummyVec);

        for (const auto& cv : elements(cVsLeafGridView))
        {
            for (unsigned int i = 0; i < finePrimalIndexOutputVecs.size(); ++i)
                modifiedfinePrimalIndexOutputVecs[i][cvIndexFromCv_(cv, cVsLeafGridView)] = finePrimalIndexOutputVecs[i][scvfDofIndexFromCv_(cv, cvCenterScvfIndicesMaps[dirIdx])];
        }

        return modifiedfinePrimalIndexOutputVecs;
    }

    template<class OutputVecs>
    std::array<std::vector<std::vector<Scalar>>,2> onePrimalIndexedVectorToTwoDualIndexedVectors_(OutputVecs& finePrimalIndexOutputVecs,
         std::array<HostGridManager, 4>& gridManagers,
        const std::array<std::map<GlobalPosition, std::vector<unsigned int>, ContainerCmpClass<GlobalPosition>>,4>& cvCenterScvfIndicesMaps) const
    {
        std::vector<std::vector<Scalar>> modifiedfinePrimalIndexOutputVecsX = onePrimalIndexedVectorToOneDualIndexedVectorInOneDirection_(0, finePrimalIndexOutputVecs, gridManagers, cvCenterScvfIndicesMaps);
        std::vector<std::vector<Scalar>> modifiedfinePrimalIndexOutputVecsY = onePrimalIndexedVectorToOneDualIndexedVectorInOneDirection_(1, finePrimalIndexOutputVecs, gridManagers, cvCenterScvfIndicesMaps);

        std::array<std::vector<std::vector<Scalar>>,2> retArray = {modifiedfinePrimalIndexOutputVecsX, modifiedfinePrimalIndexOutputVecsY};

        return retArray;
    }

    std::shared_ptr<const Problem> problem_;
    Scalar leftX_;
    Scalar rightX_;
    Scalar lowerY_;
    Scalar upperY_;
    Scalar deltaX_;
};
} // end namespace Dumux

#endif
