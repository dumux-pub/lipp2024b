// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 3 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \ingroup NavierStokesModel
 */

#ifndef DUMUX_IO_READDOFBASEDRESULT_HH
#define DUMUX_IO_READDOFBASEDRESULT_HH

#include<dumux/discretization/staggered/freeflow/mystaggeredgeometryhelper.hh>

namespace Dumux{
template<class TypeTag>
class ReadDofBasedResult
{
    using GridGeometry = GetPropType<TypeTag, Properties::GridGeometry>;
    using GridView = typename GridGeometry::GridView;
    using Scalar = GetPropType<TypeTag, Properties::Scalar>;
    using Problem = GetPropType<TypeTag, Properties::Problem>;

public:
    /*!
     * \brief The Constructor
     */
    ReadDofBasedResult(std::shared_ptr<const Problem> problem)
    : problem_(problem)
    {}

template<class Vector>
void readDofBasedResult(Vector& retVector, const std::string& quantity) const
{
    std::string paramGroup = (quantity == "pressure") ? "PressureReadIn" : "VelocityReadIn";

    std::string xFileName = getParamFromGroup<std::string>(paramGroup, "XFileName");
    std::string yFileName = getParamFromGroup<std::string>(paramGroup, "YFileName");
    std::string valueFileName = getParamFromGroup<std::string>(paramGroup, "ValueFileName");
    unsigned int numFileEntries = getParamFromGroup<unsigned int>(paramGroup, "NumFileEntries");

    std::vector<double> xPositions = read_single_column_(xFileName, numFileEntries);
    std::vector<double> yPositions = read_single_column_(yFileName, numFileEntries);
    std::vector<double> values = read_single_column_(valueFileName, numFileEntries);

    for (unsigned int i = 0; i < numFileEntries; ++i)
    {
        for (const auto& element : elements((*problem_).gridGeometry().gridView()))
        {
            auto fvGeometry = localView((*problem_).gridGeometry());
            fvGeometry.bindElement(element);

            if (quantity == "pressure")
            {
                const auto& range = scvs(fvGeometry);
                setValue_(range, retVector, xPositions, yPositions, values, i);
            }
            else
            {
                const auto& range = scvfs(fvGeometry);
                setValue_(range, retVector, xPositions, yPositions, values, i);
            }
        }
    }
}

private:
    template <class SomeType, class Vector>
    void setValue_(const SomeType& range, Vector& retVector, const std::vector<double>& xPositions, const std::vector<double>& yPositions, const std::vector<double>& values, unsigned int i) const
    {
        for (auto&& entity : range)
            if(scalarCmp(entity.center()[0],xPositions[i]) && scalarCmp(entity.center()[1], yPositions[i]))
                retVector[entity.dofIndex()] = values[i];
    }

    std::vector<double> read_single_column_ (std::string inputfile_name, int max_readin_linenumber) const
    {
        std::ifstream stream(inputfile_name.c_str());//inputfilestream (ofstrem = outputfilestrem)
        if(!stream)
        {
	    std::cout << "ERROR: inputfile could not be opened" << std::endl;
	    exit(1);
        }
        std::vector<double> Values;
        double d;
	std::string s;

	//for max_readin_linenumber
	int i=1;

        while (stream >> d) {//seems to go through everything which is separated by endl; http://stackoverflow.com/questions/7443787/using-c-ifstream-extraction-operator-to-read-formatted-data-from-a-file
	    Values.push_back(d);
	    i++;
	    if (i>max_readin_linenumber){break;}
        }//for my rhoValues it's fine if it's only considering positive values

        stream.close();

        return Values;
    }

    std::shared_ptr<const Problem> problem_;
};
} // end namespace Dumux

#endif
