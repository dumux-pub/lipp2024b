// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 3 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \ingroup PoreNetworkDiscretization
 * \brief Base class for the finite volume geometry for porenetwork models
 */
#ifndef DUMUX_DISCRETIZATION_PNM_GRID_GEOMETRY_HH
#define DUMUX_DISCRETIZATION_PNM_GRID_GEOMETRY_HH

#include <string>
#include <utility>
#include <unordered_map>
#include <functional>

#include <dune/common/exceptions.hh>
#include <dune/localfunctions/lagrange/pqkfactory.hh>

#include <dumux/discretization/method.hh>
#include <dumux/common/indextraits.hh>
#include <dumux/common/defaultmappertraits.hh>
#include <dumux/common/parameters.hh>

#include <dumux/discretization/basegridgeometry.hh>
#include <dumux/discretization/porenetwork/fvelementgeometry.hh>
#include <dumux/discretization/porenetwork/subcontrolvolume.hh>
#include <dumux/discretization/porenetwork/subcontrolvolumeface.hh>
#include <dumux/porenetworkflow/common/throatproperties.hh>
#include <dumux/porenetworkflow/common/poreproperties.hh>

namespace Dumux {

/*!
 * \ingroup PoreNetworkDiscretization
 * \brief Base class for geometry data extraction from the grid data format
 * \todo TODO different shapes for each pore/throat
 */
template<class Scalar, class GridView>
class DefaultPNMData
{
    using GridIndex = typename IndexTraits<GridView>::GridIndex;
    using SmallLocalIndex = typename IndexTraits<GridView>::SmallLocalIndex;
    using Label = std::int_least8_t;

    static const int dim = GridView::dimension;

public:

    template<class GridData>
    void update(const GridView& gridView, const GridData& gridData)
    {
        coordinationNumber_ = gridData.getCoordinationNumbers();

        const auto numThroats = gridView.size(0);
        throatRadius_.resize(numThroats);
        throatLength_.resize(numThroats);
        throatLabel_.resize(numThroats);
        throatCrossSectionalArea_.resize(numThroats);
        shapeFactor_.resize(numThroats);

        useSameShapeForAllThroats_ = true;
        considerAspectRatio_ = true;

        // first check if the same geometry shall be used for all entities ...
        if (hasParamInGroup(gridData.paramGroup(), "Grid.ThroatCrossSectionShape"))
        {
            const auto throatGeometryInput = getParamFromGroup<std::string>(gridData.paramGroup(), "Grid.ThroatCrossSectionShape");
            const auto throatGeometry = Throat::shapeFromString(throatGeometryInput);
            throatGeometry_.resize(1);
            throatGeometry_[0] = throatGeometry;

            // all geometries except rectangles feature an aspect ratio of 1
            if (throatGeometry != Throat::Shape::rectangle)
            {
                considerAspectRatio_ = false;
                throatAspectRatio_.resize(1, 1.0);
            }
            else
                throatAspectRatio_.resize(numThroats);

            std::cout << "Using '" << throatGeometryInput << "' as cross-sectional shape for all throats." << std::endl;
        }
        else // .. otherwise, get the corresponding parameter index from the grid data and resize the respective vector
        {
            std::cout << "Reading shape factors for throats from grid data." << std::endl;
            useSameShapeForAllThroats_ = false;
            throatGeometry_.resize(numThroats);
            throatAspectRatio_.resize(numThroats);
        }

        // TODO: allow for different pore geometries
        poreGeometry_.resize(1);
        poreGeometry_[0] = Pore::shapeFromString(getParamFromGroup<std::string>(gridData.paramGroup(), "Grid.PoreGeometry"));

        // get the vertex parameters
        const auto numPores = gridView.size(1);
        poreRadius_.resize(numPores);
        poreLabel_.resize(numPores);
        poreVolume_.resize(numPores);

        const auto poreRadiusIdx = gridData.parameterIndex("PoreRadius");
        const auto poreLabelIdx = gridData.parameterIndex("PoreLabel");

        // automatically determine the pore volume if not provided by the grid file
        const auto poreVolume = [&] (const auto& vertex, const auto vIdx)
        {
            static const bool gridHasPoreVolume = gridData.gridHasVertexParameter("PoreVolume");
            if (!gridHasPoreVolume)
                return Pore::volume(this->poreGeometry(), this->poreRadius(vIdx));
            else
                return gridData.parameters(vertex)[gridData.parameterIndex("PoreVolume")];
        };

        for(const auto& vertex : vertices(gridView))
        {
            const int vIdx = gridView.indexSet().index(vertex);
            const auto& params = gridData.parameters(vertex);
            poreRadius_[vIdx] = params[poreRadiusIdx];
            assert(poreRadius_[vIdx] > 0.0);
            poreVolume_[vIdx] = poreVolume(vertex, vIdx);
            poreLabel_[vIdx] = params[poreLabelIdx];
        }

        // automatically determine the throat label if not provided by the grid file
        const auto throatLabel = [&] (const auto& element)
        {
            static const bool gridHasThroatLabel = gridData.gridHasElementParameter("ThroatLabel");
            if (!gridHasThroatLabel)
                return gridData.throatLabel(element);
            else
                return static_cast<int>(gridData.parameters(element)[gridData.parameterIndex("ThroatLabel")]);
        };

        for (const auto& element : elements(gridView))
        {
            const int eIdx = gridView.indexSet().index(element);
            const auto params = gridData.parameters(element);

            static const auto throatRadiusIdx = gridData.parameterIndex("ThroatRadius");
            static const auto throatLengthIdx = gridData.parameterIndex("ThroatLength");

            throatRadius_[eIdx] = params[throatRadiusIdx];
            throatLength_[eIdx] = params[throatLengthIdx];
            throatLabel_[eIdx] = throatLabel(element);

            if (!useSameShapeForAllThroats_)
            {
                static const auto throatShapeFactorIdx = gridData.parameterIndex("ThroatShapeFactor");
                static const auto throatAreaIdx = gridData.parameterIndex("ThroatCrossSectionalArea");
                shapeFactor_[eIdx] = params[throatShapeFactorIdx];
                throatGeometry_[eIdx] = Throat::shape(shapeFactor_[eIdx]);
                throatCrossSectionalArea_[eIdx] = params[throatAreaIdx];

                // all geometries except rectangles feature an aspect ratio of 1
                if (throatGeometry_[eIdx] == Throat::Shape::rectangle)
                {
                    static const auto throatAspectRatioIdx = gridData.parameterIndex("ThroatAspectRatio");
                    throatAspectRatio_[eIdx] = params[throatAspectRatioIdx];
                }
            }
            else
            {
                // helper lambda to determine the aspect ratio for rectangular throats
                auto getAspectRatio = [&]()
                {
                    static const bool hasAspectRatio = hasParamInGroup(gridData.paramGroup(), "Grid.ThroatAspectRatio");
                    if (hasAspectRatio)
                    {
                        // If an aspect ratio for the throats is specified, make sure that the inscribed throat radius is not
                        // greater than the throat's height. If necessary, re-assign the radius accordingly.
                        static const Scalar value = getParamFromGroup<Scalar>(gridData.paramGroup(), "Grid.ThroatAspectRatio");
                        if (value >= 1.0) // inscribed radius still valid
                            return value;
                        else // inscribed radius needs to be updated
                        {
                            throatRadius_[eIdx] *= value;
                            return 1.0 / value; // make sure that the aspect ratio is greater than 1
                        }
                    }
                    else
                    {
                        // If a fixed height for the throats is specified, make sure that the inscribed throat radius is not
                        // greater than this height. If necessary, re-assign the value accordingly.
                        static const Scalar fixedHeight = getParamFromGroup<Scalar>(gridData.paramGroup(), "Grid.ThroatHeight");
                        if (fixedHeight > throatRadius_[eIdx]) // inscribed radius still valid
                            return fixedHeight / (2.0*throatRadius_[eIdx]);
                        else // inscribed radius needs to be updated
                        {
                            const Scalar oldInscribedRadius = throatRadius_[eIdx];
                            const Scalar newInscribedRadius = 0.5*fixedHeight;
                            throatRadius_[eIdx] = newInscribedRadius;
                            return oldInscribedRadius / newInscribedRadius;
                        }
                    }
                };

                const auto shape = this->throatCrossSectionShape(eIdx);
                if (shape == Throat::Shape::rectangle)
                    throatAspectRatio_[eIdx] =  getAspectRatio();

                if (shape == Throat::Shape::polygon || shape == Throat::Shape::scaleneTriangle)
                {
                    static const auto shapeFactor = getParamFromGroup<Scalar>(gridData.paramGroup(), "Grid.ThroatShapeFactor");
                    shapeFactor_[eIdx] = shapeFactor;
                }
                else
                    shapeFactor_[eIdx] = Throat::shapeFactor<Scalar>(shape, throatRadius_[eIdx], throatAspectRatio(eIdx));

                throatCrossSectionalArea_[eIdx] = Throat::totalCrossSectionalArea(shape, throatRadius_[eIdx], throatAspectRatio(eIdx));
            }

            assert(throatRadius_[eIdx] > 0.0);
            assert(throatLength_[eIdx] > 0.0);
            assert(throatCrossSectionalArea_[eIdx] > 0.0);

            static const bool addThroatVolumeToPoreVolume = getParamFromGroup<bool>(gridData.paramGroup(), "Grid.AddThroatVolumeToPoreVolume", false);
            if (addThroatVolumeToPoreVolume)
            {
                for (int vIdxLocal = 0; vIdxLocal < 2; ++vIdxLocal)
                {
                    const auto vIdx = gridView.indexSet().subIndex(element, vIdxLocal, dim);
                    poreVolume_[vIdx] += 0.5 * throatCrossSectionalArea_[eIdx] * throatLength_[eIdx];
                }
            }
        }

        // check if all throat might have the same shape in order to save some memory
        if (!useSameShapeForAllThroats_ &&
            std::adjacent_find(throatGeometry_.begin(), throatGeometry_.end(), std::not_equal_to<Throat::Shape>() ) == throatGeometry_.end())
        {
            std::cout << "All throats feature the same shape, resizing containers" << std::endl;
            useSameShapeForAllThroats_ = true;
            const Scalar shapeFactor = shapeFactor_[0];
            const auto throatGeometry = throatGeometry_[0];
            shapeFactor_.resize(1);
            throatGeometry_.resize(1);
            shapeFactor_[0] = shapeFactor;
            throatGeometry_[0] = throatGeometry;
        }

        // if none of the throats has a recatangular shape, do not consider the aspect ratio
        if (considerAspectRatio_ &&
            std::none_of(throatGeometry_.begin(), throatGeometry_.end(), [](const auto s){ return (s == Throat::Shape::rectangle); }))
        {
            considerAspectRatio_ = false;
            throatAspectRatio_.resize(1, 1.0);
        }
    }

    //! Returns the pore label (e.g. used for setting BCs)
    Label poreLabel(const GridIndex dofIdxGlobal) const
    { return poreLabel_[dofIdxGlobal]; }

    //! Returns the pore label (e.g. used for setting BCs)
    const std::vector<Label>& poreLabel() const
    { return poreLabel_; }

    //! Returns the radius of the pore
    Scalar poreRadius(const GridIndex dofIdxGlobal) const
    { return poreRadius_[dofIdxGlobal]; }

    //! Returns the volume of the pore
    Scalar poreVolume(const GridIndex dofIdxGlobal) const
    { return poreVolume_[dofIdxGlobal]; }

    //! Returns the volume of the pore
    const std::vector<Scalar>& poreVolume() const
    { return poreVolume_; }

    //! Returns the minumum pore radius
    const Scalar minPoreRadius() const
    {
        return *std::min_element(poreRadius_.begin(), poreRadius_.end());
    }

    //! Returns the maximum pore radius
    Scalar maxPoreRadius() const
    {
        return *std::max_element(poreRadius_.begin(), poreRadius_.end());
    }

    //! Returns the radius of the throat
    Scalar throatRadius(const GridIndex eIdx) const
    { return throatRadius_[eIdx]; }

    //! Returns the length of the throat
    Scalar throatLength(const GridIndex eIdx) const
    { return throatLength_[eIdx]; }

    //! Returns an index indicating if a throat is touching the domain boundary
    Label throatLabel(const GridIndex eIdx) const
    { return throatLabel_[eIdx]; }

    //! Returns the vector of throat labels
    const std::vector<Label>& throatLabel() const
    { return throatLabel_; }

    //! Returns the number of throats connected to a pore (coordination number)
    SmallLocalIndex coordinationNumber(const GridIndex dofIdxGlobal) const
    { return coordinationNumber_[dofIdxGlobal]; }

    //! Returns the vecor of coordination numbers
    const std::vector<SmallLocalIndex>& coordinationNumber() const
    { return coordinationNumber_; }

    //! the geometry / shape of the pore
    Pore::Shape poreGeometry() const //TODO
    { return poreGeometry_[0]; }

    //! Returns the throat's cross-sectional shape
    Throat::Shape throatCrossSectionShape(const GridIndex eIdx) const
    {
        return useSameShapeForAllThroats_ ? throatGeometry_[0] : throatGeometry_[eIdx];
    }

    //! Returns the throat's cross-sectional area
    Scalar throatCrossSectionalArea(const GridIndex eIdx) const
    { return throatCrossSectionalArea_[eIdx]; }

    //! Returns the throat's cross-sectional area
    const std::vector<Scalar>& throatCrossSectionalArea() const
    { return throatCrossSectionalArea_; }

    //! Returns the throat's shape factor
    Scalar shapeFactor(const GridIndex eIdx) const
    {
        return useSameShapeForAllThroats_ ? shapeFactor_[0] : shapeFactor_[eIdx];
    }

    SmallLocalIndex numCorners(const GridIndex eIdx) const
    { return Throat::numCorners(throatCrossSectionShape(eIdx)); }

    //! Returns the throat aspect ratio
    Scalar throatAspectRatio(const GridIndex eIdx) const
    { return considerAspectRatio_ ? throatAspectRatio_[eIdx] : 1.0; }

    //!  Returns the throat aspect ratio
    const std::vector<Scalar>& throatAspectRatio() const
    {
        if (considerAspectRatio_)
            return throatAspectRatio_;
        else
            DUNE_THROW(Dune::InvalidStateException, "All throats have an aspect ratio of 1.0! Create your own vector if needed.");
    }

    //! Returns whether all throats feature the same cross-sectional shape
    bool useSameShapeForAllThroats() const
    { return useSameShapeForAllThroats_; }

private:
    std::vector<Pore::Shape> poreGeometry_;
    std::vector<Throat::Shape> throatGeometry_;
    std::vector<SmallLocalIndex> coordinationNumber_;
    std::vector<Scalar> poreRadius_;
    std::vector<Scalar> poreVolume_;
    std::vector<Label> poreLabel_; // 0:no, 1:general, 2:coupling1, 3:coupling2, 4:inlet, 5:outlet
    std::vector<Label> throatLabel_; // 0:no, 1:general, 2:coupling1, 3:coupling2, 4:inlet, 5:outlet
    std::vector<Scalar> throatRadius_;
    std::vector<Scalar> throatLength_;
    std::vector<Scalar> shapeFactor_;
    std::vector<Scalar> throatCrossSectionalArea_;
    std::vector<Scalar> throatAspectRatio_;
    bool useSameShapeForAllThroats_;
    bool considerAspectRatio_;
};

/*!
 * \ingroup PoreNetworkDiscretization
 * \brief The default traits
 * \tparam the grid view type
 */
template<class GridView, class MapperTraits = DefaultMapperTraits<GridView>>
struct PNMDefaultGridGeometryTraits
: public MapperTraits
{
    using SubControlVolume = PNMSubControlVolume<GridView>;
    using SubControlVolumeFace = PNMSubControlVolumeFace<GridView>;

    template<class GridGeometry, bool enableCache>
    using LocalView = PNMFVElementGeometry<GridGeometry, enableCache>;

    using PNMData = DefaultPNMData<typename SubControlVolume::Traits::Scalar, GridView>;
};

/*!
 * \ingroup PoreNetworkDiscretization
 * \brief Base class for the finite volume geometry for porenetwork models
 * \note This class is specialized for versions with and without caching the fv geometries on the grid view
 */
template<class Scalar,
         class GridView,
         bool enableGridGeometryCache = false,
         class Traits = PNMDefaultGridGeometryTraits<GridView> >
class PNMGridGeometry;

/*!
 * \ingroup PoreNetworkDiscretization
 * \brief Base class for the finite volume geometry for porenetwork models
 * \note For caching enabled we store the fv geometries for the whole grid view which is memory intensive but faster
 */
template<class Scalar, class GV, class Traits>
class PNMGridGeometry<Scalar, GV, true, Traits>
: public BaseGridGeometry<GV, Traits>
, public Traits::PNMData
{
    using ThisType = PNMGridGeometry<Scalar, GV, true, Traits>;
    using ParentType = BaseGridGeometry<GV, Traits>;
    using GridIndexType = typename IndexTraits<GV>::GridIndex;
    using LocalIndexType = typename IndexTraits<GV>::LocalIndex;
    using PNMData = typename Traits::PNMData;

    using Element = typename GV::template Codim<0>::Entity;
    using CoordScalar = typename GV::ctype;
    static const int dim = GV::dimension;
    static const int dimWorld = GV::dimensionworld;

public:
    //! export the discretization method this geometry belongs to
    using DiscretizationMethod = DiscretizationMethods::Box;
    static constexpr DiscretizationMethod discMethod{};

    //! export the type of the fv element geometry (the local view type)
    using LocalView = typename Traits::template LocalView<ThisType, true>;
    //! export the type of sub control volume
    using SubControlVolume = typename Traits::SubControlVolume;
    //! export the type of sub control volume
    using SubControlVolumeFace = typename Traits::SubControlVolumeFace;
    //! export dof mapper type
    using DofMapper = typename Traits::VertexMapper;
    //! export the finite element cache type
    using FeCache = Dune::PQkLocalFiniteElementCache<CoordScalar, Scalar, dim, 1>;
    //! export the grid view type
    using GridView = GV;

    //! Constructor
    PNMGridGeometry(const GridView gridView)
    : ParentType(gridView)
    {
        static_assert(GridView::dimension == 1, "Porenetwork model only allow GridView::dimension == 1!");
    }

    //! the vertex mapper is the dofMapper
    //! this is convenience to have better chance to have the same main files for box/tpfa/mpfa...
    const DofMapper& dofMapper() const
    { return this->vertexMapper(); }

    //! The total number of sub control volumes
    std::size_t numScv() const
    {  return numScv_; }

    //! The total number of sun control volume faces
    std::size_t numScvf() const
    { return numScvf_; }

    //! The total number of boundary sub control volume faces
    //! For compatibility reasons with cc methods
    std::size_t numBoundaryScvf() const
    { return 0; }

    //! The total number of degrees of freedom
    std::size_t numDofs() const
    { return this->vertexMapper().size(); }

    //! update all fvElementGeometries (do this again after grid adaption)
    template<class GridData>
    void update(const GridData& gridData)
    {
        ParentType::update();
        PNMData::update(this->gridView(), gridData);

        scvs_.clear();
        scvfs_.clear();

        auto numElements = this->gridView().size(0);
        scvs_.resize(numElements);
        scvfs_.resize(numElements);
        hasBoundaryScvf_.resize(numElements, false);

        boundaryDofIndices_.assign(numDofs(), false);

        numScvf_ = numElements;
        numScv_ = 2*numElements;

        // Build the SCV and SCV faces
        for (const auto& element : elements(this->gridView()))
        {
            // get the element geometry
            auto eIdx = this->elementMapper().index(element);
            auto elementGeometry = element.geometry();

            // construct the sub control volumes
            for (LocalIndexType scvLocalIdx = 0; scvLocalIdx < elementGeometry.corners(); ++scvLocalIdx)
            {
                const auto dofIdxGlobal = this->vertexMapper().subIndex(element, scvLocalIdx, dim);

                // get the fractional volume asssociated with the scv
                const auto volume = this->poreVolume(dofIdxGlobal) / this->coordinationNumber(dofIdxGlobal);

                scvs_[eIdx][scvLocalIdx] = SubControlVolume(dofIdxGlobal,
                                                            scvLocalIdx,
                                                            eIdx,
                                                            elementGeometry.corner(scvLocalIdx),
                                                            volume);

                if (this->poreLabel(dofIdxGlobal) > 0)
                {
                    if (boundaryDofIndices_[dofIdxGlobal])
                        continue;

                    boundaryDofIndices_[dofIdxGlobal] = true;
                    hasBoundaryScvf_[eIdx] = true;
                }
            }

            // construct the inner sub control volume face
            auto unitOuterNormal = elementGeometry.corner(1)-elementGeometry.corner(0);
            unitOuterNormal /= unitOuterNormal.two_norm();
            LocalIndexType scvfLocalIdx = 0;
            scvfs_[eIdx][0] = SubControlVolumeFace(elementGeometry.center(),
                                                   std::move(unitOuterNormal),
                                                   scvfLocalIdx++,
                                                   std::vector<LocalIndexType>({0, 1}));
        }
    }

    //! The finite element cache for creating local FE bases
    const FeCache& feCache() const
    { return feCache_; }

    //! Get the local scvs for an element
    const std::array<SubControlVolume, 2>& scvs(GridIndexType eIdx) const
    { return scvs_[eIdx]; }

    //! Get the local scvfs for an element
    const std::array<SubControlVolumeFace, 1>& scvfs(GridIndexType eIdx) const
    { return scvfs_[eIdx]; }

    //! If a vertex / d.o.f. is on the boundary
    bool dofOnBoundary(GridIndexType dofIdx) const
    { return boundaryDofIndices_[dofIdx]; }

    //! If a vertex / d.o.f. is on a periodic boundary (not implemented)
    bool dofOnPeriodicBoundary(GridIndexType dofIdx) const
    { return false; }

    //! The index of the vertex / d.o.f. on the other side of the periodic boundary
    GridIndexType periodicallyMappedDof(GridIndexType dofIdx) const
    { DUNE_THROW(Dune::NotImplemented, "Periodic boundaries"); }

    //! Returns the map between dofs across periodic boundaries
    std::unordered_map<GridIndexType, GridIndexType> periodicVertexMap() const
    { return std::unordered_map<GridIndexType, GridIndexType>{}; }

    //! Returns whether one of the geometry's scvfs lies on a boundary
    bool hasBoundaryScvf(GridIndexType eIdx) const
    { return hasBoundaryScvf_[eIdx]; }

private:
    const FeCache feCache_;

    std::vector<std::array<SubControlVolume, 2>> scvs_;
    std::vector<std::array<SubControlVolumeFace, 1>> scvfs_;

    std::size_t numScv_;
    std::size_t numScvf_;

    // vertices on the boudary
    std::vector<bool> boundaryDofIndices_;
    std::vector<bool> hasBoundaryScvf_;
};

/*!
 * \ingroup PoreNetworkDiscretization
 * \brief Base class for the finite volume geometry for porenetwork models
 * \note For caching disabled we store only some essential index maps to build up local systems on-demand in
 *       the corresponding FVElementGeometry
 */
template<class Scalar, class GV, class Traits>
class PNMGridGeometry<Scalar, GV, false, Traits>
: public BaseGridGeometry<GV, Traits>
, public Traits::PNMData
{
    using ThisType = PNMGridGeometry<Scalar, GV, false, Traits>;
    using ParentType = BaseGridGeometry<GV, Traits>;
    using GridIndexType = typename IndexTraits<GV>::GridIndex;
    using LocalIndexType = typename IndexTraits<GV>::LocalIndex;
    using PNMData = typename Traits::PNMData;

    static const int dim = GV::dimension;
    static const int dimWorld = GV::dimensionworld;

    using Element = typename GV::template Codim<0>::Entity;
    using CoordScalar = typename GV::ctype;

public:
    //! export the discretization method this geometry belongs to
    using DiscretizationMethod = DiscretizationMethods::Box;
    static constexpr DiscretizationMethod discMethod{};

    //! export the type of the fv element geometry (the local view type)
    using LocalView = typename Traits::template LocalView<ThisType, false>;
    //! export the type of sub control volume
    using SubControlVolume = typename Traits::SubControlVolume;
    //! export the type of sub control volume
    using SubControlVolumeFace = typename Traits::SubControlVolumeFace;
    //! export dof mapper type
    using DofMapper = typename Traits::VertexMapper;
    //! export the finite element cache type
    using FeCache = Dune::PQkLocalFiniteElementCache<CoordScalar, Scalar, dim, 1>;
    //! export the grid view type
    using GridView = GV;

    //! Constructor
    template<class GridData>
    PNMGridGeometry(const GridView gridView, const GridData& gridData)
    : ParentType(gridView)
    {
        static_assert(GridView::dimension == 1, "Porenetwork model only allow GridView::dimension == 1!");
        update_(gridData);
    }

    //! the vertex mapper is the dofMapper
    //! this is convenience to have better chance to have the same main files for box/tpfa/mpfa...
    const DofMapper& dofMapper() const
    { return this->vertexMapper(); }

    //! The total number of sub control volumes
    std::size_t numScv() const
    {  return numScv_; }

    //! The total number of sun control volume faces
    std::size_t numScvf() const
    { return numScvf_; }

    //! The total number of boundary sub control volume faces
    //! For compatibility reasons with cc methods
    std::size_t numBoundaryScvf() const
    { return 0; }

    //! The total number of degrees of freedom
    std::size_t numDofs() const
    { return this->vertexMapper().size(); }

    //! update all fvElementGeometries (call this after grid adaption)
    template<class GridData>
    void update(const GridView& gridView, const GridData& gridData)
    {
        ParentType::update(gridView);
        update_(gridData);
    }

    //! update all fvElementGeometries (call this after grid adaption)
    template<class GridData>
    void update(GridView&& gridView, const GridData& gridData)
    {
        ParentType::update(std::move(gridView));
        update_(gridData);
    }

    //! The finite element cache for creating local FE bases
    const FeCache& feCache() const
    { return feCache_; }

    //! If a vertex / d.o.f. is on the boundary
    bool dofOnBoundary(GridIndexType dofIdx) const
    { return boundaryDofIndices_[dofIdx]; }

    //! If a vertex / d.o.f. is on a periodic boundary (not implemented)
    bool dofOnPeriodicBoundary(GridIndexType dofIdx) const
    { return false; }

    //! The index of the vertex / d.o.f. on the other side of the periodic boundary
    GridIndexType periodicallyMappedDof(GridIndexType dofIdx) const
    { DUNE_THROW(Dune::NotImplemented, "Periodic boundaries"); }

    //! Returns the map between dofs across periodic boundaries
    std::unordered_map<GridIndexType, GridIndexType> periodicVertexMap() const
    { return std::unordered_map<GridIndexType, GridIndexType>{}; }

private:
    //! update all fvElementGeometries (do this again after grid adaption)
    template<class GridData>
    void update_(const GridData& gridData)
    {
        PNMData::update(this->gridView(), gridData);

        boundaryDofIndices_.assign(numDofs(), false);

        // save global data on the grid's scvs and scvfs
        numScvf_ = this->gridView().size(0);
        numScv_ = 2*numScvf_;

        for (const auto& element : elements(this->gridView()))
        {
            // treat boundaries
            for (LocalIndexType vIdxLocal = 0; vIdxLocal < 2; ++vIdxLocal)
            {
                const auto vIdxGlobal = this->vertexMapper().subIndex(element, vIdxLocal, dim);
                if (this->poreLabel(vIdxGlobal) > 0)
                {
                    if (boundaryDofIndices_[vIdxGlobal])
                        continue;

                    boundaryDofIndices_[vIdxGlobal] = true;
                }
            }
        }
    }

    const FeCache feCache_;

    // Information on the global number of geometries
    std::size_t numScv_;
    std::size_t numScvf_;

    // vertices on the boudary
    std::vector<bool> boundaryDofIndices_;
};

} // end namespace Dumux

#endif
