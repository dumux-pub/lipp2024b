// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \ingroup StaggeredDiscretization
 * \copydoc Dumux::FreeFlowStaggeredGeometryHelper
 */
#ifndef DUMUX_STAGGERED_PAIR_DATA_HANDLER_HH
#define DUMUX_STAGGERED_PAIR_DATA_HANDLER_HH

#include <dumux/discretization/staggered/freeflow/mystaggeredgeometryhelper.hh>
#include <dumux/discretization/staggered/freeflow/geometryhelper/geometryhelperutility.hh>

namespace Dumux {

/*!
 * \ingroup StaggeredDiscretization
 * \brief Helper class constructing the dual grid finite volume geometries
 *        for the free flow staggered discretization method.
 */
template <class GridView, class IntersectionMapper, class PairData, class NPGP, class PGP, class AD, class GH>
class StaggeredPairDataHandler {

    using Scalar = typename GridView::ctype;
    static constexpr int dim = GridView::dimension;
    static constexpr int dimWorld = GridView::dimensionworld;

    using ScvGeometry = Dune::CachedMultiLinearGeometry<Scalar, dim, dimWorld>;
    using ScvfGeometry = Dune::CachedMultiLinearGeometry<Scalar, dim - 1, dimWorld>;

    using GlobalPosition = typename ScvGeometry::GlobalCoordinate;

    using Element = typename GridView::template Codim<0>::Entity;
    using Intersection = typename GridView::Intersection;

    using ReferenceElements = typename Dune::ReferenceElements<Scalar, dim>;
    using GridIndexType = typename GridView::IndexSet::IndexType;

    //TODO include assert that checks for quad geometry
    static constexpr int numPairs = 2 * (dimWorld - 1);

    private:
    const GridView gridView_;
    std::shared_ptr<const IntersectionMapper> intersectionMapper_;
    const Element element_;
    Intersection intersection_;
    GeometryHelperUtility<GridView> utility_;
    PairData pairData_;
    NPGP normalPairGlobalPositions_;
    PGP parallelGlobalPositions_;
    const GH geomHelper_;
    AD axisData_;
    bool useConservation_;

    public:
     StaggeredPairDataHandler(const GeometryHelperUtility<GridView>& utility, const GridView& gridView,
      std::shared_ptr<const IntersectionMapper> intersectionMapper,
       const Element& element,
        Intersection& intersection,
         PairData& pairData,
         NPGP& npgp,
         PGP& pgp,
         const GH& geomHelper,
         AD& axisData,
         bool useConservation)
         : gridView_(gridView),
           intersectionMapper_(intersectionMapper),
           element_(element),
           intersection_(intersection),
           utility_(utility),
           pairData_(pairData),
           normalPairGlobalPositions_(npgp),
           parallelGlobalPositions_(pgp),
           geomHelper_(geomHelper),
           axisData_(axisData),
           useConservation_(useConservation){
           }

    PairData getPairData(){
        return pairData_;
    }

    NPGP getNormalPairGlobalPositions(){
        return normalPairGlobalPositions_;
    }

    PGP getParallelGlobalPositions(){
        return parallelGlobalPositions_;
    }

    AD getAxisData(){
        return axisData_;
    }

    /*!
     * \brief Fills all entries of the pair data
     */
    void fillPairData_()
    {
        // initialize values that could remain unitialized if the intersection lies on a boundary
        for(auto& data : pairData_)
        {
            int numParallelDofs = geomHelper_->order();

            if (::doFirstOrderLocalTruncErrorGlobalRefinement)
                numParallelDofs = 2;

            // parallel Dofs
            data.parallelDofs.clear();
            data.parallelDofs.resize(numParallelDofs);

            data.hasCornerParallelNeighbor = false;
            data.hasHalfParallelNeighbor = false;

            data.parallelDofsInterpolationFactors.clear();
            data.parallelDofsInterpolationFactors.resize(numParallelDofs);

            data.virtualFirstParallelFaceDofPos = 0.;

            // parallel Distances
            data.parallelDistances.clear();
            data.parallelDistances.resize(3, 0.0);

            // outer normals
            data.normalPair.second.resize(1);
            data.normalPair.second[0] = -1;
        }

        // get the inner normal Dof Index
        std::array<Scalar, numPairs> firstNormalDistancesContribution = {};
        std::array<Scalar, numPairs> secondNormalDistancesContribution = {};

        //first
        setNormalPairInfos_(0, firstNormalDistancesContribution);
        //second
        setNormalPairInfos_(1, secondNormalDistancesContribution);

        //might at some point include normalDistance in non-pair data
        for(unsigned int localSubFaceIdx = 0; localSubFaceIdx < numPairs; localSubFaceIdx++){
            auto& data = pairData_[localSubFaceIdx];
            data.normalDistance = firstNormalDistancesContribution[localSubFaceIdx] + secondNormalDistancesContribution[localSubFaceIdx];
            data.innerNormalDistance = firstNormalDistancesContribution[localSubFaceIdx];
            data.outerNormalDistance = secondNormalDistancesContribution[localSubFaceIdx];
        }

        assert(scalarCmp(firstNormalDistancesContribution[0], firstNormalDistancesContribution[1]));
        this->axisData_.selfToOppositeDistance  = 2 * firstNormalDistancesContribution[0];

        // get the parallel Dofs
        // TODO adapt to higher order
        int numPairParallelIdx = 0;

        if (dim == 2)
        {
            for (auto& pairDataElem : this->pairData_)
                pairDataElem.parallelDistances[0] = intersection_.geometry().volume(); //this is not the full parallel distance, there is also a parallelDistance[1]
        }
        else
        {
            std::cout << "staggeredgeometryhelper parallelDistances still need to figure that out for dim != 2. Also check the other locations where parallelDistances is filled" << std::endl;
        }

        for(const auto& normalIs : intersections(gridView_, element_))
        {
            //for cases where parallel dof is within the same element
            if (intersection_.neighbor() && (element_.level() < intersection_.outside().level()))
            {
                if (containerCmp(normalIs.centerUnitOuterNormal(), intersection_.centerUnitOuterNormal()) && !containerCmp(normalIs.geometry().center(), intersection_.geometry().center()))
                {
//                     |       |       |
//                     |       |       |
//                     |       |       |
//                     ---------========
//                     |  ^        ^   |
//                     |  |        |   |
//                     |normal  inter  |
//                     | Is    section_|
//                     |               |
//                     |    inside     |
//                     |               |
//                     -----------------

                    treatParallelSelfIntersection_(normalIs, numPairParallelIdx);
                }
            }

            if (intersection_.neighbor() && (element_.level() < intersection_.outside().level()) && !haveCommonCorner(normalIs, intersection_)) //normal where the parallel was already within the same element
            {
//                     |       |       |
//                     |       |       |
//                     |       |       |
//                     ---------========
//                    n|           ^   |
//                    o|           |   |
//                    r|        inter  |
//                    m|       section_|
//                    a|               |
//                    l|    inside     |
//                   Is|               |
//                     -----------------
//
//    also other nonnormal intersection that falls out in the next step

                /* skip this "normalIs" */
                continue;
            }

            if( utility_.elementFacetIsNormal_(intersectionMapper_->isIndexInInside(normalIs), intersectionMapper_->isIndexInInside(intersection_)) && haveCommonCorner(normalIs, intersection_) )
            {
                if( normalIs.neighbor() )
                {
                    const auto insideLevel = normalIs.inside().level();
                    const auto outsideLevel = normalIs.outside().level();
                    if (insideLevel == outsideLevel)//can still have adaptive influences!
                    {
                        treatParallelStandardCase_(normalIs, numPairParallelIdx);
                    }
//                     -------------
//                     |     |^next|
//                     |     | next|
//                     |     |     |
//                     -------======
//                     |     |^next|
//                     |     | nor-|
//                     |     | mal |
//                     -------======
//                     |    normal^||in
//                     |           ||ter
//                     | inside    ||sec
//                     |           ||tion_
//                     |           ||
//                     -------------
                    else if (insideLevel < outsideLevel)
                    {
                        treatParallelInCoarserOut_(normalIs, numPairParallelIdx);
                    }
                    else //insideLevel > outsideLevel
                    {
                        treatParallelOutCoarserIn_(normalIs, numPairParallelIdx);
                    }
                }
                else
                {
                    treatNoParallel_(normalIs, numPairParallelIdx);
                }
            }
        }
    }

    void setNormalPairInfos_(bool firstOrSecond, std::array<Scalar, numPairs>& normalDistances)
    {
        int numPairIdx = 0;

        if (!(firstOrSecond == 1 && !intersection_.neighbor()))
        {
            int elementIntersectionIdx = 0;
            int normalCheckIdx;

            for (unsigned int i = 0; i < numPairs; ++i)
            {
                if (firstOrSecond == 0 /*first*/)
                {
                    pairData_[i].normalPair.first.clear();
                    pairData_[i].normalPairInterpolationFactors.first.clear();
                    //TODO is I make this an argument of the function setNormalPairInfos_ to avoid copying
                    normalCheckIdx = intersectionMapper_->isIndexInInside(intersection_);
                }
                else //firstOrSecond == 1
                {
                    pairData_[i].normalPair.second.clear();
                    pairData_[i].normalPairInterpolationFactors.second.clear();
                    //TODO is I make this an argument of the function setNormalPairInfos_ to avoid copying
                    normalCheckIdx =  intersectionMapper_->isIndexInOutside(intersection_);
                }
            }
            Element element = (firstOrSecond == 0)?element_:intersection_.outside();

            int haveNonDirectionCornerCenterMatchCounter = 0;
            int notHaveNonDirectionCornerCenterMatchCounter = 0;
            std::array<unsigned int, numPairs> normalPairInitialSizes = {};
            for (unsigned int i = 0; i < numPairs; ++i) //those lines might go, if I am sure that {} is a zero initialization
            {
                normalPairInitialSizes[i] = 0;
            }

            std::array<unsigned int, numPairs> fluxCorrectionGeometryHelpingIndices = {};
            for (unsigned int i = 0; i < numPairs; ++i) //those lines might go, if I am sure that {} is a zero initialization
            {
                fluxCorrectionGeometryHelpingIndices[i] = 0;
            }

            for(const auto& elementIntersection : intersections(gridView_, element))
            {
                if((firstOrSecond == 0)?utility_.elementFacetIsNormal_(elementIntersectionIdx, normalCheckIdx):utility_.outsideElementFacetIsNormal_(elementIntersectionIdx, normalCheckIdx))
                {
                    //TODO make the following only be called when debug
                    if (numPairIdx >= numPairs || numPairIdx < 0)
                    {
                        DUNE_THROW(Dune::InvalidStateException, "numPairsIdx is not between 0 and numPairs-1, elementIntersection center = " << elementIntersection.geometry().center() << ", scvf  center = " << intersection_.geometry().center());
                    }

                    unsigned int inLevel;
                    unsigned int outLevel;

                    if (firstOrSecond == 0 /*first*/)
                    {
                        inLevel = element_.level();
                        outLevel = intersection_.neighbor()?intersection_.outside().level():inLevel;
                    }
                    else
                    {
                        inLevel = intersection_.outside().level();
                        outLevel = element_.level();
                    }

                    treatNormalPairCases_(firstOrSecond, normalDistances, element, elementIntersectionIdx, normalCheckIdx, haveNonDirectionCornerCenterMatchCounter, notHaveNonDirectionCornerCenterMatchCounter, normalPairInitialSizes, fluxCorrectionGeometryHelpingIndices, elementIntersection, inLevel, outLevel, numPairIdx);
                }
                elementIntersectionIdx++;
            }
        }
        else
        {
//             treat inIsBoundary
//            /----------
//            /||       |
//            /|| out=  |
//            /||element|
//            /||       |
//            /----------
//
//            /:boundary
//            ||:intersection

            // fill the normal pair entries
            for(int pairIdx = 0; pairIdx < numPairs; ++pairIdx)
            {
                assert(pairData_[pairIdx].normalPair.second[0] == -1); //pairData_[pairIdx].normalPair.second[0] is initialized with -1 and cleared in the if condition above

                normalDistances[numPairIdx] = 0.;
                numPairIdx++;
            }
        }
    }

    void treatNormalPairCases_(bool firstOrSecond, std::array<Scalar, numPairs>& normalDistances, const Element& element, const int& normalIntersectionIdx, const int& normalCheckIdx, int& haveNonDirectionCornerCenterMatchCounter, int& notHaveNonDirectionCornerCenterMatchCounter, std::array<unsigned int, numPairs>& normalPairInitialSizes, std::array<unsigned int, numPairs>& fluxCorrectionGeometryHelpingIndices, const Intersection& normalIntersection, unsigned int inLevel, unsigned int outLevel, int& numPairIdx)
    {
        if ((!intersection_.neighbor()) || (inLevel >= outLevel) || (inLevel < outLevel && !utility_.haveNonDirectionCornerCenterMatch_(normalIntersection))){
            //set local information for all cases
            if (firstOrSecond == 0 /*first*/)
            {
                if (utility_.haveNonDirectionCenterCornerMatch_(normalIntersection))
                {
                    this->pairData_[numPairIdx].localNormalFluxCorrectionIndex = normalIntersectionIdx;
                }
            }

            //set global information case-dependent
            if (intersection_.neighbor() && inLevel < outLevel && (!normalIntersection.neighbor() || (normalIntersection.neighbor() && (element.level() >= normalIntersection.outside().level()))))
            {
//             |      |
//             ********--------
//             |      |   |   |
//             |  in  |--------
//             |     ||out|   |
//             ----------------
//
//             ||: intersection_
//             **: normalIntersection

                setNormalPairDofInfo_(firstOrSecond, normalIntersectionIdx, element, numPairIdx, .5);
                setNormalPairGlobalPositions_(firstOrSecond, element.geometry().center(), numPairIdx);

                normalDistances[numPairIdx] = normalIntersection.geometry().volume() * 0.5;
                numPairIdx++;
            }
            else if ((!intersection_.neighbor() || !(inLevel < outLevel)) && (!normalIntersection.neighbor() || (normalIntersection.neighbor() && (element.level() >= normalIntersection.outside().level()))))
            {
//             -----****--------
//             |   |in||       |
//             |----****  out  |
//             |   |   |       |
//             -----------------
//
//             ||: intersection_
//             **: normalIntersection

//             |       |
//             *********--------
//             |      ||       |
//             |  in  ||  out  |
//             |      ||       |
//             -----------------
//
//             ||: intersection_
//             **: normalIntersection

//             |        |
//             **********/
//             |       ||/
//             |  in=  ||/
//             |element||/
//             ----------/
//
//             /: boundary
//             ||: intersection_
//             **: normalIntersection

                setNormalPairDofInfo_(firstOrSecond, normalIntersectionIdx, element, numPairIdx, 1.);
                setNormalPairGlobalPositions_(firstOrSecond, normalIntersection.geometry().center(), numPairIdx);

                normalDistances[numPairIdx] = normalIntersection.geometry().volume() * 0.5;
                numPairIdx++;
            }
            else if (normalIntersection.neighbor() && (element.level() < normalIntersection.outside().level()))
            {
//                 | | |
//             -----*-*---------
//             |   |in||       |
//             |----****  out  |
//             |   |   |       |
//             -----------------
//
//             ||: intersection_
//             **: normalIntersection

//             |   |   |
//             **** ****--------
//             |       |   |   |
//             |  in   |--------
//             |      ||out|   |
//             -----------------
//
//             ||: intersection_
//             **: normal intersections

//             |   |   |
//             **** ****--------
//             |      ||       |
//             |  in  ||  out  |
//             |      ||       |
//             -----------------
//
//             ||: intersection_
//             **: normal intersections

//             |   |    |
//             **** *****/
//             |       ||/
//             |  in=  ||/
//             |element||/
//             ----------/
//
//             /: boundary
//             ||: intersection_
//             **: normal intersections
                treatInCoarserNormalOut_(firstOrSecond, normalDistances, element, normalIntersectionIdx, normalCheckIdx, normalPairInitialSizes, fluxCorrectionGeometryHelpingIndices, normalIntersection, inLevel, outLevel, numPairIdx, notHaveNonDirectionCornerCenterMatchCounter);
            }
            else
            {
                DUNE_THROW(Dune::InvalidStateException, "Should not occur.");
            }
        }
        else
        {
//             |      |
//             ********--------
//             |     ||out|   |
//             |  in  |--------
//             |      |   |   |
//             ----------------
//
//             ||: intersection_
//             **: normalIntersection
//
//             or
//
//             |   |   |
//             **** ****--------
//             |      ||out|   |
//             |  in   |--------
//             |       |   |   |
//             -----------------
//
//             ||: intersection_
//             **: normal intersections

            treatNormalsWithCornerCenterMatchForInCoarserOut_(firstOrSecond, normalDistances, element, normalIntersectionIdx, normalPairInitialSizes, normalIntersection, numPairIdx, haveNonDirectionCornerCenterMatchCounter);
        }
    }

    void treatNormalsWithCornerCenterMatchForInCoarserOut_(bool firstOrSecond, std::array<Scalar, numPairs>& normalDistances, const Element& element, const int& normalIntersectionIdx, std::array<unsigned int, numPairs>& normalPairInitialSizes, const Intersection& normalIntersection, int& numPairIdx, int& haveNonDirectionCornerCenterMatchCounter){
        //TODO adapt to 3D
        int otherPairIdx = (numPairIdx == 0) ? 1 : 0;

        //set local information
        if (firstOrSecond == 0 /*first*/)
        {
            if (utility_.haveNonDirectionCenterCornerMatch_(normalIntersection))
            {
                this->pairData_[numPairIdx].localNormalFluxCorrectionIndex = normalIntersectionIdx;
            }
        }

        //set global information
        if (haveNonDirectionCornerCenterMatchCounter == 0)
        {
            for (unsigned int i = 0; i < numPairs; ++i)
            {
                if (firstOrSecond == 0)
                    normalPairInitialSizes[i] = pairData_[i].normalPair.first.size();
                else
                    normalPairInitialSizes[i] = pairData_[i].normalPair.second.size();
            }
            haveNonDirectionCornerCenterMatchCounter++;
        }

        if (!normalIntersection.neighbor() || (element.level() >= normalIntersection.outside().level()))
        {
//             |      |
//             ********--------
//             |     ||out|   |
//             |  in  |--------
//             |      |   |   |
//             ----------------
//
//             ||: intersection_
//             **: normalIntersection
            setNormalPairDofInfo_(firstOrSecond, normalIntersectionIdx, element, numPairIdx, 1.);
            setNormalPairDofInfo_(firstOrSecond, normalIntersectionIdx, element, otherPairIdx, .5);
            setNormalPairGlobalPositions_(firstOrSecond, normalIntersection.geometry().center(), numPairIdx);

            normalDistances[numPairIdx] = normalIntersection.geometry().volume() * 0.5;
            numPairIdx++;
        }
        else
        {
//             |   |   |
//             **** ****--------
//             |      ||out|   |
//             |  in   |--------
//             |       |   |   |
//             -----------------
//
//             ||: intersection_
//             **: normal intersections

            setNormalPairDofInfo_(firstOrSecond, normalIntersectionIdx, element, numPairIdx, .5);
            setNormalPairDofInfo_(firstOrSecond, normalIntersectionIdx, element, otherPairIdx, .25);

            if (utility_.haveNonDirectionCenterCornerMatch_(normalIntersection))
            {
                GlobalPosition pos;
                pos[utility_.directionIndex()] = element.geometry().center()[utility_.directionIndex()];

                for (const auto nonDirectionIdx : utility_.nonDirectionIndices())
                {
                    pos[nonDirectionIdx] = normalIntersection.geometry().center()[nonDirectionIdx];
                }

                setNormalPairGlobalPositions_(firstOrSecond, pos, numPairIdx);
            }

            normalDistances[numPairIdx] = normalIntersection.geometry().volume();

            //decide if numPairIdx needs to be incremented or not and do so if necessary
            unsigned int pairMemberSize;

            if (firstOrSecond == 0 /*first*/)
                pairMemberSize = pairData_[numPairIdx].normalPair.first.size();
            else
                pairMemberSize = pairData_[numPairIdx].normalPair.second.size();

            if (pairMemberSize - normalPairInitialSizes[numPairIdx] == 2)
            {
                numPairIdx++;
            }
        }
    }

    void setNormalPairGlobalPositions_(bool firstOrSecond, const GlobalPosition& pos, const int& numPairIdx)
    {
        if (firstOrSecond == 0 /*first*/)
        {
            normalPairGlobalPositions_[numPairIdx].first=pos;
        }
        else //(firstOrSecond == 1 /*second*/)
        {
            normalPairGlobalPositions_[numPairIdx].second=pos;
        }
    }

    void treatInCoarserNormalOut_(bool firstOrSecond, std::array<Scalar, numPairs>& normalDistances, const Element& element, const int& normalIntersectionIdx, const int& normalCheckIdx, std::array<unsigned int, numPairs>& normalPairInitialSizes, std::array<unsigned int, numPairs>& fluxCorrectionGeometryHelpingIndices, const Intersection& normalIntersection, unsigned int inLevel, unsigned int outLevel, int& numPairIdx, int& notHaveNonDirectionCornerCenterMatchCounter)
    {
        if ((!intersection_.neighbor()) || (inLevel >= outLevel))
        {
//             !intersection_.neighbor()
//             |   |    |
//             **** *****/
//             |       ||/
//             |  in=  ||/
//             |element||/
//             ----------/
//
//             /: boundary
//             ||: intersection_
//             **: normal intersections

//             inLevel > outLevel
//                 | | |
//             -----*-*---------
//             |   |in||       |
//             |--------  out  |
//             |   |   |       |
//             -----------------
//
//             ||: intersection_
//             **: normal intersections
//
//             case of two level distance over a diagonal should already work here

//             inLevel = outLevel
//             |   |   |
//             **** ****--------
//             |      ||       |
//             |  in  ||  out  |
//             |      ||       |
//             -----------------
//
//             ||: intersection_
//             **: normal intersections

            if (isFluxCorrectionGeometry(inLevel, outLevel, firstOrSecond, normalCheckIdx, numPairIdx, element))
            {
                if (haveCommonCorner(normalIntersection, intersection_))
                {
                    setNormalPairDofInfo_(firstOrSecond, normalIntersectionIdx, element, numPairIdx, 1.);
                    setNormalPairGlobalPositions_(firstOrSecond, normalIntersection.geometry().center()/*flux correction*/, numPairIdx);

                    normalDistances[numPairIdx] = 0.5*normalIntersection.geometry().volume();
                }

                if (fluxCorrectionGeometryHelpingIndices[numPairIdx] == 1)
                {
                    numPairIdx++;
                }
                else
                {
                    fluxCorrectionGeometryHelpingIndices[numPairIdx]++;
                }
            }
            else
            {
                if (utility_.haveNonDirectionCenterCornerMatch_(normalIntersection))
                {
                    GlobalPosition pos;
                    pos[utility_.directionIndex()] = element.geometry().center()[utility_.directionIndex()];

                    for (const auto nonDirectionIdx : utility_.nonDirectionIndices())
                    {
                        pos[nonDirectionIdx] = normalIntersection.geometry().center()[nonDirectionIdx];
                    }

                    setNormalPairGlobalPositions_(firstOrSecond, pos, numPairIdx);
                }

                setNormalPairDofInfo_(firstOrSecond, normalIntersectionIdx, element, numPairIdx, .5);

                normalDistances[numPairIdx] = normalIntersection.geometry().volume();

                unsigned int pairMemberSize;

                if (firstOrSecond == 0 /*first*/)
                    pairMemberSize = pairData_[numPairIdx].normalPair.first.size();
                else
                    pairMemberSize = pairData_[numPairIdx].normalPair.second.size();

                if (pairMemberSize == 2)
                {
                    numPairIdx++;
                }
            }
        }
        else
        {
//             |   |   |
//             **** ****--------
//             |       |   |   |
//             |  in   |--------
//             |      ||out|   |
//             -----------------
//
//             ||: intersection_
//             **: normal intersections
            if (notHaveNonDirectionCornerCenterMatchCounter == 0)
            {
                for (unsigned int i = 0; i < numPairs; ++i)
                {
                    if (firstOrSecond == 0 /*first*/)
                        normalPairInitialSizes[i] = pairData_[i].normalPair.first.size();
                    else
                        normalPairInitialSizes[i] = pairData_[i].normalPair.second.size();
                }
                notHaveNonDirectionCornerCenterMatchCounter++;
            }

            setNormalPairDofInfo_(firstOrSecond, normalIntersectionIdx, element, numPairIdx, .25);
            setNormalPairGlobalPositions_(firstOrSecond, element.geometry().center(), numPairIdx);

            normalDistances[numPairIdx] = normalIntersection.geometry().volume();

            unsigned int pairMemberSize;

            if (firstOrSecond == 0 /*first*/)
                pairMemberSize = pairData_[numPairIdx].normalPair.first.size();
            else
                pairMemberSize = pairData_[numPairIdx].normalPair.second.size();

            if (pairMemberSize - normalPairInitialSizes[numPairIdx] == 2)
            {
                numPairIdx++;
            }
        }
    }

    bool isFluxCorrectionGeometry(bool inLevel, bool outLevel, bool firstOrSecond, const int& normalCheckIdx, bool numPairIdx, const Element& element)
    {
//       for the cases of no neighbor or inLevel > outLevel appearing here, the flux is corrected in fluxvariables.hh. It needs not be corrected here in the staggeredgeometryhelper. isFluxCorrectionGeometry should return false. For this I assumed that at an intersection levels can differ by 1 maximally
        if (useConservation_ && inLevel == outLevel && firstOrSecond == 1 /*second*/)//this is basically a flux correction, we only fill //fill booleans
        {
            unsigned int firstNormalOutLevel = inLevel; //just an initialization, should always be overwritten in cases reaching the return inLevel == firstNormalOutLevel
            int variedNormalIsIdx = 0;
            int variedPairIdx = 0;
            Intersection intersectionTouchingNormalIntersectionOfCorrectNumPair; //normalIntersection which touches the intersection

            for(const auto& variedNormalIs : intersections(gridView_, element))
            {
                if((firstOrSecond == 0)?utility_.elementFacetIsNormal_(variedNormalIsIdx, normalCheckIdx):utility_.outsideElementFacetIsNormal_(variedNormalIsIdx, normalCheckIdx))
                {
//                     flux correction geometry
//
//                     ----------------------------------
//                     |       |       |                |
//                     |       |       |                |
//                     |       |       |                |
//                     ------------**************       |
//                     |       |   *   |        *       |
//                     |       |   *   |        *       |
//                     |       |   *   |        *       |
//                     ---x1--x3**x2***?????????x4-------
//                     |       *       ||       *       |
//                     |       *       ||       *       |
//                     |       *       ||       *       |
//                     |      in       ||     out       |
//                     |       *       ||       *       |
//                     |       *       ||       *       |
//                     |       *       ||       *       |
//                     --------******************--------
//
//           ||: the scvf
//           ***: the control volume
//           ???: line along which the gradient normalDeltaV is to be thought about carefully, As the fine side (in the picture the upper half) along this line does a gradient between x2 and x4 (see fine control volume) the coarse side (in the picture the lower half) has to adapt its behavior accordingly. Hence not an interpolated value beween x1 and x2 but only a value at x2 serves as the normal second for the coarse control volume around the scvf

                    if(haveCommonCorner(variedNormalIs, intersection_))
                    {
                        if (numPairIdx == variedPairIdx)
                        {
                            intersectionTouchingNormalIntersectionOfCorrectNumPair = variedNormalIs;
                        }
                        variedPairIdx++; //the if-case is not complicated with respect to pair
                    }
                }
                variedNormalIsIdx++;
            }

            if (!intersectionTouchingNormalIntersectionOfCorrectNumPair.neighbor())
            {
                return false;
            }
            else
            {
                for(const auto&  diagonalOutsideIs : intersections(gridView_, intersectionTouchingNormalIntersectionOfCorrectNumPair.outside()))
                {
                    auto minusIntersectionNormal = intersection_.centerUnitOuterNormal(); //minus because only relevant for firstOrSecond = second
                    minusIntersectionNormal *= -1.;

                    if (containerCmp(diagonalOutsideIs.centerUnitOuterNormal(), minusIntersectionNormal))
                    {
                        if (!diagonalOutsideIs.neighbor())
                        {
                            return false;
                            //if there is no neighbor here, then there is no critical control volume
                        }
                        else
                        {
                            firstNormalOutLevel = diagonalOutsideIs.outside().level();
                        }
                    }
                }
            }

            return inLevel == firstNormalOutLevel;
        }
        else
        {
            return false;
        }
    }

    //! Sets the information about the normal faces (within the element)
    void setNormalPairDofInfo_(bool firstOrSecond, const int isIdx, const Element& element, int i, Scalar interpolationFactor)
    {
        auto val = intersectionMapper_->globalIntersectionIndex(element, isIdx);

        // store the normal dofIdx
        if (firstOrSecond == 0 /*first*/)
        {
            auto& dofIdx = pairData_[i].normalPair.first;
            auto& interpFact = pairData_[i].normalPairInterpolationFactors.first;

            if (std::find(dofIdx.begin(), dofIdx.end(), val) == dofIdx.end())
            {
                dofIdx.push_back(val);
                interpFact.push_back(interpolationFactor);
            }
        }
        else /*(firstOrSecond == 1 second)*/
        {
            auto& dofIdx = pairData_[i].normalPair.second;
            auto& interpFact = pairData_[i].normalPairInterpolationFactors.second;

            if (std::find(dofIdx.begin(), dofIdx.end(), val) == dofIdx.end())
            {
                dofIdx.push_back(val);
                interpFact.push_back(interpolationFactor);
            }
        }
    }

    void treatParallelSelfIntersection_(const Intersection& parallelSelfIntersection, int& numPairParallelIdx)
    {
//      Pictures here are drawn assuming that there are not fine neighbors other than at the side of the intersection. If there are additional fine neighbors this does not change anything.
//      || or ==: self intersection_
//      ***: normal intersection
//      +++: parallel self intersection
//       []: pair indices belonging to the normal intersections
// plain number: local indices

//      Four geometries with no special treatment:
//          4 [1]                       4 [1]                        4                           3      4
//      **************              **************             --------------                 =======+++++++
//    1 ||           |              |           || 2           *            |                 *            |
//      ||   in      | 2          0 |    in     ||         0[0]*    in      | 1[1]        0[0]*    in      | 1[1]
//      +            |              |            +             *            |                 *            |
//    0 +            |              |            + 1           *            |                 *            |
//      --------------              --------------             =======+++++++                 --------------
//          3 [0]                       3 [0]                     2      3                           2

        //parallel within the same element
        const auto elem = parallelSelfIntersection.inside();
        const auto idx = intersectionMapper_->isIndexInInside(parallelSelfIntersection);
        const auto globIdx = intersectionMapper_->globalIntersectionIndex(elem, idx);
        const auto& paraCenter = parallelSelfIntersection.geometry().center();
        const auto& isCenter = intersection_.geometry().center();

        Scalar eps = 1e-10;
        std::size_t previousIdx = numPairParallelIdx;

        if (paraCenter[1] > isCenter[1] + eps)
        {
//      We have to consider the pair indices because the loop for normal first, first reaches [0] then [1] and parallel should be numbered in the same way as the normal pairs.

//          4 [1]                      4 [1]
//      --------------             --------------
//    1 +            |             |            +
//      +     in     | 2         0 |    in      + 2
//      ||           |             |           ||
//    0 ||           |             |           || 1
//      **************             **************
//          3 [0]                       3 [0]
            assert (previousIdx == 0); //we arrive at the parallelSelfIs, before we arrive at the normal intersection
            numPairParallelIdx = 1;
            //TODO think through for 3D
        }

        if (paraCenter[0] < isCenter[0] - eps)
        {
//            4                       3      4
//      --------------             +++++++=======
//      |            *             |            *
//  0[0]|    in      * 1[1]    0[0]|     in     * 1[1]
//      |            *             |            *
//      |            *             |            *
//      +++++++=======             --------------
//         2     3                        2
//
//      In the above-visualized two cases, one first arrives at the normalIs, then I have to pack the values which I mistakenly wrote into pair 0 into pair 1
            assert (previousIdx == 1);
            numPairParallelIdx = 0;
            //TODO think through for 3D
            this->pairData_[1].parallelDofs[0/*order*/] = this->pairData_[0].parallelDofs[0/*order*/];
            this->pairData_[1].parallelDofsInterpolationFactors[0/*order*/] = this->pairData_[0].parallelDofsInterpolationFactors[0/*order*/];
            this->pairData_[1].parallelDistances[1] = this->pairData_[0].parallelDistances[1];
            this->pairData_[1].virtualFirstParallelFaceDofPos = this->pairData_[0].virtualFirstParallelFaceDofPos;

            if(!parallelSelfIntersection.neighbor() && intersection_.neighbor())
            {
                DUNE_THROW(Dune::InvalidStateException, "forbidden geometry");
            }

            parallelGlobalPositions_[1] = parallelGlobalPositions_[0];

            this->pairData_[0].parallelDofs[0].clear();
            this->pairData_[0].parallelDofsInterpolationFactors[0].clear();
        }

        parallelGlobalPositions_[numPairParallelIdx] = parallelSelfIntersection.geometry().center();

        this->pairData_[numPairParallelIdx].parallelDofs[0].push_back(globIdx);

        if(!parallelSelfIntersection.neighbor() && intersection_.neighbor())
        {
            DUNE_THROW(Dune::InvalidStateException, "forbidden geometry");
        }

        this->pairData_[numPairParallelIdx].parallelDofsInterpolationFactors[0].push_back(1);
        this->pairData_[numPairParallelIdx].parallelDistances[1] = parallelSelfIntersection.geometry().volume();

        if (paraCenter[1] > isCenter[1] + eps)
        {
            numPairParallelIdx = previousIdx;
            //TODO think through for 3D
        }
        else
        {
            numPairParallelIdx++;
        }
    }

    void treatParallelStandardCase_(const Intersection& normalIs, int& numPairParallelIdx)
    {
//      TODO adapt for higher order

//         coarse                fine upper right neighbor(s)
//      --------------       --------------
//      |          |         |          |    not taking value here
//      |         -->        |          |--
//      |          |         |          -->  taking value only here (linear interpolation between two values)
//      ************--       ************--
//      |         ||         |         ||
//      |         ||         |         ||
//      |         ||         |         ||
//      ------------         ------------
//      |          |         |          |
//
//     **: normalIs


        for(const auto& is : intersections(gridView_, normalIs.outside()))
        {
            if (containerCmp(is.centerUnitOuterNormal(), intersection_.centerUnitOuterNormal()) && haveCommonCorner(is, intersection_))
            {
                parallelGlobalPositions_[numPairParallelIdx] = is.geometry().center();
                this->pairData_[numPairParallelIdx].parallelDofs[0].push_back(intersectionMapper_->globalIntersectionIndex(normalIs.outside(), intersectionMapper_->isIndexInInside(is)));
                this->pairData_[numPairParallelIdx].parallelDistances[1] = is.geometry().volume(); //parallelDistances[0] is filled in the superordinate function
/*
 * ------------
 * | yyyyyyyy s
 * | yyyyyyyy s
 * | yyyyyyyy s
 * -----------------------
 * |          |          |
 * |          |          |
 * |          |          |
 * -----------------------
 */
                if (!intersection_.neighbor() && is.neighbor())
                {
                    this->pairData_[numPairParallelIdx].hasCornerParallelNeighbor = true;

                    this->pairData_[numPairParallelIdx].cornerGeometryEIdx = gridView_.indexSet().index(is.outside());

                    int localIdx = 0;
                    for (const auto& boundaryIs : intersections(gridView_, is.outside()))
                    {
                        auto minusNormalIsNormal = normalIs.centerUnitOuterNormal();
                        minusNormalIsNormal *= -1.;

                        if (haveCommonCorner(boundaryIs,intersection_)&& containerCmp(boundaryIs.centerUnitOuterNormal(), minusNormalIsNormal))
                        {
                            this->pairData_[numPairParallelIdx].cornerGeometryLocalFaceIdx = localIdx;
                            break;
                        }

                        ++localIdx;
                    }
                }
/*
 * ------------
 * |          |
 * |          |
 * |          |
 * -----------------------
 * | yyyyyyyy s          |
 * | yyyyyyyy s          |
 * | yyyyyyyy s          |
 * -----------------------
 */
                if (!is.neighbor() && intersection_.neighbor())
                {
                    this->pairData_[numPairParallelIdx].hasHalfParallelNeighbor = true;

                    this->pairData_[numPairParallelIdx].cornerGeometryEIdx = gridView_.indexSet().index(intersection_.outside());

                    int localIdx = 0;
                    for (const auto& boundaryIs : intersections(gridView_, intersection_.outside()))
                    {
                        if (haveCommonCorner(boundaryIs,intersection_)&& containerCmp(boundaryIs.centerUnitOuterNormal(), normalIs.centerUnitOuterNormal()))
                        {
                            this->pairData_[numPairParallelIdx].cornerGeometryLocalFaceIdx = localIdx;
                            break;
                        }

                        ++localIdx;
                    }
                }
            }
            else if (::doFirstOrderLocalTruncErrorGlobalRefinement && containerCmp(is.centerUnitOuterNormal(), normalIs.centerUnitOuterNormal()))
            {
                // we only use parallelIs[1] in the case of a (possibly-graded) grid which is NOT locally refined, that is wyh this else if implementation is only thought through for that case
                if (is.neighbor())
                {
                    for(const auto& nextIs : intersections(gridView_, is.outside()))
                    {
                        if (containerCmp(nextIs.centerUnitOuterNormal(), intersection_.centerUnitOuterNormal()))
                        {
                            this->pairData_[numPairParallelIdx].parallelDofs[1].push_back(intersectionMapper_->globalIntersectionIndex(is.outside(), intersectionMapper_->isIndexInInside(nextIs)));
                            this->pairData_[numPairParallelIdx].parallelDistances[2] = nextIs.geometry().volume();
                        }
                    }
                }
                else
                {
                    this->pairData_[numPairParallelIdx].parallelDofs[1].push_back(-1);
                }
            }
        }

        fillParallelInterpolationFactors_(numPairParallelIdx);

        numPairParallelIdx++;
    }

    void treatParallelInCoarserOut_(const Intersection& normalIs, int& numPairParallelIdx)
    {
//                     -------------
//                     |     |^next|
//                     |     | next|
//                     |     |     |
//                     -------======
//                     |     |^next|
//                     |     | nor-|
//                     |     | mal |
//                     -------======
//                     |    normal^||in
//                     |           ||ter
//                     | inside    ||sec
//                     |           ||tion_
//                     |           ||
//                     -------------
        bool wantToContinue = false;
        for (unsigned int i = 0; i < normalIs.geometry().corners(); ++i)
        {
            if (scalarCmp(normalIs.geometry().corner(i)[utility_.directionIndex()], intersection_.geometry().center()[utility_.directionIndex()]))
            {
                wantToContinue = true;
            }
        }
        if (wantToContinue)
        {
            for(const auto& nextElementIs : intersections(gridView_, normalIs.outside()))
            {
//                 if( containerCmp(nextElementIs.centerUnitOuterNormal(), normalIs.centerUnitOuterNormal()) )
//                 {
//                     const auto& nextNormalIs = nextElementIs;
//
//                     if (nextNormalIs.neighbor())
//                     {
//                         for (const auto& nextNextElementIs : intersections(gridView_, nextNormalIs.outside()))
//                         {
//                             if (containerCmp(nextNextElementIs.centerUnitOuterNormal(), intersection_.centerUnitOuterNormal()) )
//                             {
//                                 const auto& nextNextParallelIs = nextNextElementIs;
//                                 this->pairData_[numPairParallelIdx].parallelDofs[0/*without higher order*/].push_back(intersectionMapper_->globalIntersectionIndex(nextNormalIs.outside(), intersectionMapper_->isIndexInInside(nextNextParallelIs)));
//                                 this->pairData_[numPairParallelIdx].parallelDofsInterpolationFactors[0].push_back(.5);
//                                 this->pairData_[numPairParallelIdx].parallelDistances[1] = nextNextParallelIs.geometry().volume(); //TODO think if +=
//                             }
//                         }
//                     }
//                     else
//                     {
//                         //TODO write what should happen here
//                     }
//                 }
                /*else */if ( containerCmp(nextElementIs.centerUnitOuterNormal(), intersection_.centerUnitOuterNormal()) )
                {
                    bool isTwoLevelDiagonal = nextElementIs.neighbor() && nextElementIs.inside().level() < nextElementIs.outside().level();

                    if (!isTwoLevelDiagonal || (isTwoLevelDiagonal && haveCommonCorner(nextElementIs, intersection_)))
                    {
                        const auto& nextParallelIs = nextElementIs;

                        parallelGlobalPositions_[numPairParallelIdx] = nextParallelIs.geometry().center();

                        this->pairData_[numPairParallelIdx].parallelDofs[0/*without higher order*/].push_back(intersectionMapper_->globalIntersectionIndex(normalIs.outside(), intersectionMapper_->isIndexInInside(nextParallelIs)));
                        this->pairData_[numPairParallelIdx].parallelDofsInterpolationFactors[0].push_back(/*.5*/1.);
                        this->pairData_[numPairParallelIdx].parallelDistances[1] += nextParallelIs.geometry().volume();

/*
 * ------------
 * | yyyyyyyy s
 * | yyyyyyyy s
 * | yyyyyyyy s
 * -----------------------
 * |    |     |          |
 * |----------|          |
 * |    |     |          |
 * -----------------------
 */
                        if (!intersection_.neighbor() && nextParallelIs.neighbor())
                        {
                            this->pairData_[numPairParallelIdx].hasCornerParallelNeighbor = true;

                            this->pairData_[numPairParallelIdx].cornerGeometryEIdx = gridView_.indexSet().index(nextParallelIs.outside());

                            int localIdx = 0;
                            for (const auto& boundaryIs : intersections(gridView_, nextParallelIs.outside()))
                            {
                                auto minusNormalIsNormal = normalIs.centerUnitOuterNormal();
                                minusNormalIsNormal *= -1.;

                                if (haveCommonCorner(boundaryIs,intersection_)&& containerCmp(boundaryIs.centerUnitOuterNormal(), minusNormalIsNormal))
                                {
                                    this->pairData_[numPairParallelIdx].cornerGeometryLocalFaceIdx = localIdx;
                                    break;
                                }

                                ++localIdx;
                            }
                        }
/*
 * ------------
 * |    |     |
 * |----------|
 * |    |     |
 * -----------------------
 * | yyyyyyyy s          |
 * | yyyyyyyy s          |
 * | yyyyyyyy s          |
 * -----------------------
 */
                        if (!nextParallelIs.neighbor() && intersection_.neighbor())
                        {
                            this->pairData_[numPairParallelIdx].hasHalfParallelNeighbor = true;

                            this->pairData_[numPairParallelIdx].cornerGeometryEIdx = gridView_.indexSet().index(intersection_.outside());

                            int localIdx = 0;
                            for (const auto& boundaryIs : intersections(gridView_, intersection_.outside()))
                            {
                                if (haveCommonCorner(boundaryIs,intersection_)&& containerCmp(boundaryIs.centerUnitOuterNormal(), normalIs.centerUnitOuterNormal()))
                                {
                                    this->pairData_[numPairParallelIdx].cornerGeometryLocalFaceIdx = localIdx;
                                    break;
                                }

                                ++localIdx;
                            }
                        }
                    }
                }
            }
            numPairParallelIdx++;
        }
    }

    void treatParallelOutCoarserIn_(const Intersection& normalIs, int& numPairParallelIdx)
    {
        bool isEasierGeometry = false;
        const auto outsideGeometry = normalIs.outside().geometry();
        for (unsigned int i = 0; i < outsideGeometry.corners(); ++i)
        {
            if (dim == 2 && i == 4)
            {
                DUNE_THROW(Dune::InvalidStateException, "Expected 4 corners of the rectangle only.");
            }

            for (unsigned int j = 0; j < intersection_.geometry().corners(); ++j)
            {
                if ( containerCmp(intersection_.geometry().corner(j), outsideGeometry.corner(i)))
                {
                    isEasierGeometry = true;
                }
            }
        }

//                     -------------
//            this left|           |
//  neighbor determines|           |
//if true one or twice,|           |
// parallel dofs only  |           |
// from the left       |           |
//                     ======-------
//                    ||     |     |
//                    ||     |     |
//                    ||     |     |
//                     -------------
// ||:myintersection, =: normalIntersection

        if (isEasierGeometry )
        {
            for(const auto& nextElementIs : intersections(gridView_, normalIs.outside()))
            {
                if ( containerCmp(nextElementIs.centerUnitOuterNormal(), intersection_.centerUnitOuterNormal()) && haveCommonCorner(nextElementIs, intersection_) )
                {
                    const auto& nextParallelIs = nextElementIs;
                    parallelGlobalPositions_[numPairParallelIdx] = nextParallelIs.geometry().center();
                    this->pairData_[numPairParallelIdx].parallelDofs[0/*without higher order*/].push_back(intersectionMapper_->globalIntersectionIndex(normalIs.outside(), intersectionMapper_->isIndexInInside(nextParallelIs)));
                    this->pairData_[numPairParallelIdx].parallelDistances[1] = nextParallelIs.geometry().volume();

/*
 * ------------
 * |     |    |
 * |----------|
 * |     | yy s
 * -----------------------
 * |          |          |
 * |          |          |
 * |          |          |
 * -----------------------
 */
                    if (!intersection_.neighbor() && nextParallelIs.neighbor())
                    {
                        this->pairData_[numPairParallelIdx].hasCornerParallelNeighbor = true;

                        this->pairData_[numPairParallelIdx].cornerGeometryEIdx = gridView_.indexSet().index(nextParallelIs.outside());

                        int localIdx = 0;
                        for (const auto& boundaryIs : intersections(gridView_, nextParallelIs.outside()))
                        {
                            auto minusNormalIsNormal = normalIs.centerUnitOuterNormal();
                            minusNormalIsNormal *= -1.;

                            if (haveCommonCorner(boundaryIs,intersection_)&& containerCmp(boundaryIs.centerUnitOuterNormal(), minusNormalIsNormal))
                            {
                                this->pairData_[numPairParallelIdx].cornerGeometryLocalFaceIdx = localIdx;
                                break;
                            }

                            ++localIdx;
                        }
                    }
/*
 * ------------
 * |          |
 * |          |
 * |          |
 * -----------------------
 * |     | yy s          |
 * |----------|          |
 * |     |    |          |
 * -----------------------
 */
                    if (!nextParallelIs.neighbor() && intersection_.neighbor())
                    {
                        this->pairData_[numPairParallelIdx].hasHalfParallelNeighbor = true;

                        this->pairData_[numPairParallelIdx].cornerGeometryEIdx = gridView_.indexSet().index(intersection_.outside());

                        int localIdx = 0;
                        for (const auto& boundaryIs : intersections(gridView_, intersection_.outside()))
                        {
                            if (haveCommonCorner(boundaryIs,intersection_)&& containerCmp(boundaryIs.centerUnitOuterNormal(), normalIs.centerUnitOuterNormal()))
                            {
                                this->pairData_[numPairParallelIdx].cornerGeometryLocalFaceIdx = localIdx;
                                break;
                            }

                            ++localIdx;
                        }
                    }
                }
            }

            fillParallelInterpolationFactors_(numPairParallelIdx);

//             if (this->pairData_[numPairParallelIdx].parallelDofs[0/*without higher order*/].size() == 2)
//             {
//                 this->pairData_[numPairParallelIdx].parallelDistances[1] *= 2.;
//             }
        }

//                     -------------
//            this left|           | parallel dofs also from right,
//  neighbor determines|           | also if statement true once
//if true one or twice,|           | or twice, depending on this
// parallel dofs from  |           | right neighbor
// left                |           |
//                     ======-------
//                     |    ||     |
//                     |    ||     |
//                     |    ||     |
//                     -------------
// ||:myintersection, =: normalIntersection

        else
        {
            //unitNormal
            for(const auto& nextElementIs : intersections(gridView_, normalIs.outside()))
            {
                const auto unitNormal = intersection_.centerUnitOuterNormal();
                const auto nextUnitNormal = nextElementIs.centerUnitOuterNormal();

                if ( containerCmp(nextUnitNormal, unitNormal))
                {
                    // can be true once or twice
                    const auto& nextParallelIs = nextElementIs;
                    parallelGlobalPositions_[numPairParallelIdx] = normalIs.outside().geometry().center();
                    this->pairData_[numPairParallelIdx].parallelDofs[0/*without higher order*/].push_back(intersectionMapper_->globalIntersectionIndex(normalIs.outside(), intersectionMapper_->isIndexInInside(nextParallelIs)));
                    this->pairData_[numPairParallelIdx].parallelDistances[1] = nextParallelIs.geometry().volume();
                }
            }

            fillParallelInterpolationFactors_(numPairParallelIdx, true);

            if (this->pairData_[numPairParallelIdx].parallelDofs[0/*without higher order*/].size() == 2)
            {
                this->pairData_[numPairParallelIdx].parallelDistances[1] *= 2.;
            }

            unsigned int numDofsOffset = this->pairData_[numPairParallelIdx].parallelDofs[0/*without higher order*/].size();

            //minusUnitNormal
            for(const auto& nextElementIs : intersections(gridView_, normalIs.outside()))
            {
                const auto unitNormal = intersection_.centerUnitOuterNormal();
                auto minusUnitNormal = unitNormal;
                minusUnitNormal *= (-1.0);
                const auto nextUnitNormal = nextElementIs.centerUnitOuterNormal();

                if ( containerCmp(nextUnitNormal, minusUnitNormal))
                {
                    // can be true once or twice
                    const auto& nextParallelIs = nextElementIs;
                    this->pairData_[numPairParallelIdx].parallelDofs[0/*without higher order*/].push_back(intersectionMapper_->globalIntersectionIndex(normalIs.outside(), intersectionMapper_->isIndexInInside(nextParallelIs)));
                    this->pairData_[numPairParallelIdx].parallelDistances[1] = nextParallelIs.geometry().volume();
                }
            }
            fillParallelInterpolationFactors_(numPairParallelIdx, true, numDofsOffset);

            if (this->pairData_[numPairParallelIdx].parallelDofs[0/*without higher order*/].size() - numDofsOffset == 2)
            {
                this->pairData_[numPairParallelIdx].parallelDistances[1] *= 2.;
            }
        }

        numPairParallelIdx++;
    }

    void fillParallelInterpolationFactors_(const int numPairParallelIdx, bool halfed = false, int numDofsOffset = 0)
    {
        //TODO adapt for higher order
        auto& interpFactors = this->pairData_[numPairParallelIdx].parallelDofsInterpolationFactors[0/*without higher order*/];
        auto& parallelDofsFilled = this->pairData_[numPairParallelIdx].parallelDofs[0/*without higher order*/];

        unsigned int numDofs = parallelDofsFilled.size() - numDofsOffset;

        if (numDofs == 1)
        {
            if (halfed == false)
                interpFactors.push_back(1);
            else if (halfed == true)
                interpFactors.push_back(.5);
        }
        else if (numDofs == 2)
        {
            if (halfed == false)
            {
                interpFactors.push_back(.5);
                interpFactors.push_back(.5);
            }
            else if (halfed == true)
            {
                interpFactors.push_back(.25);
                interpFactors.push_back(.25);
            }
        }
        else
        {
            DUNE_THROW(Dune::InvalidStateException, "Did not expect anything other than 1 or 2 parallelDofs-offset.");
        }
    }

    void treatNoParallel_(const Intersection& normalIs, int& numPairParallelIdx)
    {
        this->pairData_[numPairParallelIdx].parallelDofs[0].push_back(-1);

        if (::doFirstOrderLocalTruncErrorGlobalRefinement)
            this->pairData_[numPairParallelIdx].parallelDofs[1].push_back(-1);

        // If the normalIs has no neighbor we have to deal with the virtual outer parallel dof
        const auto& elementCenter = this->element_.geometry().center();
        const auto& boundaryFacetCenter = normalIs.geometry().center();

        auto distance = boundaryFacetCenter - elementCenter;

        if (scalarCmp(std::abs(distance[0]) + std::abs(distance[1]), std::abs(intersection_.geometry().volume())))
        {
//                           /////////////
//          -------------------------------
//          |       |      ||      ^ dis- |
//          |       |      ||      | tance|
//          ----------------|      v      |
//          |       |       |             |
//          |       |       |             |
//          -------------------------------

            distance[0] /= 2.;
            distance[1] /= 2.;
        }

        const auto virtualFirstParallelFaceDofPos = this->intersection_.geometry().center() + distance;

        this->pairData_[numPairParallelIdx].virtualFirstParallelFaceDofPos = std::move(virtualFirstParallelFaceDofPos);

        numPairParallelIdx++;
    }
};

}  // namespace Dumux
#endif
