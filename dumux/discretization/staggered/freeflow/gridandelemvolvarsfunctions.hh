// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \ingroup StaggeredDiscretization
 * \copydoc Dumux::MyStaggeredGridVolumeVariables
 */
#ifndef DUMUX_DISCRETIZATION_STAGGERED_GRID_GRIDANDELEMVOLVARSFUNCTIONS_HH
#define DUMUX_DISCRETIZATION_STAGGERED_GRID_GRIDANDELEMVOLVARSFUNCTIONS_HH

#include <dune/common/exceptions.hh>

//! make the local view function available whenever we use this class
#include <dumux/discretization/localview.hh>
#include <dumux/discretization/staggered/elementsolution.hh>

namespace Dumux {
    //! Returns the primary  variales used for the boundary volVars and checks for admissable
    //! combinations for boundary conditions.
    template<class Problem, class CellCenterPrimaryVariables, class SubControlVolumeFace, class VolumeVariables>
    static auto getBoundaryPriVars(const Problem& problem,
                                   const CellCenterPrimaryVariables& boundaryFaceInsideSol,
                                   const SubControlVolumeFace& boundaryFace,
                                   const VolumeVariables& volVars)
    {
        using PrimaryVariables = typename VolumeVariables::PrimaryVariables;
        using Indices = typename VolumeVariables::Indices;

        static constexpr auto dim = PrimaryVariables::dimension - CellCenterPrimaryVariables::dimension;
        static constexpr auto offset = dim;

        const auto bcTypes = problem.boundaryTypes(boundaryFace.inside(), boundaryFace);
        PrimaryVariables boundaryPriVars(0.0);

        // make sure to not use outflow BC for momentum balance
        for(int i = 0; i < dim; ++i)
        {
            if(bcTypes.isOutflow(Indices::velocity(i)))
                DUNE_THROW(Dune::InvalidStateException, "Outflow condition cannot be used for velocity. Set only a Dirichlet value for pressure instead.");
        }

        if(bcTypes.isOutflow(Indices::pressureIdx))
            DUNE_THROW(Dune::InvalidStateException, "Outflow condition cannot be used for pressure. Set only a Dirichlet value for velocity instead.");

        // Determine the pressure value at a boundary with a Dirichlet condition for velocity.
        // This just takes the value of the adjacent inner cell.
        if(bcTypes.isDirichlet(Indices::velocity(boundaryFace.directionIndex())))
        {
            if(bcTypes.isDirichlet(Indices::pressureIdx))
                DUNE_THROW(Dune::InvalidStateException, "A Dirichlet condition for velocity must not be combined with a Dirichlet condition for pressure");
            else
                boundaryPriVars[Indices::pressureIdx] = boundaryFaceInsideSol[Indices::pressureIdx - offset];
                // TODO: pressure could be extrapolated to the boundary
        }

        // Determine the pressure value for a boundary with a Dirichlet condition for pressure.
        // Takes a value specified in the problem.
        if(bcTypes.isDirichlet(Indices::pressureIdx))
        {
            if(bcTypes.isDirichlet(Indices::velocity(boundaryFace.directionIndex())))
                DUNE_THROW(Dune::InvalidStateException, "A Dirichlet condition for velocity must not be combined with a Dirichlet condition for pressure");
            else
                boundaryPriVars[Indices::pressureIdx] = problem.dirichlet(boundaryFace.inside(), boundaryFace)[Indices::pressureIdx];
        }

        // Return for isothermal single-phase systems ...
        if(CellCenterPrimaryVariables::dimension == 1)
            return boundaryPriVars;

        // ... or handle values for components, temperature, etc.
        for(int eqIdx = offset; eqIdx < PrimaryVariables::dimension; ++eqIdx)
        {
            if(eqIdx == Indices::pressureIdx)
                continue;

            if(bcTypes.isDirichlet(eqIdx))
                boundaryPriVars[eqIdx] = problem.dirichlet(boundaryFace.inside(), boundaryFace)[eqIdx];
            else if(bcTypes.isOutflow(eqIdx) || bcTypes.isSymmetry() || bcTypes.isNeumann(eqIdx))
                boundaryPriVars[eqIdx] = boundaryFaceInsideSol[eqIdx - offset];
        }
        return boundaryPriVars;
    }

    template <class VolumeVariables, class OppoType, class NormalType, class GridGeometry, class CellCenterPrimaryVariablesVector, class CellCenterPrimaryVariablesVectorsArray, class SubControlVolumeFace, class Problem>
    void updateDual(VolumeVariables& inside, OppoType/*VolumeVariables*/& virtualOppo, NormalType/*std::array<VolumeVariables, numPairs>*/& normal, const GridGeometry& gridGeometry,  const CellCenterPrimaryVariablesVector& insideScvSols, const CellCenterPrimaryVariablesVectorsArray& normalFaceOutsideSols, const SubControlVolumeFace& scvf, const Problem& problem)
    {
        //inside
        updateCertainVolVar(inside, gridGeometry, insideScvSols, problem, scvf.volVarsData().innerVolVarsDofs, scvf.volVarsData().innerVolVarsDofsInterpolationFactors, scvf.volVarsData().innerVolVarsScvfBuildingData);

        using PrimaryVariables = typename VolumeVariables::PrimaryVariables;
        using Scalar = typename GridGeometry::GridView::ctype;
        using Element = typename GridGeometry::GridView::template Codim<0>::Entity;
        using ElementSolution = StaggeredElementSolution<PrimaryVariables>;
        using SubControlVolume = typename GridGeometry::SubControlVolume;

        static constexpr auto dimWorld = GridGeometry::GridView::dimensionworld;
        static constexpr int numPairs = 2 * (dimWorld - 1);

        //outside, can be completely without interpolations, is for mass balance only
        if (scvf.boundary())
        {
            auto outsideBoundaryPriVars = getBoundaryPriVars(problem, insideScvSols[0], scvf, inside/*only workaround to get a type*/);
            const auto outsideElemSol = elementSolution<typename GridGeometry::LocalView>(std::move(outsideBoundaryPriVars));

            std::vector<ElementSolution> outsideElemSols;
            outsideElemSols.push_back(outsideElemSol);
            std::vector<Scalar> outsideInterpolationFactors;
            outsideInterpolationFactors.push_back(1.);

            std::vector<Element> insideElements;
            const auto& insideScv = gridGeometry.scv(scvf.insideScvIdx());
            const auto& insideElement = gridGeometry.element(insideScv.elementIndex());
            insideElements.push_back(insideElement);
            std::vector<SubControlVolume> insideScvs;
            insideScvs.push_back(insideScv);

            virtualOppo.adaptiveUpdate(outsideElemSols, problem, insideElements, insideScvs, outsideInterpolationFactors);
        }

        //normal
        for (unsigned int i = 0; i < numPairs; ++i)
        {
            updateCertainVolVar(normal[i], gridGeometry, normalFaceOutsideSols[i], problem, scvf.volVarsData().normalVolVarsDofs[i], scvf.volVarsData().normalVolVarsDofsInterpolationFactors[i], scvf.volVarsData().normalVolVarsScvfBuildingData[i]);
        }
    }

    template <class NaviesStokesPrimalScvfPair, class GridGeometry, class CellCenterPrimaryVariablesVector, class CellCenterPrimaryVariablesArray, class SubControlVolumeFace, class Problem>
    void updatePrimal(NaviesStokesPrimalScvfPair& scvfVolVarsPair, const GridGeometry& gridGeometry, const CellCenterPrimaryVariablesVector & insideScvSols, const CellCenterPrimaryVariablesArray& outsideScvSols, const SubControlVolumeFace& scvf, const Problem& problem)
    {
        updateCertainVolVar(scvfVolVarsPair.inside, gridGeometry, insideScvSols, problem, scvf.volVarsData().innerVolVarsDofs, scvf.volVarsData().innerVolVarsDofsInterpolationFactors, scvf.volVarsData().innerVolVarsScvfBuildingData);
        updateCertainVolVar(scvfVolVarsPair.outside, gridGeometry, outsideScvSols, problem, scvf.volVarsData().outerVolVarsDofs, scvf.volVarsData().outerVolVarsDofsInterpolationFactors, scvf.volVarsData().outerVolVarsScvfBuildingData);
    }


    template <class VolumeVariables, class GridGeometry, class CellCenterPrimaryVariablesVector, class Problem, class Dofs, class Facts, class ScvfBuildingData>
    void updateCertainVolVar(VolumeVariables& volVar, const GridGeometry& gridGeometry, const CellCenterPrimaryVariablesVector & sols, const Problem& problem, const Dofs& dofs, const Facts& interpFacts,const ScvfBuildingData& scvfBuildingData)
    {
        using PrimaryVariables = typename VolumeVariables::PrimaryVariables;
        using Scalar = typename GridGeometry::GridView::ctype;
        using Element = typename GridGeometry::GridView::template Codim<0>::Entity;
        using ElementSolution = StaggeredElementSolution<PrimaryVariables>;
        using SubControlVolume = typename GridGeometry::SubControlVolume;

        std::vector<ElementSolution> insideElemSols;
        std::vector<Element> insideElements;
        std::vector<SubControlVolume> insideScvs;
        std::vector<Scalar> insideInterpolationFactors;

        if (!(dofs.size()>0))
        {
            DUNE_THROW(Dune::InvalidStateException, "should not happen");
        }

        for (unsigned int i = 0; i < dofs.size(); ++i)
        {
            const auto scvIdx = dofs[i];
            const auto interpFact = interpFacts[i];

            if (! (scvIdx < 0))
            {
                const auto& insideScv = gridGeometry.scv(scvIdx);
                const auto& insideElement = gridGeometry.element(insideScv.elementIndex());
                // construct a privars object from the cell center solution vector
                const auto& cellCenterPriVarsInside = sols[i];
                PrimaryVariables priVarsInside = makePriVarsFromCellCenterPriVars<PrimaryVariables>(cellCenterPriVarsInside);
                auto insideElemSol = elementSolution<typename GridGeometry::LocalView>(std::move(priVarsInside));

                insideElemSols.push_back(insideElemSol);
                insideElements.push_back(insideElement);
                insideScvs.push_back(insideScv);
                insideInterpolationFactors.push_back(interpFact);
            }
            else
            {
                const auto& boundaryFace = gridGeometry.scvf(scvfBuildingData[i].eIdx, scvfBuildingData[i].localScvfIdx);

                const auto& insideScvForBoundary = gridGeometry.scv(boundaryFace.insideScvIdx());
                const auto& insideElement = gridGeometry.element(insideScvForBoundary.elementIndex());

                auto boundaryPriVars = getBoundaryPriVars(problem, sols[i], boundaryFace, volVar/*only workaround to get a type*/);
                const auto boundaryElemSol = elementSolution<typename GridGeometry::LocalView>(std::move(boundaryPriVars));

                insideElemSols.push_back(boundaryElemSol);
                insideElements.push_back(insideElement);
                insideScvs.push_back(insideScvForBoundary);
                insideInterpolationFactors.push_back(interpFact);
            }
        }

        volVar.adaptiveUpdate(insideElemSols, problem, insideElements, insideScvs, insideInterpolationFactors);
    }
} // end namespace Dumux

#endif
