// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \ingroup StaggeredDiscretization
 * \copydoc Dumux::MyFreeFlowStaggeredGeometryHelper
 */
#ifndef DUMUX_DISCRETIZATION_MY_STAGGERED_GEOMETRY_HELPER_HH
#define DUMUX_DISCRETIZATION_MY_STAGGERED_GEOMETRY_HELPER_HH

#include <dune/geometry/multilineargeometry.hh>
#include <dune/geometry/referenceelements.hh>

#include <dumux/common/math.hh>
#include <type_traits>
#include <algorithm>

#include <dumux/discretization/staggered/freeflow/staggeredgeometryhelper.hh>
#include <dumux/discretization/staggered/freeflow/geometryhelper/staggereddata.hh>

#include <dumux/discretization/staggered/freeflow/geometryhelper/geometryhelpergenericmethods.hh>
#include <dumux/discretization/staggered/freeflow/geometryhelper/geometryhelperutility.hh>

#include <dumux/discretization/staggered/freeflow/geometryhelper/handlers/staggeredpairdatahandler.hh>
#include <dumux/discretization/staggered/freeflow/geometryhelper/handlers/staggeredinneroutervolvarshandler.hh>
#include <dumux/discretization/staggered/freeflow/geometryhelper/handlers/staggerednormalvolvarshandler.hh>
#include <dumux/discretization/staggered/freeflow/geometryhelper/handlers/staggeredaxisdatahandler.hh>
#include <dumux/discretization/staggered/freeflow/geometryhelper/interpolationhelper.hh>

namespace Dumux
{
/*!
 * \ingroup StaggeredDiscretization
 * \brief Helper class constructing the dual grid finite volume geometries
 *        for the free flow staggered discretization method.
 */
template<class GridView, class IntersectionMapper>
class MyFreeFlowStaggeredGeometryHelper
{
    using Scalar = typename GridView::ctype;
    static constexpr int dim = GridView::dimension;
    static constexpr int dimWorld = GridView::dimensionworld;

    using ScvGeometry = Dune::CachedMultiLinearGeometry<Scalar, dim, dimWorld>;
    using ScvfGeometry = Dune::CachedMultiLinearGeometry<Scalar, dim-1, dimWorld>;

    using GlobalPosition = typename ScvGeometry::GlobalCoordinate;

    using Element = typename GridView::template Codim<0>::Entity;
    using Intersection = typename GridView::Intersection;

    using ReferenceElements = typename Dune::ReferenceElements<Scalar, dim>;
    using GridIndexType = typename GridView::IndexSet::IndexType;

    //TODO include assert that checks for quad geometry
    static constexpr int numPairs = 2 * (dimWorld - 1);
    static int order_;

public:
    using PairData = MyPairData<Scalar, GlobalPosition>;
    using AxisData = MyAxisData<Scalar>;

    MyFreeFlowStaggeredGeometryHelper( const Intersection& intersection,
                                       const GridView& gridView,
                                       std::shared_ptr<const IntersectionMapper> intersectionMapper,
                                       const std::vector<std::pair<GridIndexType, GlobalPosition>>& velocityXPositions,
                                       const std::vector<std::pair<GridIndexType, GlobalPosition>>& velocityYPositions,
                                       bool higherOrderInterpolation,
                                       bool linearInterpolation,
                                       bool useConservation)
        : intersection_(intersection),
          element_(intersection_.inside()),
          elementGeometry_(element_.geometry()),
          gridView_(gridView),
          intersectionMapper_(intersectionMapper),
          velocityXPositions_(velocityXPositions),
          velocityYPositions_(velocityYPositions),
          utility_(gridView, intersection),
          interpUtility_(gridView, velocityXPositions, velocityYPositions),
          higherOrderInterpolation_(higherOrderInterpolation),
          linearInterpolation_(linearInterpolation),
          useConservation_(useConservation)
          {}

    //! update the local face
    void update()
    {
        utility_.setIntersection(intersection_);

        //Fill pair data
        StaggeredPairDataHandler pairDataHandler(utility_, gridView_, intersectionMapper_,
                                                    element_, intersection_, pairData_,
                                                    normalPairGlobalPositions_, parallelGlobalPositions_, this, axisData_, useConservation_);
        pairDataHandler.fillPairData_();
        pairData_ = pairDataHandler.getPairData();
        normalPairGlobalPositions_ = pairDataHandler.getNormalPairGlobalPositions();
        parallelGlobalPositions_ = pairDataHandler.getParallelGlobalPositions();
        axisData_ = pairDataHandler.getAxisData();


        //Fill axis data
        StaggeredAxisDataHandler axisDataHandler(utility_, gridView_, intersectionMapper_, element_, intersection_, axisData_, oppositePosition_);
        axisDataHandler.fillAxisData_();
        axisData_ = axisDataHandler.getAxisData();
        oppositePosition_ = axisDataHandler.getOppositePosition();

        //Fill inner outer volume variables
        StaggeredInnerOuterVolVarsHandler innerOuterVolVarsHandler(utility_, gridView_, intersectionMapper_, element_, intersection_, linearInterpolation_);
        innerOuterVolVarsHandler.fillInnerOrOuterVolVarsData_(volVarsData_.innerVolVarsDofs, volVarsData_.innerVolVarsDofsInterpolationFactors, volVarsData_.innerVolVarsScvfBuildingData, 0/*in*/);
        innerOuterVolVarsHandler.fillInnerOrOuterVolVarsData_(volVarsData_.outerVolVarsDofs, volVarsData_.outerVolVarsDofsInterpolationFactors, volVarsData_.outerVolVarsScvfBuildingData, 1/*out*/);//empty if intersection_ at boundary

        //Fill normal volume variables
        StaggeredNormalVolumeVarsHandler normalVolVarsHandler(utility_, gridView_, intersectionMapper_, element_, intersection_, volVarsData_, linearInterpolation_);
        normalVolVarsHandler.fillNormalVolVarsData_();
        volVarsData_ = normalVolVarsHandler.getVolVarsData();

        if (higherOrderInterpolation_)
        {
            interpUtility_.replaceDofsHigherOrder_(axisData_.oppositeDofs, axisData_.oppositeDofsInterpolationFactors, oppositePosition_, directionIndex());

            for (unsigned int i = 0; i < numPairs; ++i)
            {
                interpUtility_.replaceDofsHigherOrder_(pairData_[i].parallelDofs[0],pairData_[i].parallelDofsInterpolationFactors[0],parallelGlobalPositions_[i], directionIndex());
                for (const auto nonDirectionIdx : nonDirectionIndices())
                {
                    interpUtility_.replaceDofsHigherOrder_(pairData_[i].normalPair.first,pairData_[i].normalPairInterpolationFactors.first,normalPairGlobalPositions_[i].first, nonDirectionIdx);
                    interpUtility_.replaceDofsHigherOrder_(pairData_[i].normalPair.second,pairData_[i].normalPairInterpolationFactors.second,normalPairGlobalPositions_[i].second, nonDirectionIdx);
                }
            }
        }
        // printDataForDebug();
    }

    /*!
     * \brief Returns the local index of the face (i.e. the intersection)
     */
    int localFaceIndex() const
    {
        return intersectionMapper_->isIndexInInside(intersection_);
    }

    /*!
     * \brief Returns the local indices of the opposing faces
     */
    std::vector<int> localIndicesOpposingFace() const
    {
        const auto inIdx = intersectionMapper_->isIndexInInside(intersection_);
        return localOppositeIndices_(inIdx, this->element_);
    }

    /*!
     * \brief Returns a copy of the axis data
     */
    auto axisData() const
    {
        return axisData_;
    }

    /*!
     * \brief Returns a copy of the pair data
     */
    auto pairData() const
    {
        return pairData_;
    }

    /*!
     * \brief Returns a copy of the volume variables data
     */
    auto volVarsData() const
    {
        return volVarsData_;
    }

    /*!
     * \brief Returns the dirction index of the primary facet (0 = x, 1 = y, 2 = z)
     */
    int directionIndex() const
    {
        return Dumux::directionIndex(std::move(intersection_.centerUnitOuterNormal()));
    }

    std::array<int, dim-1> nonDirectionIndices() const
    {
        return utility_.nonDirectionIndices();
    }

    /*!
     * \brief Returns the dirction index of the facet passed as an argument (0 = x, 1 = y, 2 = z)
     */
    int directionIndex(const Intersection& intersection) const
    {
        return utility_.directionIndex(intersection);
    }

    //! \brief Returns the order needed by the scheme
    static int order()
    {
        return order_;
    }

    //! \brief Set the order needed by the scheme
    static void setOrder(const int order)
    {
        if (order != 1)
        {
            DUNE_THROW(Dune::InvalidStateException, "Adaptive only works for first order.");
        }
        order_ = order;
    }

private:
 void printDataForDebug() {
     std::cout << std::endl
               << "element center = " << element_.geometry().center() << ", intersection center = " << intersection_.geometry().center() << std::endl;
     for (unsigned int i = 0; i < numPairs; ++i) {
         std::cout << "pairData of pair " << i << ":" << std::endl;

         std::cout << "parallel dofs = ";
         for (const auto& prallelDof : pairData_[i].parallelDofs[0 /*order*/]) {
             std::cout << prallelDof << ", ";
         }
         std::cout << std::endl;
         std::cout << "parallelGlobalPositions_ = " << parallelGlobalPositions_[i] << std::endl;

         std::cout << "parallelSelfDistance = " << pairData_[i].parallelDistances[0] << std::endl;
         std::cout << "parallelNextDistance = " << pairData_[i].parallelDistances[1] << std::endl;

         std::cout << "normalfirst = ";
         for (const auto& normalFirstDof : pairData_[i].normalPair.first) {
             std::cout << normalFirstDof << ", ";
         }
         std::cout << std::endl;
         std::cout << "first normal pos = " << normalPairGlobalPositions_[i].first << std::endl;

         std::cout << "normalsecond = ";
         for (const auto& normalSecondDof : pairData_[i].normalPair.second) {
             std::cout << normalSecondDof << ", ";
         }
         std::cout << std::endl;
         std::cout << "second normal pos = " << normalPairGlobalPositions_[i].second << std::endl;

         std::cout << "localNormalFluxCorrectionIndex = " << pairData_[i].localNormalFluxCorrectionIndex << std::endl;

         std::cout << "normalDistance = " << pairData_[i].normalDistance << std::endl;

         std::cout << "virtualFirstParallelFaceDofPos = " << pairData_[i].virtualFirstParallelFaceDofPos << std::endl;
     }

     std::cout << "axis data:" << std::endl;

     std::cout << "self dof = " << axisData_.selfDof << std::endl;

     std::cout << "opposite dofs = ";
     for (const auto& oppositeDof : axisData_.oppositeDofs) {
         std::cout << oppositeDof << ", ";
     }
     std::cout << std::endl;
     std::cout << "oppositePosition_= " << oppositePosition_ << std::endl;

     std::cout << "inAxisForwardDofs = ";
     for (const auto& inAxisForwardDof : axisData_.inAxisForwardDofs) {
         std::cout << inAxisForwardDof << ", ";
     }
     std::cout << std::endl;

     std::cout << "inAxisBackwardDofs = ";
     for (const auto& inAxisBackwardDof : axisData_.inAxisBackwardDofs) {
         std::cout << inAxisBackwardDof << ", ";
     }
     std::cout << std::endl;

     std::cout << "selfToOppositeDistance = " << axisData_.selfToOppositeDistance << std::endl;

     std::cout << "inAxisForwardDistances = ";
     for (const auto& inAxisForwardDistance : axisData_.inAxisForwardDistances) {
         std::cout << inAxisForwardDistance << ", ";
     }
     std::cout << std::endl;

     std::cout << "inAxisBackwardDistances = ";
     for (const auto& inAxisBackwardDistance : axisData_.inAxisBackwardDistances) {
         std::cout << inAxisBackwardDistance << ", ";
     }
     std::cout << std::endl;

     //check sums of interpolation factors
     Scalar sum = 0.;
     std::cout << "oppoFactors ";
     for (const auto& factor : axisData_.oppositeDofsInterpolationFactors) {
         std::cout << factor << ", ";
         sum += factor;
     }
     std::cout << std::endl;
     if (!scalarCmp(sum, 1.)) {
         DUNE_THROW(Dune::InvalidStateException, "Opposite dofs interpolation factors have to sum up to one, but they sum up to" << sum << ".");
     }

     for (const auto& data : pairData_) {
         sum = 0.;
         std::cout << "normalPairFirstFactors ";
         for (const auto& factor : data.normalPairInterpolationFactors.first) {
             std::cout << factor << ", ";
             sum += factor;
         }
         std::cout << std::endl;
         if (!scalarCmp(sum, 1.)) {
             DUNE_THROW(Dune::InvalidStateException, "Normal pair interpolation factors have to sum up to one, but they sum up to" << sum << ".");
         }
     }

     if (intersection_.neighbor()) {
         for (const auto& data : pairData_) {
             sum = 0.;
             std::cout << "normalPairsecondFactors ";
             for (const auto& factor : data.normalPairInterpolationFactors.second) {
                 std::cout << factor << ", ";
                 sum += factor;
             }
             std::cout << std::endl;
             if (!scalarCmp(sum, 1.)) {
                 DUNE_THROW(Dune::InvalidStateException, "Normal pair interpolation factors have to sum up to one, but they sum up to" << sum << ".");
             }
         }
     }

     for (const auto& data : pairData_) {
         for (unsigned int i = 0; i < data.parallelDofsInterpolationFactors.size(); ++i) {
             sum = 0.;

             std::cout << "parallelFactors ";
             for (const auto& factor : data.parallelDofsInterpolationFactors[i]) {
                 std::cout << factor << ", ";
                 sum += factor;
             }
             std::cout << std::endl;

             if (!scalarCmp(sum, 1.0) && data.parallelDofs[i][0] != -1) {
                 DUNE_THROW(Dune::InvalidStateException, "Parallel pair interpolation factors have to sum up to one, but they sum up to" << sum << ".");
             }
         }
     }

     std::cout << "innerVolVarsDofs = ";
     for (const auto& innerVolVarDof : volVarsData_.innerVolVarsDofs) {
         std::cout << innerVolVarDof << ", ";
     }
     std::cout << std::endl;

     std::cout << "innerVolVarsDofsInterpolationFactors = ";
     sum = 0.;
     for (const auto& factor : volVarsData_.innerVolVarsDofsInterpolationFactors) {
         std::cout << factor << ",";
         sum += factor;
     }
     std::cout << std::endl;

     if (!scalarCmp(sum, 1.0)) {
         DUNE_THROW(Dune::InvalidStateException, "vol vars inner interpolation factors have to sum up to one, but they sum up to" << sum << ".");
     }

     //         std::cout << "outerVolVarsDofs = ";
     //         for (const auto& outerVolVarDof : volVarsData_.outerVolVarsDofs)
     //         {
     //             std::cout << outerVolVarDof << ", ";
     //         }
     //         std::cout << std::endl;
     //
     //         std::cout << "outerVolVarsDofsInterpolationFactors = ";
     //         sum = 0.;
     //         for (const auto& factor : volVarsData_.outerVolVarsDofsInterpolationFactors)
     //         {
     //             std::cout << factor << ",";
     //             sum += factor;
     //         }
     //         std::cout << std::endl;
     //
     //         if (!scalarCmp(sum, 1.0))
     //         {
     //             DUNE_THROW(Dune::InvalidStateException, "vol vars outer interpolation factors have to sum up to one, but they sum up to" << sum << ".");//empty if intersection at boundary
     //         }

     std::cout << "normalVolVarsDofs = ";
     for (const auto& normalVolVarDofs : volVarsData_.normalVolVarsDofs) {
         std::cout << std::endl;
         for (const auto& normalVolVarDof : normalVolVarDofs)
             std::cout << normalVolVarDof << ", ";
     }
     std::cout << std::endl;

     std::cout << "normalVolVarsDofsInterpolationFactors = ";
     for (const auto& factors : volVarsData_.normalVolVarsDofsInterpolationFactors) {
         std::cout << std::endl;
         sum = 0.;
         for (const auto& factor : factors) {
             std::cout << factor << ",";
             sum += factor;
         }
         std::cout << ", sum is " << sum << std::endl;
     }
     std::cout << std::endl;

     if (!scalarCmp(sum, 1.0)) {
         DUNE_THROW(Dune::InvalidStateException, "vol vars normal interpolation factors have to sum up to one, but they sum up to" << sum << ".");
     }
 }

    Scalar quadraticInterpolationFactor_(Scalar ownDistance, Scalar otherDistance1, Scalar otherDistance2)
    {
        return utility_.quadraticInterpolationFactor_(ownDistance, otherDistance1, otherDistance2);
    }

    /*!
     * \brief Returns the local opposing intersection index
     *
     * \param idx The local index of the intersection itself
     */
    std::vector<int> localOppositeIndices_(const int idx, const Element& element) const
    {
        return utility_.localOppositeIndices_(idx, element);
    }

    //returns the index of self and of parallelSelf
    std::vector<int> localSelfIndices_(const int idx, const Element& element) const
    {
        return utility_.localSelfIndices_(idx, element);
    }

    Intersection getFacet_(const int localFacetIdx, const Element& element) const
    {
        return utility_.getFacet_(localFacetIdx, element);
    };

    Intersection intersection_; //!< The intersection of interest
    const Element element_; //!< The respective element
    const typename Element::Geometry elementGeometry_; //!< Reference to the element geometry
    const GridView gridView_; //!< The grid view
    std::shared_ptr<const IntersectionMapper> intersectionMapper_;
    MyAxisData<Scalar> axisData_; //!< Data related to forward and backward faces
    std::array<MyPairData<Scalar, GlobalPosition>, numPairs> pairData_; //!< Collection of pair information related to normal and parallel faces
    MyVolVarsData<Scalar, numPairs> volVarsData_;
    std::array<std::pair<GlobalPosition,GlobalPosition>, numPairs> normalPairGlobalPositions_;
    std::array<GlobalPosition, numPairs> parallelGlobalPositions_;
    GlobalPosition oppositePosition_;
    std::vector<std::pair<GridIndexType, GlobalPosition>> velocityXPositions_;
    std::vector<std::pair<GridIndexType, GlobalPosition>> velocityYPositions_;
    GeometryHelperUtility<GridView> utility_;
    InterpolationHelper<GridView> interpUtility_;
    bool higherOrderInterpolation_;
    bool linearInterpolation_;
    bool useConservation_;
};

template<class GridView, class IntersectionMapper>
int MyFreeFlowStaggeredGeometryHelper<GridView, IntersectionMapper>::order_ = 1;

} // end namespace Dumux

#endif
