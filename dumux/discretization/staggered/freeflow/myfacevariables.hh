// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \ingroup StaggeredDiscretization
 * \copydoc Dumux::MyStaggeredFaceVariables
 */
#ifndef DUMUX_DISCRETIZATION_STAGGERED_FREEFLOW_MY_FACEVARIABLES_HH
#define DUMUX_DISCRETIZATION_STAGGERED_FREEFLOW_MY_FACEVARIABLES_HH

#include <array>
#include <vector>

namespace Dumux
{

/*!
 * \ingroup StaggeredDiscretization
 * \brief The face variables class for free flow staggered grid models.
 *        Contains all relevant velocities for the assembly of the momentum balance.
 */
template<class ModelTraits, class FacePrimaryVariables, int dim, int upwindSchemeOrder>
class MyStaggeredFaceVariables
{
    static constexpr int numPairs = (dim == 2) ? 2 : 4;
    using Scalar = typename FacePrimaryVariables::block_type;

public:

    /*!
    * \brief Partial update of the face variables. Only the face itself is considered.
    *
    * \param priVars The face-specific primary variales
    */
    void updateOwnFaceOnly(const FacePrimaryVariables& priVars)
    {
        velocitySelf_ = priVars[0];
    }

    /*!
    * \brief Complete update of the face variables (i.e. velocities for free flow)
    *        for a given face
    *
    * \param faceSol The face-specific solution vector
    * \param problem The problem
    * \param element The element
    * \param fvGeometry The finite-volume geometry
    * \param scvf The sub-control volume face of interest
    */
    template<class SolVector, class Problem, class Element,
             class FVElementGeometry, class SubControlVolumeFace>
    void update(const SolVector& faceSol,
                const Problem& problem,
                const Element& element,
                const FVElementGeometry& fvGeometry,
                const SubControlVolumeFace& scvf)
    {
        using Indices = typename ModelTraits::Indices;

        velocitySelf_ = faceSol[scvf.dofIndex()];

        velocityOpposite_ = 0;
        unsigned int wasMinusOne = 0;
        unsigned int wasMinusOneIndex = 0; //I will only use it if I explicitly set it, so I can initialize without worry

        for (unsigned int i = 0; i < scvf.dofIndicesOpposingFace().size(); ++i)
        {
            if (!(scvf.dofIndicesOpposingFace()[i] < 0))
            {
                if (!scvf.axisData().oppositeDofsInterpolationFactors.size() == scvf.dofIndicesOpposingFace().size())
                {
                    DUNE_THROW(Dune::InvalidStateException, "Sizes do not match for scvf center = " << scvf.center());
                }

                velocityOpposite_ += faceSol[scvf.dofIndicesOpposingFace()[i]]*scvf.axisData().oppositeDofsInterpolationFactors[i] ;
            }
            else
            {
                ++wasMinusOne;
                wasMinusOneIndex = i;
            }
        }
        if (wasMinusOne > 0)
        {
            if ((scvf.dofIndicesOpposingFace().size() == 2) && (wasMinusOne == 1))
            {
                SubControlVolumeFace oppoScvf;
                auto fvGeometry = localView(problem.gridGeometry());
                fvGeometry.bindElement(scvf.inside());
                unsigned int numberOfTrues = 0;
                for (auto&& myscvf : scvfs(fvGeometry))
                {
                    auto minusNormal = scvf.unitOuterNormal();
                    minusNormal *= -1.;
                    if (containerCmp(myscvf.unitOuterNormal(),minusNormal))
                    {
                        oppoScvf = myscvf;
                        ++numberOfTrues;
                    }
                }
                if (numberOfTrues != 1)
                    DUNE_THROW(Dune::InvalidStateException, "Expected to be true exactly once.");

                std::size_t goodIdx = (wasMinusOneIndex == 0) ? 1 : 0;

                bool isDirichletPressure = false; // check for Dirichlet boundary condition for the pressure
                bool isBJS = false; // check for Beavers-Joseph-Saffman boundary condition

                bool localSubFaceIdxSet = false;
                int localSubFaceIdx;
                const int numSubFaces = oppoScvf.pairData().size();
                for(int i = 0; i < numSubFaces; ++i)
                {
                    if(!oppoScvf.hasParallelNeighbor(i,0))
                    {
                        localSubFaceIdx = i;
                        localSubFaceIdxSet = true;
                    }
                }
                if (!localSubFaceIdxSet)
                    DUNE_THROW(Dune::InvalidStateException, "bad");

                const SubControlVolumeFace& boundaryNormalFace = problem.gridGeometry().scvf(oppoScvf.insideScvIdx(), oppoScvf.pairData(localSubFaceIdx).localNormalFluxCorrectionIndex);
                auto boundarySubFaceCenter = oppoScvf.pairData(localSubFaceIdx).virtualFirstParallelFaceDofPos + boundaryNormalFace.center();
                boundarySubFaceCenter *= 0.5;
                const SubControlVolumeFace boundarySubFace = makeStaggeredBoundaryFace(boundaryNormalFace, boundarySubFaceCenter);

                const auto bcTypes = problem.boundaryTypes(element, boundarySubFace);

                if (bcTypes.isDirichlet(Indices::pressureIdx))
                    isDirichletPressure = true;
                else if (bcTypes.isBeaversJoseph(Indices::velocity(oppoScvf.directionIndex())))
                    isBJS = true;
                else if (bcTypes.isDirichlet(Indices::velocity(oppoScvf.directionIndex())) == false)
                    DUNE_THROW(Dune::InvalidStateException,  "Something went wrong with the boundary conditions "
                           "for the momentum equations at global position " << boundarySubFaceCenter);

                const auto eIdx = oppoScvf.insideScvIdx();
                const auto& normalFace = fvGeometry.scvf(eIdx, oppoScvf.pairData(localSubFaceIdx).localNormalFluxCorrectionIndex);

                velocityOpposite_ = 0.5 * (faceSol[scvf.dofIndicesOpposingFace()[goodIdx]] +
                                            getParallelVelocityFromBoundary_(problem,fvGeometry, oppoScvf,normalFace,velocitySelf_,localSubFaceIdx,element,isDirichletPressure,isBJS));
            }
            else
            {
                DUNE_THROW(Dune::InvalidStateException, "Not prepared for this situation.");
            }
        }

        // treat the velocity forward of the self face i.e. the face that is
        // forward wrt the self face by degree i
        velocityForward_.clear();
        for (int i = 0; i < scvf.axisData().inAxisForwardDofs.size(); i++)
        {
             if(!(scvf.axisData().inAxisForwardDofs[i] < 0))
             {
                 //TODO adapt for higher order
                 velocityForward_.push_back(faceSol[scvf.axisData().inAxisForwardDofs[i]]);
             }
        }

        // treat the velocity at the first backward face i.e. the face that is
        // behind the opposite face by degree i
        velocityBackward_.clear();
        for (int i = 0; i < scvf.axisData().inAxisBackwardDofs.size(); i++)
        {
             if(!(scvf.axisData().inAxisBackwardDofs[i] < 0))
             {
                 //TODO adapt for higher order
                 velocityBackward_.push_back(faceSol[scvf.axisData().inAxisBackwardDofs[i]]);
             }
        }

        // handle all sub faces
        std::vector<Scalar> a;
        for(int i = 0; i < scvf.pairData().size(); ++i)
        {
            const auto& subFaceData = scvf.pairData(i);

            // treat the velocities normal to the face
            velocityNormalInside_[i] = 0;
            for (unsigned int j = 0; j < subFaceData.normalPair.first.size(); ++j)
            {
                if (!(subFaceData.normalPair.first[j] < 0))
                {
                    velocityNormalInside_[i] += faceSol[subFaceData.normalPair.first[j]] * subFaceData.normalPairInterpolationFactors.first[j];
                }
                else
                {
                    DUNE_THROW(Dune::InvalidStateException, "Not prepared for this case");
                }
            }

            if(scvf.hasOuterNormal(i))
            {
                velocityNormalOutside_[i] = 0;
                for (unsigned int j = 0; j < subFaceData.normalPair.second.size(); ++j)
                {
                    if (!(subFaceData.normalPair.second[j] < 0))
                    {
                        velocityNormalOutside_[i] += faceSol[subFaceData.normalPair.second[j]] * subFaceData.normalPairInterpolationFactors.second[j];
                    }
                    else
                    {
                        DUNE_THROW(Dune::InvalidStateException, "Not prepared for this case");
                    }
                }
            }

            a.clear();
            // treat the velocities parallel to the self face
            for(int j = 0; j < subFaceData.parallelDofs.size(); j++)
            {
                if(scvf.hasParallelNeighbor(i,j))
                {
                    Scalar velParal = 0;
                    for (unsigned int k = 0; k < subFaceData.parallelDofs[j].size(); ++k)
                    {
                        if (!(subFaceData.parallelDofs[j][k] < 0))
                        {
                            if (j == 0)
                                velParal += faceSol[subFaceData.parallelDofs[j][k]] * subFaceData.parallelDofsInterpolationFactors[j][k];
                            else
                                velParal += faceSol[subFaceData.parallelDofs[j][k]];
                        }
                        else
                        {
                            DUNE_THROW(Dune::InvalidStateException, "Not prepared for this case");
                        }
                    }

                    a.push_back(velParal);
                }
            }
            velocityParallel_[i] = a;
        }
    }

    /*!
    * \brief Returns the velocity at the face itself
    */
    Scalar velocitySelf() const
    {
        return velocitySelf_;
    }

    /*!
    * \brief Returns the velocity at the opposing face
    */
    Scalar velocityOpposite() const
    {
        return velocityOpposite_;
    }

    /*!
    * \brief Returns the velocity at a backward face
    *
    * \param backwardIdx The index describing how many faces backward this dof is from the opposite face
    */
    Scalar velocityBackward(const int backwardIdx) const
    {
        return velocityBackward_[backwardIdx];
    }

    /*!
    * \brief Returns the velocity at a forward face
    *
    * \param forwardIdx The index describing how many faces forward this dof is of the self face
    */
    Scalar velocityForward(const int forwardIdx) const
    {
        return velocityForward_[forwardIdx];
    }

    /*!
    * \brief Returns the velocity at a parallel face
    *
    * \param localSubFaceIdx The local index of the subface
    * \param parallelDegreeIdx The index describing how many faces parallel this dof is of the parallel face
    */
    Scalar velocityParallel(const int localSubFaceIdx, const int parallelDegreeIdx) const
    {
        if (velocityParallel_[localSubFaceIdx].size() <= parallelDegreeIdx)
            DUNE_THROW(Dune::InvalidStateException, "parallelDegreeIdx = " << parallelDegreeIdx);

        return velocityParallel_[localSubFaceIdx][parallelDegreeIdx];
    }

    /*!
    * \brief Returns the velocity at the inner normal face
    *
    * \param localSubFaceIdx The local index of the subface
    */
    Scalar velocityNormalInside(const int localSubFaceIdx) const
    {
        return velocityNormalInside_[localSubFaceIdx];
    }

    /*!
    * \brief Returns the velocity at the outer normal face
    *
    * \param localSubFaceIdx The local index of the subface
    */
    Scalar velocityNormalOutside(const int localSubFaceIdx) const
    {
        return velocityNormalOutside_[localSubFaceIdx];
    }

private:
    /*!
     * \brief Return the outer parallel velocity for normal faces that are on the boundary and therefore have no neighbor.
     *
     * Calls the problem to retrieve a fixed value set on the boundary.
     *
     * \param problem The problem
     * \param scvf The SubControlVolumeFace that is normal to the boundary
     * \param normalFace The face at the boundary
     * \param velocitySelf the velocity at scvf
     * \param localSubFaceIdx The local index of the face that is on the boundary
     * \param element The element that is on the boundary
     * \param isDirichletPressure @c true if there is a dirichlet condition for the pressure on the boundary
     * \param isBJS @c true if there is a BJS condition fot the velocity on the boundary
     */
    template <class Problem, class FVElementGeometry, class SubControlVolumeFace, class Scalar, class Element>
    Scalar getParallelVelocityFromBoundary_(const Problem& problem,
                                            const FVElementGeometry& fvGeometry,
                                            const SubControlVolumeFace& scvf,
                                            const SubControlVolumeFace& normalFace,
                                            const Scalar velocitySelf,
                                            const int localSubFaceIdx,
                                            const Element& element,
                                            const bool isDirichletPressure,
                                            const bool isBJS) const
    {
        // If there is a Dirichlet condition for the pressure we assume zero gradient for the velocity,
        // so the velocity at the boundary equal to that on the scvf.
        if (isDirichletPressure)
            return velocitySelf;

        if (isBJS)
        {
            const auto tangentialVelocityGradient = [&]()
            {
                // If the current scvf is on a boundary and if a Dirichlet BC for the pressure or a BJ condition for
                // the slip velocity is set there, assume a tangential velocity gradient of zero along the lateral face
                // (towards the current scvf).
                static const bool unsymmetrizedGradientForBJ = getParamFromGroup<bool>(problem.paramGroup(),
                                                            "FreeFlow.EnableUnsymmetrizedVelocityGradientForBeaversJoseph", false);

                if (unsymmetrizedGradientForBJ)
                    return 0.0;
                else
                    DUNE_THROW(Dune::InvalidStateException, "not implemented for adaptive");
            }();

            return problem.beaversJosephVelocity(element, fvGeometry.scv(scvf.insideScvIdx()), scvf, normalFace, velocitySelf, tangentialVelocityGradient);
        }

        const auto ghostFace = makeStaggeredBoundaryFace(normalFace, scvf.pairData(localSubFaceIdx).virtualFirstParallelFaceDofPos);

        return problem.dirichlet(element, ghostFace)[ModelTraits::Indices::velocity(scvf.directionIndex())];
    };

    Scalar velocitySelf_;
    Scalar velocityOpposite_;
    std::vector<Scalar> velocityForward_;
    std::vector<Scalar> velocityBackward_;
    std::array<std::vector<Scalar>, numPairs> velocityParallel_;
    std::array<Scalar, numPairs>  velocityNormalInside_;
    std::array<Scalar, numPairs>  velocityNormalOutside_;
};

} // end namespace Dumux

#endif // DUMUX_DISCRETIZATION_STAGGERED_FREEFLOW_FACEVARIABLES_HH
