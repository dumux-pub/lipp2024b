// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 3 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \ingroup Assembly
 * \brief Helper function to generate Jacobian pattern for different discretization methods
 */
#ifndef DUMUX_SIMPLE_JACOBIAN_PATTERN_HH
#define DUMUX_SIMPLE_JACOBIAN_PATTERN_HH

#include <type_traits>
#include <dune/istl/matrixindexset.hh>
#include <dumux/discretization/method.hh>

namespace Dumux {
/*!
 * \ingroup Assembly
 * \brief Helper function to generate Jacobian pattern for the staggered method
 */
template<bool isImplicit, class GridGeometry,
         typename std::enable_if_t<( (GridGeometry::discMethod == DiscretizationMethods::staggered) ), int> = 0>
auto getSimpleJacobianPattern(const GridGeometry& gridGeometry)
{
    // resize the jacobian and the residual
    const auto numDofs = gridGeometry.numDofs();
    Dune::MatrixIndexSet pattern(numDofs, numDofs);

    const auto& connectivityMap = gridGeometry.connectivityMap();

    // evaluate the acutal pattern
    for (const auto& element : elements(gridGeometry.gridView()))
    {
        if(gridGeometry.isCellCenter())
        {
//             // the global index of the element at hand
//             static constexpr auto cellCenterIdx = GridGeometry::cellCenterIdx();
//             const auto ccGlobalI = gridGeometry.elementMapper().index(element);
//             pattern.add(ccGlobalI, ccGlobalI);
//             for (auto&& ccGlobalJ : connectivityMap(cellCenterIdx, cellCenterIdx, ccGlobalI))
//                 pattern.add(ccGlobalI, ccGlobalJ);
        }
        else
        {
            static constexpr auto faceIdx = GridGeometry::faceIdx();
            auto fvGeometry = localView(gridGeometry);
            fvGeometry.bindElement(element);

            // loop over sub control faces
            for (auto&& scvf : scvfs(fvGeometry))
            {
                const auto faceGlobalI = scvf.dofIndex();
                pattern.add(faceGlobalI, faceGlobalI);
                for (auto&& faceGlobalJ : connectivityMap(faceIdx, faceIdx, scvf.index()))
                    pattern.add(faceGlobalI, faceGlobalJ);
            }
        }
    }

    return pattern;
}

} // namespace Dumux

#endif
