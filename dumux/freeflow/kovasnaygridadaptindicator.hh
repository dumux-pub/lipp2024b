// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \ingroup TwoPModel
 * \brief Class defining a standard, analytical solution dependent indicator for grid adaptation
 */

#ifndef DUMUX_FREEFLOW_KOVASNAY_ADAPTION_INDICATOR_HH
#define DUMUX_FREEFLOW_KOVASNAY_ADAPTION_INDICATOR_HH

#include <memory>
#include <dune/common/exceptions.hh>
#include <dune/grid/common/partitionset.hh>

#include <dumux/common/properties.hh>
#include <dumux/common/parameters.hh>
#include <dumux/discretization/elementsolution.hh>

#include <dumux/discretization/staggered/freeflow/mystaggeredgeometryhelper.hh>

namespace Dumux {

/*!\ingroup TwoPModel
 * \brief  Class defining a standard, analytical solution dependent indicator for grid adaptation
 */
template<class TypeTag>
class FreeflowKovasnayGridAdaptIndicator
{
    using GridGeometry = GetPropType<TypeTag, Properties::GridGeometry>;
    using GridView = typename GetPropType<TypeTag, Properties::GridGeometry>::GridView;
    using Element = typename GridView::template Codim<0>::Entity;
    using Scalar = GetPropType<TypeTag, Properties::Scalar>;
    using Indices = typename GetPropType<TypeTag, Properties::ModelTraits>::Indices;
    using SolutionVector = GetPropType<TypeTag, Properties::SolutionVector>;
    using CellCenterPrimaryVariables = GetPropType<TypeTag, Properties::CellCenterPrimaryVariables>;
    using FVElementGeometry = typename GridGeometry::LocalView;
    using Problem = GetPropType<TypeTag, Properties::Problem>;
    using GlobalPosition = typename Element::Geometry::GlobalCoordinate;

    static constexpr int dimWorld = GridView::dimensionworld;

    static constexpr auto pressureIdx = Indices::pressureIdx;

    static constexpr auto cellCenterIdx = GridGeometry::cellCenterIdx();
    static constexpr auto faceIdx = GridGeometry::faceIdx();

public:
    /*! \brief The Constructor
     *
     * \param gridGeometry The finite volume grid geometry
     * \param paramGroup The parameter group in which to look for runtime parameters first (default is "")
     *
     *  Note: refineBound_, coarsenBound_, relativeError_, & maxVelocityDelta_ are chosen
     *        in a way such that the indicator returns false for all elements
     *        before having been calculated.
     */
    FreeflowKovasnayGridAdaptIndicator(std::shared_ptr<const Problem> problem, const std::string& paramGroup = "")
    : problem_(problem)
    , refineBound_(std::numeric_limits<Scalar>::max())
    , relativeError_(problem_->gridGeometry().numCellCenterDofs(), 0.0)
    , minLevel_(getParamFromGroup<std::size_t>(paramGroup, "Adaptive.MinLevel", 0))
    , maxLevel_(getParamFromGroup<std::size_t>(paramGroup, "Adaptive.MaxLevel", 0))
    {}

    /*!
     * \brief Function to set the minimum allowed level.
     *
     * \param minLevel The minimum level
     */
    void setMinLevel(std::size_t minLevel)
    {
        minLevel_ = minLevel;
    }

    /*!
     * \brief Function to set the maximum allowed level.
     *
     *\param maxLevel The maximum level
     */
    void setMaxLevel(std::size_t maxLevel)
    {
        maxLevel_ = maxLevel;
    }

    /*!
     * \brief Function to set the minumum/maximum allowed levels.
     *
     * \param minLevel The minimum level
     * \param maxLevel The maximum level
     */
    void setLevels(std::size_t minLevel, std::size_t maxLevel)
    {
        minLevel_ = minLevel;
        maxLevel_ = maxLevel;
    }

    /*!
     * \brief Calculates the indicator used for refinement/coarsening for each grid cell.
     *
     * \param sol The solution vector
     * \param refineTol The refineTol worst percent of the elements are refined
     *
     *  This standard two-phase indicator is based on the saturation gradient.
     */
    void calculatePressureError(const SolutionVector& sol,
                   Scalar refineTol = 0.05,
                   Scalar coarsenTol = 0.001)
    {
        assert(0 < refineTol && refineTol < 1);

        //! reset the indicator to a state that returns false for all elements
        refineBound_ = std::numeric_limits<Scalar>::max();
        relativeError_.assign(problem_->gridGeometry().gridView().size(0), 0.0);

        //! maxLevel_ must be higher than minLevel_ to allow for refinement
        if (minLevel_ >= maxLevel_)
            return;

        //! check for inadmissible tolerance combination
        if (coarsenTol > refineTol)
            DUNE_THROW(Dune::InvalidStateException, "Refine tolerance must be higher than coarsen tolerance");

//         //! variables to hold the max/mon saturation values on the leaf
//         Scalar globalMax = std::numeric_limits<Scalar>::lowest();
//         Scalar globalMin = std::numeric_limits<Scalar>::max();

        //! Calculate minimum and maximum saturation
        for (const auto& element : elements(problem_->gridGeometry().gridView()))
        {
            //! index of the current leaf-element
            const auto globalIdx = problem_->gridGeometry().elementMapper().index(element);

//             //! obtain the saturation at the center of the element
//             const auto geometry = element.geometry();
//
//             using PrimaryVariables = typename VolumeVariables::PrimaryVariables;
//             const auto& cellCenterPriVars = sol[cellCenterIdx][globalIdx];
//             constexpr auto offset = PrimaryVariables::dimension - CellCenterPrimaryVariables::dimension;
//             PrimaryVariables priVars[pressureIdx + offset] = makePriVarsFromCellCenterPriVars<PrimaryVariables>(cellCenterPriVars);
//             const auto elemSol = elementSolution<FVElementGeometry>(std::move(priVars));
//
//             const Scalar pressure = evalSolution(element, geometry, *gridGeometry_, elemSol, geometry.center())[pressureIdx];

            const Scalar pressure = sol[cellCenterIdx][globalIdx][pressureIdx];

//             //! maybe update the global minimum/maximum
//             using std::min;
//             using std::max;
//             globalMin = min(pressure, globalMin);
//             globalMax = max(pressure, globalMax);

            //! calculate delta in pressure for this cell
            //! obtain analytic pressure
            const Scalar pressureExact = problem_->getAnalyticalPressureSolution()[globalIdx];

            if (pressureExact < std::numeric_limits<Scalar>::lowest())
            {
                relativeError_[globalIdx] = std::abs(pressure - pressureExact); // use the absolute pressure instead of dividing by zero
            }
            else
            {
                relativeError_[globalIdx] = std::abs((pressure - pressureExact) / pressureExact);
            }
        }

        std::vector<Scalar> data(relativeError_.begin(), relativeError_.end());
        const auto midIterator = data.end() - ceil(data.size() * refineTol);
        std::nth_element(data.begin(), midIterator, data.end());
        refineBound_ = data[midIterator - data.begin()];

        //! check if neighbors have to be refined too
        for (const auto& element : elements(problem_->gridGeometry().gridView(), Dune::Partitions::interior))
            if (this->operator()(element) > 0)
                checkNeighborsRefine_(element);
    }

    /*!
     * \brief Calculates the indicator used for refinement/coarsening for each grid cell.
     *
     * \param sol The solution vector
     * \param refineTol The refineTol worst percent of the elements are refined
     *
     *  This standard two-phase indicator is based on the saturation gradient.
     */
    void calculateVelocityError(const SolutionVector& sol,
                                Scalar step,
                   Scalar refineTol = 0.05,
                   Scalar coarsenTol = 0.001)
    {
        step_ = step;

        assert(0 < refineTol && refineTol < 1);

        //! reset the indicator to a state that returns false for all elements
        refineBound_ = std::numeric_limits<Scalar>::max();
        relativeError_.assign(problem_->gridGeometry().gridView().size(0), 0.0);

        //! maxLevel_ must be higher than minLevel_ to allow for refinement
        if (minLevel_ >= maxLevel_)
            return;

        //! check for inadmissible tolerance combination
        if (coarsenTol > refineTol)
            DUNE_THROW(Dune::InvalidStateException, "Refine tolerance must be higher than coarsen tolerance");

        //! Calculate minimum and maximum saturation
        for (const auto& element : elements(problem_->gridGeometry().gridView()))
        {
            //! index of the current leaf-element
            const auto globalIdx = problem_->gridGeometry().elementMapper().index(element);

//             auto kinematicViscosity_ = getParam<Scalar>("Component.LiquidKinematicViscosity", 1.0);
//             Scalar reynoldsNumber = 1.0 / kinematicViscosity_;
//             auto lambda_ = 0.5 * reynoldsNumber
//                             - std::sqrt(reynoldsNumber * reynoldsNumber * 0.25 + 4.0 * M_PI * M_PI);
//
//             using CellArray = std::array<unsigned int, dimWorld>;
//             const auto numCells = getParam<CellArray>("Grid.Cells");
//
//             auto cellSizeX_ = (problem_->gridGeometry().bBoxMax()[0]-problem_->gridGeometry().bBoxMin()[0]) / (numCells[0]*std::pow(2,(step_- 2)));
//             auto cellSizeY_ = (problem_->gridGeometry().bBoxMax()[1]-problem_->gridGeometry().bBoxMin()[1]) / (numCells[1]*std::pow(2,(step_- 2)));
//             auto captureZoneBoundary = -1/lambda_*std::log(std::sin(2*M_PI*y)/(2*M_PI*y));
//             auto secondCaptureZoneBoundary = -1/lambda_*std::log(std::sin(2*M_PI*y)/(2*M_PI*(y- 1)));
//             auto dividingLine = 0.5;

            auto x=element.geometry().center()[0];
            auto y=element.geometry().center()[1];

            if (/*((captureZoneBoundary-cellSizeX_/2.) < x && x < (captureZoneBoundary + cellSizeX_/2.)) ||
                ((secondCaptureZoneBoundary-cellSizeX_/2.) < x && x < (secondCaptureZoneBoundary + cellSizeX_/2.))||
                (dividingLine-cellSizeY_/2. < y && y < dividingLine+cellSizeY_/2.)*/
                (x > 0.5 && x < 1.5 && y > 0.75 && y < 1.25) ||
                (x > 0.5 && x < 1 && y > 0.25 && y < 0.5) ||
                (x > 1 && x < 1.5 && y > -0.25 && y < 0) /*||
                (x > 1.5 && y > 1.25)*/
                )
                relativeError_[globalIdx] = std::numeric_limits<Scalar>::max();
            else
                relativeError_[globalIdx] = std::numeric_limits<Scalar>::min();
        }

        refineBound_ = 1;

        //! check if neighbors have to be refined too
        for (const auto& element : elements(problem_->gridGeometry().gridView(), Dune::Partitions::interior))
            if (this->operator()(element) > 0)
                checkNeighborsRefine_(element);
    }

    /*! \brief function call operator to return mark
     *
     *  \return  1 if an element should be refined
     *          -1 if an element should be coarsened
     *           0 otherwise
     *
     *  \param element A grid element
     */
    int operator() (const Element& element) const
    {
        if (element.level() < maxLevel_
                 && relativeError_[problem_->gridGeometry().elementMapper().index(element)] > refineBound_)
        {
            return 1;
        }
        else
            return 0;
    }

private:
    /*!
     * \brief Method ensuring the refinement ratio of 2:1
     *
     *  For any given element, a loop over the neighbors checks if the
     *  entities refinement would require that any of the neighbors has
     *  to be refined, too. This is done recursively over all levels of the grid.
     *
     * \param element Element of interest that is to be refined
     * \param level level of the refined element: it is at least 1
     * \return true if everything was successful
     */
    bool checkNeighborsRefine_(const Element &element, std::size_t level = 1)
    {
        std::vector<Element> surroundingElements;
        std::vector<GlobalPosition> surroundingElementCenters;

        for (const auto& intersection : intersections(problem_->gridGeometry().gridView(), element))
        {
            if(!intersection.neighbor())
                continue;

            //direct neighbor
            surroundingElements.push_back(intersection.outside());

            //diagonal neighbor
            for (const auto& outsideIntersection : intersections(problem_->gridGeometry().gridView(), intersection.outside()))
            {
                if (!outsideIntersection.neighbor())
                    continue;

                if (haveCommonCorner(outsideIntersection,element) && !containerCmp(outsideIntersection.geometry().center(),intersection.geometry().center()))
                {
                    if(containerFind(surroundingElementCenters.begin(), surroundingElementCenters.end(), outsideIntersection.outside().geometry().center()) == surroundingElementCenters.end())
                    {
                        surroundingElements.push_back(outsideIntersection.outside());
                        surroundingElementCenters.push_back(outsideIntersection.outside().geometry().center());
                    }
                }
            }
        }

        for (const auto& surroundingElement : surroundingElements)
        {
            // only mark non-ghost elements
            if (surroundingElement.partitionType() == Dune::GhostEntity)
                continue;

            if (surroundingElement.level() < maxLevel_ && surroundingElement.level() < element.level()/*is not yet refined but will be*/)
            {
                // ensure refinement for surroundingElement element
                relativeError_[problem_->gridGeometry().elementMapper().index(surroundingElement)] = std::numeric_limits<Scalar>::max();
                if(level < maxLevel_)
                    checkNeighborsRefine_(surroundingElement, ++level);
            }
        }

        return true;
    }

    std::shared_ptr<const Problem> problem_;
    int step_ = 0;

    Scalar refineBound_;
    std::vector< Scalar > relativeError_;
    std::size_t minLevel_;
    std::size_t maxLevel_;
};

} // end namespace Dumux

#endif
