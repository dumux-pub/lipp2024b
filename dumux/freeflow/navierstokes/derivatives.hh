// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 3 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \ingroup NavierStokesModel
 * \copydoc Dumux::Derivatives
 */
#ifndef DUMUX_NAVIERSTOKES_DERIVATIVES_HH
#define DUMUX_NAVIERSTOKES_DERIVATIVES_HH

#include <dumux/discretization/extrusion.hh>

namespace Dumux {

/*!
 * \ingroup NavierStokesModel
 * \brief Derivatives calc
 *
 * This implements the calculation of dxu, dyv and divu.
 *
 */
template<class GridGeometry, class Scalar = double>
class Derivatives
{
    using FVElementGeometry = typename GridGeometry::LocalView;
    using SubControlVolume = typename FVElementGeometry::SubControlVolume;
    using Extrusion = Extrusion_t<GridGeometry>;

public:
    template<class SolutionVector, class LambdaB>
    void update(const FVElementGeometry& fvGeometry,
                const SubControlVolume& scv,
                const SolutionVector& curSol,
                const LambdaB& instationaryAnalyticalVelocitySolutionAtPosLambdaFunction)
    {
        for (const auto& scvf : scvfs(fvGeometry))
        {
            const auto& gradientPrepare = [&] (Scalar& analyticalGradient, Scalar& numericalGradient)
            {
                const Scalar analyticalSolutionFace = instationaryAnalyticalVelocitySolutionAtPosLambdaFunction(scvf);
                const Scalar numericalSolutionFace = curSol[GridGeometry::faceIdx()][scvf.dofIndex()][0];

                analyticalGradient += analyticalSolutionFace * scvf.directionSign() * scvf.area();
                numericalGradient += numericalSolutionFace * scvf.directionSign() * scvf.area();
            };

            if (scvf.directionIndex() == 0)
                gradientPrepare(analyticalDxUCC_, numericalDxUCC_);
            else if (scvf.directionIndex() == 1)
                gradientPrepare(analyticalDyVCC_, numericalDyVCC_);
            else
                DUNE_THROW(Dune::InvalidStateException, "");
        }

        const auto& gradientFinalize = [&] (Scalar& analyticalGradient, Scalar& numericalGradient)
        {
            analyticalGradient /= Extrusion::volume(scv);
            numericalGradient /= Extrusion::volume(scv);
        };

        gradientFinalize(analyticalDxUCC_, numericalDxUCC_);
        gradientFinalize(analyticalDyVCC_, numericalDyVCC_);

        analyticalDivVelCC_ = analyticalDxUCC_ + analyticalDyVCC_;
        numericalDivVelCC_ = numericalDxUCC_ + numericalDyVCC_;
    }

    /*!
     * \brief Returns the analytical dxu
     */
    auto& getAnalyticalDxUCC() const
    {
        return analyticalDxUCC_;
    }

    /*!
     * \brief Returns the analytical dxu
     */
    auto& getAnalyticalDyVCC() const
    {
        return analyticalDyVCC_;
    }

    /*!
     * \brief Returns the analytical dxu
     */
    auto& getAnalyticalDivVelCC() const
    {
        return analyticalDivVelCC_;
    }

    /*!
     * \brief Returns the numerical dxu
     */
    auto& getNumericalDxUCC() const
    {
        return numericalDxUCC_;
    }

    /*!
     * \brief Returns the numerical dxu
     */
    auto& getNumericalDyVCC() const
    {
        return numericalDyVCC_;
    }

    /*!
     * \brief Returns the numerical dxu
     */
    auto& getNumericalDivVelCC() const
    {
        return numericalDivVelCC_;
    }

private:
    Scalar analyticalDxUCC_ = 0.;
    Scalar numericalDxUCC_ = 0.;

    Scalar analyticalDyVCC_ = 0.;
    Scalar numericalDyVCC_ = 0.;

    Scalar analyticalDivVelCC_;
    Scalar numericalDivVelCC_;
};
}// end namespace Dumux

#endif
