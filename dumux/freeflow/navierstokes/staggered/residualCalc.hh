// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 3 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \ingroup NavierStokesModel
 */

#ifndef DUMUX_NAVIERSTOKES_STAGGERED_RESIDUALCALC_HH
#define DUMUX_NAVIERSTOKES_STAGGERED_RESIDUALCALC_HH

#include <dumux/common/timeloop.hh>
#include <dune/grid/io/file/vtk/vtkwriter.hh>
#include <dumux/io/grid/controlvolumegrids.hh>
#include <dumux/discretization/staggered/freeflow/mystaggeredgeometryhelper.hh>
#include <dumux/discretization/staggered/freeflow/mysubcontrolvolumeface.hh>
#include <dumux/freeflow/navierstokes/analyticalSolutionIntegration.hh>
#include <dumux/freeflow/navierstokes/staggered/residualcalcgenericmethods.hh>

namespace Dumux{
    namespace Properties {
        template<class TypeTag, class MyTypeTag>
        struct CVGridGeometry { using type = UndefinedProperty; };
    }

template<class TypeTag>
class ResidualCalc
{
    using GridGeometry = GetPropType<TypeTag, Properties::GridGeometry>;
    using SubControlVolumeFace = typename GridGeometry::SubControlVolumeFace;
    using GridView = typename GridGeometry::GridView;
    using Scalar = GetPropType<TypeTag, Properties::Scalar>;
    using MLGTraits = typename MyFreeFlowStaggeredDefaultScvfGeometryTraits<GridView>::template ScvfMLGTraits<Scalar> ;

    using Indices = typename GetPropType<TypeTag, Properties::ModelTraits>::Indices;
    using Element = typename GridGeometry::GridView::template Codim<0>::Entity;
    using GlobalPosition = typename Element::Geometry::GlobalCoordinate;
    using Problem = GetPropType<TypeTag, Properties::Problem>;

    using HostGrid = Dune::ALUGrid<2,2,Dune::cube,Dune::nonconforming >;
    using HostGridManager = GridManager<HostGrid>;
    using CVGridGeometry = GetPropType<TypeTag, Properties::CVGridGeometry>;

    using SolutionVector = GetPropType<TypeTag, Properties::SolutionVector>;
    using CellCenterSolutionVector = GetPropType<TypeTag, Properties::CellCenterSolutionVector>;
    using FaceSolutionVector = GetPropType<TypeTag, Properties::FaceSolutionVector>;

    using GridVariables = GetPropType<TypeTag, Properties::GridVariables>;
    using Assembler = StaggeredRefinedFVAssembler<TypeTag, DiffMethod::numeric>;

public:
    /*!
     * \brief The Constructor
     */
    ResidualCalc(std::shared_ptr<const Problem> problem)
    : problem_(problem)
    {}

    template<class LambdaBC, class LambdaC, class LambdaD, class LambdaE, class LambdaF>
    void calcResidualsStationary(std::shared_ptr<const GridGeometry> gridGeometry,
                                 std::array<HostGridManager, 4>& cvGridManagers,
                                 const std::array<std::map<GlobalPosition, std::vector<unsigned int>, ContainerCmpClass<GlobalPosition>>,4>& cvCenterScvfIndicesMaps,
                                 CellCenterSolutionVector& cellCenterResidualWithAnalyticalSol,
                                 FaceSolutionVector& faceResidualWithAnalyticalSol,
                                 std::optional<std::array<std::vector<Scalar>, 4>>& perfectInterpolationFaceResiduals,
                                 const LambdaBC& fineToCoarseInside,
                                 const LambdaC& coarseToFineInside,
                                 const LambdaD& coarseToFineLeft,
                                 const LambdaE& coarseToFineRight,
                                 const LambdaF& coarseToFineRightEdgeDualGrid,
                                 unsigned int refinementIdx = 0)
    {
        if (perfectInterpolationFaceResiduals)
            *perfectInterpolationFaceResiduals = calcPerfectInterpolationFaceResidual_(
                refinementIdx,
                [&] (const GlobalPosition& globalPos, Scalar time) { return problem_->analyticalSolutionAtPos (globalPos); },
                [&] (const GlobalPosition& globalPos, Scalar time) { return problem_->sourceAtPos             (globalPos); },
                fineToCoarseInside,
                coarseToFineInside,
                coarseToFineLeft,
                coarseToFineRight,
                coarseToFineRightEdgeDualGrid,
                true,
                0.,
                0.,
                cvGridManagers,
                cvCenterScvfIndicesMaps);

        SolutionVector numericalResidual = calcNumericalResidualStationary_(gridGeometry);

        FaceSolutionVector areaWeightedFaceResidualWithAnalyticalSol = numericalResidual[GridGeometry::faceIdx()];
        std::cout << "velocity residual norm = " << areaWeightedFaceResidualWithAnalyticalSol.two_norm() << std::endl;
        faceResidualWithAnalyticalSol = areaWeightedToNonAreaWeightedFaceVec_(areaWeightedFaceResidualWithAnalyticalSol);

        CellCenterSolutionVector areaWeightedCCResidualWithAnalyticalSol = numericalResidual[GridGeometry::cellCenterIdx()];
        std::cout << "presssure residual norm = " << areaWeightedCCResidualWithAnalyticalSol.two_norm() << std::endl;
        cellCenterResidualWithAnalyticalSol = areaWeightedToNonAreaWeightedCCVec_(areaWeightedCCResidualWithAnalyticalSol);
    }

    template<class LambdaBC, class LambdaC, class LambdaD, class LambdaE, class LambdaF>
    void calcResidualsInstationary(unsigned int timeIndex,
                                   std::shared_ptr<const TimeLoop<Scalar>> timeLoop,
                                   std::shared_ptr<const GridGeometry> gridGeometry,
                                   std::array<HostGridManager, 4>& cvGridManagers,
                                   const std::array<std::map<GlobalPosition, std::vector<unsigned int>, ContainerCmpClass<GlobalPosition>>,4>& cvCenterScvfIndicesMaps,
                                   CellCenterSolutionVector& cellCenterResidualWithAnalyticalSol,
                                   FaceSolutionVector& faceResidualWithAnalyticalSol,
                                   std::optional<std::array<std::vector<Scalar>, 4>>& perfectInterpolationFaceResiduals,
                                   const LambdaBC& fineToCoarseInside,
                                   const LambdaC& coarseToFineInside,
                                   const LambdaD& coarseToFineLeft,
                                   const LambdaE& coarseToFineRight,
                                   const LambdaF& coarseToFineRightEdgeDualGrid)
    {
        Scalar t = timeLoop->time()+timeLoop->timeStepSize();
        Scalar tOld = timeLoop->time();

        if (perfectInterpolationFaceResiduals)
            *perfectInterpolationFaceResiduals = calcPerfectInterpolationFaceResidual_(
            timeIndex,
            [&] (const GlobalPosition& globalPos, Scalar time) { return problem_->instationaryAnalyticalSolutionAtPos (globalPos, time); },
            [&] (const GlobalPosition& globalPos, Scalar time) { return problem_->instationarySourceAtPos             (globalPos, time); },
            fineToCoarseInside,
            coarseToFineInside,
            coarseToFineLeft,
            coarseToFineRight,
            coarseToFineRightEdgeDualGrid,
            false,
            t,
            tOld,
            cvGridManagers,
            cvCenterScvfIndicesMaps);

        SolutionVector numericalResidual = calcNumericalResidualInstationary_(gridGeometry, timeLoop, t, tOld);

        FaceSolutionVector areaWeightedFaceResidualWithAnalyticalSol = numericalResidual[GridGeometry::faceIdx()];
        std::cout << "velocity residual norm = " << areaWeightedFaceResidualWithAnalyticalSol.two_norm() << std::endl;
        faceResidualWithAnalyticalSol = areaWeightedToNonAreaWeightedFaceVec_(areaWeightedFaceResidualWithAnalyticalSol);

        CellCenterSolutionVector areaWeightedCCResidualWithAnalyticalSol = numericalResidual[GridGeometry::cellCenterIdx()];
        std::cout << "presssure residual norm = " << areaWeightedCCResidualWithAnalyticalSol.two_norm() << std::endl;
        cellCenterResidualWithAnalyticalSol = areaWeightedToNonAreaWeightedCCVec_(areaWeightedCCResidualWithAnalyticalSol);
    }

private:
    template <class Vec>
    auto areaWeightedToNonAreaWeightedFaceVec_(const Vec& areaWeightedVec) const
    {
        Dune::BlockVector<Dune::FieldVector<Scalar, 1>, std::allocator<Dune::FieldVector<Scalar, 1> > > resVel;
        resVel = areaWeightedVec;

        auto cvAreas = resVel;
        cvAreas = 0.;

        for (const auto& element : elements((*problem_).gridGeometry().gridView()))
        {
            auto fvGeometry = localView((*problem_).gridGeometry());
            fvGeometry.bindElement(element);
            for (const auto& scvf : scvfs(fvGeometry))
            {
                cvAreas[scvf.dofIndex()] += (scvf.area() * 0.5 * scvf.selfToOppositeDistance());
            }
        }

        for (int i = 0; i < resVel.size(); ++i)
        {
            resVel[i] /= cvAreas[i];
        }

        return resVel;
    }

    template <class Vec>
    auto areaWeightedToNonAreaWeightedCCVec_(const Vec& areaWeightedVec) const
    {
        CellCenterSolutionVector resVec;
        resVec = areaWeightedVec;

        for (const auto& element : elements((*problem_).gridGeometry().gridView()))
            resVec[(*problem_).gridGeometry().gridView().indexSet().index(element)] /= element.geometry().volume();

        return resVec;
    }

    template<class LambdaA, class LambdaB, class LambdaBC, class LambdaC, class LambdaD, class LambdaE, class LambdaF>
    std::array<std::vector<Scalar>,4> calcPerfectInterpolationFaceResidual_(
        unsigned int timeIndex,
        const LambdaA& possiblyInstationaryAnalyticalSolution,
        const LambdaB& possiblyInstationarySourceAtPos,
        const LambdaBC& fineToCoarseInside,
        const LambdaC& coarseToFineInside,
        const LambdaD& coarseToFineLeft,
        const LambdaE& coarseToFineRight,
        const LambdaF& coarseToFineRightEdgeDualGrid,
        bool isStationary,
        Scalar t,
        Scalar tOld,
        std::array<HostGridManager, 4>& gridManagers,
        const std::array<std::map<GlobalPosition, std::vector<unsigned int>, ContainerCmpClass<GlobalPosition>>,4>& cvCenterScvfIndicesMaps)
    {
        std::array<std::vector<Scalar>,4> retArray;

        fillResidual_(timeIndex, possiblyInstationaryAnalyticalSolution, possiblyInstationarySourceAtPos, fineToCoarseInside, coarseToFineInside, coarseToFineLeft, coarseToFineRight,coarseToFineRightEdgeDualGrid, isStationary, t, tOld, retArray[0], gridManagers[0].grid().leafGridView(), cvCenterScvfIndicesMaps[0], 0, false);
        fillResidual_(timeIndex, possiblyInstationaryAnalyticalSolution, possiblyInstationarySourceAtPos, fineToCoarseInside, coarseToFineInside, coarseToFineLeft, coarseToFineRight,coarseToFineRightEdgeDualGrid, isStationary, t, tOld, retArray[1], gridManagers[1].grid().leafGridView(), cvCenterScvfIndicesMaps[1], 1, false);
        fillResidual_(timeIndex, possiblyInstationaryAnalyticalSolution, possiblyInstationarySourceAtPos, fineToCoarseInside, coarseToFineInside, coarseToFineLeft, coarseToFineRight,coarseToFineRightEdgeDualGrid, isStationary, t, tOld, retArray[2], gridManagers[2].grid().leafGridView(), cvCenterScvfIndicesMaps[2], 0, true);
        fillResidual_(timeIndex, possiblyInstationaryAnalyticalSolution, possiblyInstationarySourceAtPos, fineToCoarseInside, coarseToFineInside, coarseToFineLeft, coarseToFineRight,coarseToFineRightEdgeDualGrid, isStationary, t, tOld, retArray[3], gridManagers[3].grid().leafGridView(), cvCenterScvfIndicesMaps[3], 1, true);

        return  retArray;
    }

    SolutionVector calcNumericalResidualInstationary_(std::shared_ptr<const GridGeometry> gridGeometry, std::shared_ptr<const TimeLoop<Scalar>> timeLoop, Scalar t, Scalar tOld) const
    {
        const auto analyticalTPlusDeltaTTuple = problem_->getAnalyticalSolution(t);
        const auto analyticalTTuple = problem_->getAnalyticalSolution(tOld);

        SolutionVector analyticalTPlusDeltaT;
        analyticalTPlusDeltaT[GridGeometry::faceIdx()] = std::get<3>(analyticalTPlusDeltaTTuple);
        analyticalTPlusDeltaT[GridGeometry::cellCenterIdx()] = std::get<0>(analyticalTPlusDeltaTTuple);

        SolutionVector analyticalT;
        analyticalT[GridGeometry::faceIdx()] = std::get<3>(analyticalTTuple);
        analyticalT[GridGeometry::cellCenterIdx()] = std::get<0>(analyticalTTuple);

        auto localGridVariables = std::make_shared<GridVariables>(problem_, gridGeometry);
        localGridVariables->init(analyticalT);
        localGridVariables->update(analyticalTPlusDeltaT);

        const auto numDofsCellCenter = gridGeometry->numCellCenterDofs();
        const auto numDofsFace = gridGeometry->numIntersections();

        SolutionVector residual;
        residual[GridGeometry::cellCenterIdx()].resize(numDofsCellCenter);
        residual[GridGeometry::faceIdx()].resize(numDofsFace);

        Assembler localAssemblerObject(problem_, gridGeometry, localGridVariables, timeLoop, analyticalT);
        localAssemblerObject.assembleResidual(residual, analyticalTPlusDeltaT);

        return residual;
    }

    SolutionVector calcNumericalResidualStationary_(std::shared_ptr<const GridGeometry> gridGeometry) const
    {
        const auto analyticalTuple = problem_->getAnalyticalSolution();

        SolutionVector analytical;
        analytical[GridGeometry::faceIdx()] = std::get<3>(analyticalTuple);
        analytical[GridGeometry::cellCenterIdx()] = std::get<0>(analyticalTuple);

        auto localGridVariables = std::make_shared<GridVariables>(problem_, gridGeometry);
        localGridVariables->init(analytical);

        const auto numDofsCellCenter = gridGeometry->numCellCenterDofs();
        const auto numDofsFace = gridGeometry->numIntersections();

        SolutionVector residual;
        residual[GridGeometry::cellCenterIdx()].resize(numDofsCellCenter);
        residual[GridGeometry::faceIdx()].resize(numDofsFace);

        Assembler localAssemblerObject(problem_, gridGeometry, localGridVariables);
        localAssemblerObject.assembleResidual(residual, analytical);

        return residual;
    }

    Scalar residual_(bool isStationary, Scalar velSelfOld, Scalar deltaT, Scalar leftOrDownOppoDelta, Scalar rightOrUpOppoDelta, Scalar rightOrUpLateralDelta, Scalar scvfDelta, Scalar leftOrDownLateralDelta, Scalar q, Scalar velSelf, Scalar velParallelRightOrUp, Scalar velParallelLeftOrDown, Scalar velOppoRightOrUp, Scalar velOppoLeftOrDown, Scalar velNormalUpRightOrRightUp, Scalar velNormalUpLeftOrRightDown, Scalar velNormalDownRightOrLeftUp, Scalar velNormalDownLeftOrLeftDown, Scalar presLeftOrDown, Scalar presRightOrUp, bool navierStokes, Scalar viscosity) const
    {
        Scalar opposDelta = .5 * (leftOrDownOppoDelta + rightOrUpOppoDelta);
        Scalar density = 1.; //can only deal with constant viscosity here
        Scalar res;

        static const bool enableUnsymmetrizedVelocityGradient
            = getParam<bool>("FreeFlow.EnableUnsymmetrizedVelocityGradient", true); //this is about -dxx u - dxy v = -dx (dxu+dyv)=(with conti) 0

        if (enableUnsymmetrizedVelocityGradient)
        {
            res = -q*opposDelta * scvfDelta
            + ( 2*opposDelta/(rightOrUpLateralDelta+scvfDelta) + 2*opposDelta/(leftOrDownLateralDelta+scvfDelta) + scvfDelta/leftOrDownOppoDelta + scvfDelta/rightOrUpOppoDelta ) * viscosity * velSelf
            + ( -2*opposDelta/(rightOrUpLateralDelta+scvfDelta)) * viscosity * velParallelRightOrUp
            + ( -2*opposDelta/(leftOrDownLateralDelta+scvfDelta) ) * viscosity * velParallelLeftOrDown
            + ( -scvfDelta/leftOrDownOppoDelta ) * viscosity * velOppoLeftOrDown
            + ( -scvfDelta/rightOrUpOppoDelta ) * viscosity * velOppoRightOrUp

            - scvfDelta* presLeftOrDown
            + scvfDelta * presRightOrUp;
        }
        else
        {
            res = -q*opposDelta * scvfDelta
            + ( 2*opposDelta/(rightOrUpLateralDelta+scvfDelta) + 2*opposDelta/(leftOrDownLateralDelta+scvfDelta) + 2*scvfDelta/leftOrDownOppoDelta + 2*scvfDelta/rightOrUpOppoDelta ) * viscosity * velSelf
            + ( -2*opposDelta/(rightOrUpLateralDelta+scvfDelta)) * viscosity * velParallelRightOrUp
            + ( -2*opposDelta/(leftOrDownLateralDelta+scvfDelta) ) * viscosity * velParallelLeftOrDown
            + ( -2*scvfDelta/leftOrDownOppoDelta ) * viscosity * velOppoLeftOrDown
            + ( -2*scvfDelta/rightOrUpOppoDelta ) * viscosity * velOppoRightOrUp
            - velNormalUpRightOrRightUp * viscosity
            + velNormalUpLeftOrRightDown * viscosity
            + velNormalDownRightOrLeftUp * viscosity
            - velNormalDownLeftOrLeftDown * viscosity
            - scvfDelta* presLeftOrDown
            + scvfDelta * presRightOrUp;
        }

    //     std::cout << "source " << -q*opposDelta * scvfDelta;
    //     std::cout << ", self " << ( 2*opposDelta/(rightOrUpLateralDelta+scvfDelta) + 2*opposDelta/(leftOrDownLateralDelta+scvfDelta) + 2*scvfDelta/leftOrDownOppoDelta + 2*scvfDelta/rightOrUpOppoDelta ) * viscosity * velSelf;
    //     std::cout << ", parallel a " <<  ( -2*opposDelta/(rightOrUpLateralDelta+scvfDelta)) * viscosity * velParallelRightOrUp;
    //     std::cout << ", prallel b " << ( -2*opposDelta/(leftOrDownLateralDelta+scvfDelta) ) * viscosity * velParallelLeftOrDown;
    //     std::cout << ", oppo a " << ( -2*scvfDelta/leftOrDownOppoDelta ) * viscosity * velOppoLeftOrDown;
    //     std::cout << ", oppo b " << ( -2*scvfDelta/rightOrUpOppoDelta ) * viscosity * velOppoRightOrUp;
    //     std::cout << ", normal a " << - velNormalUpRightOrRightUp * viscosity;
    //     std::cout << ", normal b " << velNormalUpLeftOrRightDown * viscosity;
    //     std::cout << ", normal c " << velNormalDownRightOrLeftUp * viscosity;
    //     std::cout << ", normal d " << - velNormalDownLeftOrLeftDown * viscosity;
    //     std::cout << ", pressure a " << - scvfDelta* presLeftOrDown;
    //     std::cout << ", pressure b " << scvfDelta * presRightOrUp;
    //     std::cout << std::endl;

    //     std::cout << "res = " << res << std::endl;
    //
    //     std::cout << "opposDelta * scvfDelta = " << opposDelta * scvfDelta << std::endl;
    //
    //     std::cout << "d_y u term: " <<
    //       ((2/(rightOrUpLateralDelta+scvfDelta) + 2/(leftOrDownLateralDelta+scvfDelta))/(scvfDelta)* velSelf
    //     + ( -2/(rightOrUpLateralDelta+scvfDelta))/(scvfDelta) * velParallelRightOrUp
    //     + ( -2/(leftOrDownLateralDelta+scvfDelta))/(scvfDelta) * velParallelLeftOrDown) << std::endl;

    //     Scalar selfCoefficient = ((leftOrDownLateralDelta+scvfDelta)/(2.*scvfDelta)+(rightOrUpLateralDelta+scvfDelta)/(2.*scvfDelta))/(((rightOrUpLateralDelta+scvfDelta)/2.)*((leftOrDownLateralDelta+scvfDelta)/2.))*scvfDelta*scvfDelta;
    //     Scalar upCoefficient = (leftOrDownLateralDelta+scvfDelta)/(2.*scvfDelta)/(((rightOrUpLateralDelta+scvfDelta)/2.)*((leftOrDownLateralDelta+scvfDelta)/2.))*scvfDelta*scvfDelta;
    //     Scalar downCoefficient = (rightOrUpLateralDelta+scvfDelta)/(2.*scvfDelta)/(((rightOrUpLateralDelta+scvfDelta)/2.)*((leftOrDownLateralDelta+scvfDelta)/2.))*scvfDelta*scvfDelta;

    //     std::cout << "selfCoefficient " << selfCoefficient << ", upCoefficient = " << upCoefficient << ", downCoefficient = " << downCoefficient << std::endl;

    //     std::cout << "var d_y u term: " <<
    //     (selfCoefficient*velSelf
    //     -upCoefficient*velParallelRightOrUp
    //     -downCoefficient*velParallelLeftOrDown)
    //     /(scvfDelta*scvfDelta)
    //     << std::endl;
    //
    //     std::cout << "pressure term: " << (- scvfDelta* presLeftOrDown + scvfDelta * presRightOrUp)/(opposDelta * scvfDelta) << std::endl;

        if (navierStokes)
        {
            auto mixedVelocity = [&](Scalar upwindVelocity, Scalar downwindVelocity)
            {
                Scalar upwindWeight = Dumux::getParam<Scalar>("Flux.UpwindWeight", 1.);
                return upwindWeight * upwindVelocity + (1. - upwindWeight) * downwindVelocity;
            };

            //x:left, y: down
            Scalar transportingVelocity = (velSelf + velOppoLeftOrDown) * 0.5;
            Scalar upwindVelocity = (Dumux::sign(transportingVelocity) == +1) ? velOppoLeftOrDown : velSelf;
            Scalar downwindVelocity = (Dumux::sign(transportingVelocity) == +1) ? velSelf : velOppoLeftOrDown;

            res -= scvfDelta * density * transportingVelocity * mixedVelocity(upwindVelocity,downwindVelocity);

            //x:right, y: up
            transportingVelocity = (velSelf + velOppoRightOrUp) * 0.5;
            upwindVelocity = (Dumux::sign(transportingVelocity) == +1) ? velSelf : velOppoRightOrUp;
            downwindVelocity = (Dumux::sign(transportingVelocity) == +1) ? velOppoRightOrUp : velSelf;

            res += scvfDelta * density * transportingVelocity * mixedVelocity(upwindVelocity,downwindVelocity);

            //x:upright, y: rightup
            if (rightOrUpLateralDelta == 0.)
            {
                upwindVelocity = velParallelRightOrUp;
                downwindVelocity = velParallelRightOrUp;
            }
            else
            {
                upwindVelocity = (Dumux::sign(velNormalUpRightOrRightUp)==+1) ? velSelf : velParallelRightOrUp;
                downwindVelocity = (Dumux::sign(velNormalUpRightOrRightUp)==+1) ? velParallelRightOrUp : velSelf;
            }

            res += .5 * rightOrUpOppoDelta * density * mixedVelocity(upwindVelocity,downwindVelocity) * velNormalUpRightOrRightUp;

            //x: downright, y: leftup
            if (leftOrDownLateralDelta == 0.)
            {
                upwindVelocity = velParallelLeftOrDown;
                downwindVelocity = velParallelLeftOrDown;
            }
            else
            {
                upwindVelocity = (Dumux::sign(velNormalDownRightOrLeftUp)==+1) ? velParallelLeftOrDown : velSelf;
                downwindVelocity = (Dumux::sign(velNormalDownRightOrLeftUp)==+1) ? velSelf : velParallelLeftOrDown;
            }

            res -= .5 * rightOrUpOppoDelta * density * mixedVelocity(upwindVelocity,downwindVelocity) * velNormalDownRightOrLeftUp;

            //x:upleft, y: rightdown
            if (rightOrUpLateralDelta == 0.)
            {
                downwindVelocity = velParallelRightOrUp;
                upwindVelocity = velParallelRightOrUp;
            }
            else
            {
                downwindVelocity =  (Dumux::sign(velNormalUpLeftOrRightDown)==+1) ? velParallelRightOrUp : velSelf;
                upwindVelocity = (Dumux::sign(velNormalUpLeftOrRightDown)==+1) ? velSelf : velParallelRightOrUp;
            }

            res += .5 * leftOrDownOppoDelta * density * mixedVelocity(upwindVelocity,downwindVelocity) * velNormalUpLeftOrRightDown;

            //x:downleft, y: leftdown
            if (leftOrDownLateralDelta == 0.)
            {
                upwindVelocity = velParallelLeftOrDown;
                downwindVelocity = velParallelLeftOrDown;
            }
            else
            {
                upwindVelocity = (Dumux::sign(velNormalDownLeftOrLeftDown)==+1) ? velParallelLeftOrDown : velSelf;
                downwindVelocity = (Dumux::sign(velNormalDownLeftOrLeftDown)==+1) ? velSelf : velParallelLeftOrDown;
            }

            res -= .5 * leftOrDownOppoDelta * density * mixedVelocity(upwindVelocity,downwindVelocity) * velNormalDownLeftOrLeftDown;
        }

        if (!isStationary)
        {
            res += (velSelf - velSelfOld) / deltaT * opposDelta * scvfDelta;
        }

    //      std::cout << "final res = " << res/(opposDelta * scvfDelta) << std::endl;

        return res/(opposDelta * scvfDelta); //that should be the one relevant for grid convergence
    }

    template <class LambdaA>
    Scalar possiblyInstationaryPossiblyMeanAnalyticalVelocityySolutionAtPos_(const LambdaA& possiblyInstationaryAnalyticalSolution, const std::array<GlobalPosition, 2>& corners, unsigned int dirIdx, Scalar t) const
    {
        if (containerCmp(corners[0],corners[1]))//for parallel velocities at boundaries we can have nonStandardHigherLateralDelta == 0, nonStandardLowerLateralDelta == 0; then we would be dividing by zero when trying to average
        {
            GlobalPosition center = corners[0];//=corners[1]
            return possiblyInstationaryAnalyticalSolution(center,t)[Indices::velocity(dirIdx)];
        }
        else if ((*problem_).useScvfLineAveraging() )
        {
            AnalyticalSolutionIntegration<TypeTag, MLGTraits> analyticalSolInt((*problem_).quadOrder());
            auto lineGeo = analyticalSolInt.getLineGeometry(corners);
            return analyticalSolInt.lineIntegral(lineGeo, [&](const GlobalPosition& globalPos) { return possiblyInstationaryAnalyticalSolution(globalPos,t)[Indices::velocity(dirIdx)]; });
        }
        else if ((*problem_).useScvfCVAveraging() )
        {
            DUNE_THROW(Dune::InvalidStateException, "Residual calc can only do line averaging for scvf.");
        }
        else
        {
            GlobalPosition center = corners[0]+corners[1];
            center *= 0.5;
            return possiblyInstationaryAnalyticalSolution(center,t)[Indices::velocity(dirIdx)];
        }
    }

    template <class LambdaA>
    Scalar analyticalXVelocityBetweenCorners_ (const LambdaA& possiblyInstationaryAnalyticalSolution, const std::array<GlobalPosition, 2>& corners, Scalar time) const
    {
        return possiblyInstationaryPossiblyMeanAnalyticalVelocityySolutionAtPos_(possiblyInstationaryAnalyticalSolution, corners, 0, time);
    }

    template <class LambdaA>
    Scalar analyticalYVelocityBetweenCorners_ (const LambdaA& possiblyInstationaryAnalyticalSolution, const std::array<GlobalPosition, 2>& corners, Scalar time) const
    {
        return possiblyInstationaryPossiblyMeanAnalyticalVelocityySolutionAtPos_(possiblyInstationaryAnalyticalSolution, corners, 1, time);
    }

    template <class LambdaA>
    Scalar possiblyInstationaryAnalyticalPressureAtPos_ (const LambdaA& possiblyInstationaryAnalyticalSolution, const GlobalPosition& globalPos, Scalar time) const
    {
        if ((*problem_).useScvVolumeAveraging())
            DUNE_THROW(Dune::InvalidStateException, "Residual calc can only do no averaging for scv.");

        return possiblyInstationaryAnalyticalSolution(globalPos,time)[Indices::pressureIdx];
    }

    template <class LambdaA, class LambdaB>
    Scalar momentumXResidual_(const LambdaA& possiblyInstationaryAnalyticalSolution, const LambdaB& possiblyInstationarySourceAtPos, bool isStationary, Scalar t, Scalar tOld, Scalar x, Scalar y, Scalar leftOppoDelta, Scalar rightOppoDelta, Scalar upLateralDelta, Scalar scvfDelta, Scalar downLateralDelta) const
    {
    //     std::cout << "x= " << x << ", y = " << y << ", leftOppoDelta = " << leftOppoDelta << ", rightOppoDelta = " << rightOppoDelta << ", upLateralDelta = " << upLateralDelta << ", scvfDelta = " << scvfDelta << ", downLateralDelta = " << downLateralDelta << std::endl;
        std::array<GlobalPosition, 2> selfCorners, parallelUpCorners, parallelDownCorners, oppoRightCorners, oppoLeftCorners, normalUpRightCorners, normalUpLeftCorners, normalDownRightCorners, normalDownLeftCorners;

        selfCorners[0] = {x,y-0.5*scvfDelta};
        selfCorners[1] = {x,y+0.5*scvfDelta};

        parallelUpCorners   [0] = {x, y+.5*scvfDelta};
        parallelUpCorners   [1] = {x, y+.5*scvfDelta+upLateralDelta};
        parallelDownCorners [0] = {x, y-.5*scvfDelta-downLateralDelta};
        parallelDownCorners [1] = {x, y-.5*scvfDelta};
        oppoRightCorners    [0] = {x+rightOppoDelta, y-0.5*scvfDelta};
        oppoRightCorners    [1] = {x+rightOppoDelta, y+0.5*scvfDelta};
        oppoLeftCorners     [0] = {x-leftOppoDelta, y-0.5*scvfDelta};
        oppoLeftCorners     [1] = {x-leftOppoDelta, y+0.5*scvfDelta};

        normalUpRightCorners[0]   = {x,                y+.5*scvfDelta};
        normalUpRightCorners[1]   = {x+rightOppoDelta, y+.5*scvfDelta};
        normalUpLeftCorners[0]    = {x-leftOppoDelta,  y+.5*scvfDelta};
        normalUpLeftCorners[1]    = {x,                y+.5*scvfDelta};
        normalDownRightCorners[0] = {x,                y-.5*scvfDelta};
        normalDownRightCorners[1] = {x+rightOppoDelta, y-.5*scvfDelta};
        normalDownLeftCorners [0] = {x-leftOppoDelta,  y-.5*scvfDelta};
        normalDownLeftCorners [1] = {x,                y-.5*scvfDelta};

        Scalar deltaT = t-tOld;
        Scalar q = possiblyInstationarySourceAtPos({x,y},t)[Indices::momentumXBalanceIdx];

        Scalar velSelfOld = isStationary? 0. : analyticalXVelocityBetweenCorners_ (possiblyInstationaryAnalyticalSolution, selfCorners,             tOld);
        Scalar velSelf =                       analyticalXVelocityBetweenCorners_ (possiblyInstationaryAnalyticalSolution,  selfCorners,            t);

        Scalar velParallelRightOrUp =          analyticalXVelocityBetweenCorners_ (possiblyInstationaryAnalyticalSolution,  parallelUpCorners,      t);
        Scalar velParallelLeftOrDown =         analyticalXVelocityBetweenCorners_ (possiblyInstationaryAnalyticalSolution,  parallelDownCorners,    t);
        Scalar velOppoRightOrUp =              analyticalXVelocityBetweenCorners_ (possiblyInstationaryAnalyticalSolution,  oppoRightCorners,       t);
        Scalar velOppoLeftOrDown =             analyticalXVelocityBetweenCorners_ (possiblyInstationaryAnalyticalSolution,  oppoLeftCorners,        t);

        Scalar velNormalUpRightOrRightUp =     analyticalYVelocityBetweenCorners_ (possiblyInstationaryAnalyticalSolution,  normalUpRightCorners,   t);
        Scalar velNormalUpLeftOrRightDown =    analyticalYVelocityBetweenCorners_ (possiblyInstationaryAnalyticalSolution,  normalUpLeftCorners,    t);
        Scalar velNormalDownRightOrLeftUp =    analyticalYVelocityBetweenCorners_ (possiblyInstationaryAnalyticalSolution,  normalDownRightCorners, t);
        Scalar velNormalDownLeftOrLeftDown =   analyticalYVelocityBetweenCorners_ (possiblyInstationaryAnalyticalSolution,  normalDownLeftCorners,  t);

        Scalar presLeftOrDown = possiblyInstationaryAnalyticalPressureAtPos_(possiblyInstationaryAnalyticalSolution, {x-0.5*leftOppoDelta,y},t);
        Scalar presRightOrUp =  possiblyInstationaryAnalyticalPressureAtPos_(possiblyInstationaryAnalyticalSolution, {x+0.5*rightOppoDelta,y},t);

        bool navierStokes = getParam<bool>("Problem.EnableInertiaTerms");
        Scalar viscosity = getParam<Scalar>("Component.LiquidKinematicViscosity");

        return residual_(isStationary,
                        velSelfOld,
                        deltaT,
                        leftOppoDelta,
                        rightOppoDelta,
                        upLateralDelta,
                        scvfDelta,
                        downLateralDelta,
                        q,
                        velSelf,
                        velParallelRightOrUp,
                        velParallelLeftOrDown,
                        velOppoRightOrUp,
                        velOppoLeftOrDown,
                        velNormalUpRightOrRightUp,
                        velNormalUpLeftOrRightDown,
                        velNormalDownRightOrLeftUp,
                        velNormalDownLeftOrLeftDown,
                        presLeftOrDown,
                        presRightOrUp,
                        navierStokes,
                        viscosity);
    }

    template <class LambdaA, class LambdaB>
    Scalar momentumYResidual_(const LambdaA& possiblyInstationaryAnalyticalSolution, const LambdaB& possiblyInstationarySourceAtPos, bool isStationary, Scalar t, Scalar tOld, Scalar x, Scalar y, Scalar downOppoDelta, Scalar upOppoDelta, Scalar rightLateralDelta, Scalar scvfDelta, Scalar leftLateralDelta) const
    {
        std::array<GlobalPosition, 2> selfCorners, parallelRightCorners, parallelLeftCorners, oppoUpCorners, oppoDownCorners, normalRightUpCorners, normalRightDownCorners, normalLeftUpCorners, normalLeftDownCorners;

        selfCorners[0] = {x-0.5*scvfDelta,y};
        selfCorners[1] = {x+0.5*scvfDelta,y};

        parallelRightCorners[0] = {x+.5*scvfDelta,                   y};
        parallelRightCorners[1] = {x+.5*scvfDelta+rightLateralDelta, y};
        parallelLeftCorners [0] = {x-.5*scvfDelta-leftLateralDelta,  y};
        parallelLeftCorners [1] = {x-.5*scvfDelta,                   y};
        oppoUpCorners       [0] = {x-0.5*scvfDelta, y+upOppoDelta};
        oppoUpCorners       [1] = {x+0.5*scvfDelta, y+upOppoDelta};
        oppoDownCorners     [0] = {x-0.5*scvfDelta, y-downOppoDelta};
        oppoDownCorners     [1] = {x+0.5*scvfDelta, y-downOppoDelta};

        normalRightUpCorners  [0] = {x+.5*scvfDelta, y};
        normalRightUpCorners  [1] = {x+.5*scvfDelta, y+upOppoDelta};
        normalRightDownCorners[0] = {x+.5*scvfDelta, y-downOppoDelta};
        normalRightDownCorners[1] = {x+.5*scvfDelta, y};
        normalLeftUpCorners   [0] = {x-.5*scvfDelta, y};
        normalLeftUpCorners   [1] = {x-.5*scvfDelta, y+upOppoDelta};
        normalLeftDownCorners [0] = {x-.5*scvfDelta, y-downOppoDelta};
        normalLeftDownCorners [1] = {x-.5*scvfDelta, y};

        Scalar deltaT = t-tOld;
        Scalar q = possiblyInstationarySourceAtPos({x,y},t)[Indices::momentumYBalanceIdx];

        Scalar velSelfOld = isStationary? 0. : analyticalYVelocityBetweenCorners_ (possiblyInstationaryAnalyticalSolution,  selfCorners,            tOld);
        Scalar velSelf =                       analyticalYVelocityBetweenCorners_ (possiblyInstationaryAnalyticalSolution,  selfCorners,            t);

        Scalar velParallelRightOrUp =          analyticalYVelocityBetweenCorners_ (possiblyInstationaryAnalyticalSolution,  parallelRightCorners,   t);
        Scalar velParallelLeftOrDown =         analyticalYVelocityBetweenCorners_ (possiblyInstationaryAnalyticalSolution,  parallelLeftCorners,    t);
        Scalar velOppoRightOrUp =              analyticalYVelocityBetweenCorners_ (possiblyInstationaryAnalyticalSolution,  oppoUpCorners,          t);
        Scalar velOppoLeftOrDown =             analyticalYVelocityBetweenCorners_ (possiblyInstationaryAnalyticalSolution,  oppoDownCorners,        t);

        Scalar velNormalUpRightOrRightUp =     analyticalXVelocityBetweenCorners_ (possiblyInstationaryAnalyticalSolution,  normalRightUpCorners,   t);
        Scalar velNormalUpLeftOrRightDown =    analyticalXVelocityBetweenCorners_ (possiblyInstationaryAnalyticalSolution,  normalRightDownCorners, t);
        Scalar velNormalDownRightOrLeftUp =    analyticalXVelocityBetweenCorners_ (possiblyInstationaryAnalyticalSolution,  normalLeftUpCorners,    t);
        Scalar velNormalDownLeftOrLeftDown =   analyticalXVelocityBetweenCorners_ (possiblyInstationaryAnalyticalSolution,  normalLeftDownCorners,  t);

        Scalar presLeftOrDown = possiblyInstationaryAnalyticalPressureAtPos_(possiblyInstationaryAnalyticalSolution, {x,y-.5*downOppoDelta}, t);
        Scalar presRightOrUp =  possiblyInstationaryAnalyticalPressureAtPos_(possiblyInstationaryAnalyticalSolution, {x,y+.5*upOppoDelta}, t);

        bool navierStokes = getParam<bool>("Problem.EnableInertiaTerms");
        Scalar viscosity = getParam<Scalar>("Component.LiquidKinematicViscosity");

        return residual_(isStationary,
                        velSelfOld,
                        deltaT,
                        downOppoDelta,
                        upOppoDelta,
                        rightLateralDelta,
                        scvfDelta,
                        leftLateralDelta,
                        q,
                        velSelf,
                        velParallelRightOrUp,
                        velParallelLeftOrDown,
                        velOppoRightOrUp,
                        velOppoLeftOrDown,
                        velNormalUpRightOrRightUp,
                        velNormalUpLeftOrRightDown,
                        velNormalDownRightOrLeftUp,
                        velNormalDownLeftOrLeftDown,
                        presLeftOrDown,
                        presRightOrUp,
                        navierStokes,
                        viscosity);
    }

    std::string seqName_(unsigned int count) const
    {
      std::stringstream n;
      n.fill('0');
      n << "-" << std::setw(5) << count;
      return n.str();
    }

    // output the residuals that we would get for perfect interpolation.
    template<class LambdaA, class LambdaB, class LambdaBC, class LambdaC, class LambdaD, class LambdaE, class LambdaF, class CVLeafGridView>
    void fillResidual_(unsigned int timeIndex,
                       const LambdaA& possiblyInstationaryAnalyticalSolution,
                       const LambdaB& possiblyInstationarySourceAtPos,
                       const LambdaBC& fineToCoarseInside,
                       const LambdaC& coarseToFineInside,
                       const LambdaD& coarseToFineLeft,
                       const LambdaE& coarseToFineRight,
                       const LambdaF& coarseToFineRightEdgeDualGrid,
                       bool isStationary,
                       Scalar t,
                       Scalar tOld,
                       std::vector<Scalar>& dofOptionBorDStencilPositionsMineResidual,
                       const CVLeafGridView& cvLeafGridView,
                       const std::map<GlobalPosition, std::vector<unsigned int>, ContainerCmpClass<GlobalPosition>>& cvCenterScvfIndicesMap,
                       unsigned int dirIdx,
                       bool dofAtHangingNode /*CV options A,C,E*/ )
    {
        dofOptionBorDStencilPositionsMineResidual.resize(cvLeafGridView.size(0));

        //gnuplotfileoutput
        std::string name;
        if (dirIdx == 0 & dofAtHangingNode == false)
            name = "CVOptionD_StandardStencilPositions_localTruncErrorMomWithoutInterpol_x";
        else if (dirIdx == 1 & dofAtHangingNode == false)
            name = "CVOptionD_StandardStencilPositions_localTruncErrorMomWithoutInterpol_y";
        else if (dirIdx == 0 & dofAtHangingNode == true)
            name = "CVOptionC_StandardStencilPositions_localTruncErrorMomWithoutInterpol_x";
        else if (dirIdx == 1 & dofAtHangingNode == true)
            name = "CVOptionC_StandardStencilPositions_localTruncErrorMomWithoutInterpol_y";
        else
            DUNE_THROW(Dune::InvalidStateException, "");

        std::string myStencilPositionsName;
        if (dirIdx == 0 & dofAtHangingNode == false)
            myStencilPositionsName = "CVOptionD_myStencilPositions_localTruncErrorMomWithoutInterpol_x";
        else if (dirIdx == 1 & dofAtHangingNode == false)
            myStencilPositionsName = "CVOptionD_myStencilPositions_localTruncErrorMomWithoutInterpol_y";
        else
            myStencilPositionsName = "trash";

        std::string noStretchingName;
        if (dirIdx == 0 & dofAtHangingNode == false)
            noStretchingName = "CVOptionB_StandardStencilPositions_localTruncErrorMomWithoutInterpol_x";
        else if (dirIdx == 1 & dofAtHangingNode == false)
            noStretchingName = "CVOptionB_StandardStencilPositions_localTruncErrorMomWithoutInterpol_y";
        else if (dirIdx == 0 & dofAtHangingNode == true)
            noStretchingName = "CVOptionA_StandardStencilPositions_localTruncErrorMomWithoutInterpol_x";
        else if (dirIdx == 1 & dofAtHangingNode == true)
            noStretchingName = "CVOptionA_StandardStencilPositions_localTruncErrorMomWithoutInterpol_y";
        else
            DUNE_THROW(Dune::InvalidStateException, "");

        std::string myStencilPositionsNoStretchingName;
        if (dirIdx == 0 & dofAtHangingNode == false)
            myStencilPositionsNoStretchingName = "CVOptionB_MyStencilPositions_localTruncErrorMomWithoutInterpol_x";
        else if (dirIdx == 1 & dofAtHangingNode == false)
            myStencilPositionsNoStretchingName = "CVOptionB_MyStencilPositions_localTruncErrorMomWithoutInterpol_y";
        else if (dirIdx == 0 & dofAtHangingNode == true)
            myStencilPositionsNoStretchingName = "CVOptionA_MyStencilPositions_localTruncErrorMomWithoutInterpol_x";
        else if (dirIdx == 1 & dofAtHangingNode == true)
            myStencilPositionsNoStretchingName = "CVOptionA_MyStencilPositions_localTruncErrorMomWithoutInterpol_y";
        else
            DUNE_THROW(Dune::InvalidStateException, "");

        std::string projectionCoarseOnCVOptionDName;
        if (dirIdx == 0 & dofAtHangingNode == false)
            projectionCoarseOnCVOptionDName = "projectionCoarseOnCVOptionD_x";
        else if (dirIdx == 1 & dofAtHangingNode == false)
            projectionCoarseOnCVOptionDName = "projectionCoarseOnCVOptionD_y";
        else if (dofAtHangingNode == true)
            projectionCoarseOnCVOptionDName = "trashtrash";
        else
            DUNE_THROW(Dune::InvalidStateException, "");

        std::string projectionCVOptionDOnCoarseName;
        if (dirIdx == 0 & dofAtHangingNode == false)
            projectionCVOptionDOnCoarseName = "projectionCVOptionDOnCoarse_x";
        else if (dirIdx == 1 & dofAtHangingNode == false)
            projectionCVOptionDOnCoarseName = "projectionCVOptionDOnCoarse_y";
        else if (dofAtHangingNode == true)
            projectionCVOptionDOnCoarseName = "trashtrash";
        else
            DUNE_THROW(Dune::InvalidStateException, "");

        std::ofstream CVOptionCorDFileStandardStencilPositions;
        CVOptionCorDFileStandardStencilPositions.open(name + seqName_(timeIndex) + ".csv");
        if (dofAtHangingNode)
            CVOptionCorDFileStandardStencilPositions << "xMin" << "," << "yMin" << "," << "xMax" << "," << "yMax" << "," << "dof option a or c" << "," << "dof option f" << std::endl;
        else
            CVOptionCorDFileStandardStencilPositions << "xMin" << "," << "yMin" << "," << "xMax" << "," << "yMax" << "," << "dof option b or d" << "," << "dof option g" << std::endl;

        std::ofstream CVOptionDFileMyStencilPositions;
        CVOptionDFileMyStencilPositions.open(myStencilPositionsName + seqName_(timeIndex) + ".csv");
        CVOptionDFileMyStencilPositions << "xMin" << "," << "yMin" << "," << "xMax" << "," << "yMax" << "," << "dof option b or d" << std::endl;

        std::ofstream CVOptionAorBFileStandardStencilPositions;
        CVOptionAorBFileStandardStencilPositions.open(noStretchingName + seqName_(timeIndex) + ".csv");
        if (dofAtHangingNode)
            CVOptionAorBFileStandardStencilPositions << "xMin" << "," << "yMin" << "," << "xMax" << "," << "yMax" << "," << "dof option a or c" << std::endl;
        else
            CVOptionAorBFileStandardStencilPositions << "xMin" << "," << "yMin" << "," << "xMax" << "," << "yMax" << "," << "dof option b or d" << std::endl;

        std::ofstream CVOptionAorBFileMyStencilPositions;
        CVOptionAorBFileMyStencilPositions.open(myStencilPositionsNoStretchingName + seqName_(timeIndex) + ".csv");
        if (dofAtHangingNode)
            CVOptionAorBFileMyStencilPositions << "xMin" << "," << "yMin" << "," << "xMax" << "," << "yMax" << "," << "dof option a or c" << std::endl;
        else
            CVOptionAorBFileMyStencilPositions << "xMin" << "," << "yMin" << "," << "xMax" << "," << "yMax" << "," << "dof option b or d" << std::endl;

        std::ofstream projectionCoarseOnCVOptionD;
        projectionCoarseOnCVOptionD.open(projectionCoarseOnCVOptionDName + seqName_(timeIndex) + ".csv");
        projectionCoarseOnCVOptionD << "xMin" << "," << "yMin" << "," << "xMax" << "," << "yMax" << "," << "dof option g" << std::endl;

        std::ofstream projectionCVOptionDOnCoarse;
        projectionCVOptionDOnCoarse.open(projectionCVOptionDOnCoarseName + seqName_(timeIndex) + ".csv");
        projectionCVOptionDOnCoarse << "xMin" << "," << "yMin" << "," << "xMax" << "," << "yMax" << "," << "dof option g" << std::endl;

        for (const auto& controlVolume : elements(cvLeafGridView))
        {
    std::vector<unsigned int> scvfIndices;//size 1 value at boundary, 2 usually, 4 for dofAtHangingNode transition face
    auto it = cvCenterScvfIndicesMap.find(controlVolume.geometry().center());

    bool transitionCV = false;
    SubControlVolumeFace scvf;
    SubControlVolumeFace otherFace;

    if (it == cvCenterScvfIndicesMap.end())
    {
        std::cout << "looking for " << controlVolume.geometry().center() << std::endl;

        for ( const auto& entry : cvCenterScvfIndicesMap )
        {
            std::cout << "map entry cv center = " << entry.first;
            for ( const auto& val : entry.second)
                std::cout << " " << val;
            std::cout << std::endl;
        }

        DUNE_THROW(Dune::InvalidStateException, "");
    }
    else
    {
        scvfIndices = (*it).second;

        if (scvfIndices.size() == 1 /*boundary*/ || scvfIndices.size() == 2 /*inner,non-transition face*/)
        {
            scvf = (*problem_).gridGeometry().scvf(scvfIndices[0]);
        }
        else if (dofAtHangingNode && scvfIndices.size() == 4)/*transition face*/
        {
            transitionCV = true;

            const auto& scvf0 = (*problem_).gridGeometry().scvf(scvfIndices[0]);
            const auto& scvf1 = (*problem_).gridGeometry().scvf(scvfIndices[1]);
            const auto& scvf2 = (*problem_).gridGeometry().scvf(scvfIndices[2]);

            scvf = scvf0;

            if (!containerCmp(scvf1.center(), scvf0.center()))
                otherFace = scvf1;
            else
                otherFace = scvf2;
        }
        else
        {
            for (const auto& scvfIndex : scvfIndices)
            {
                const auto& scvf = (*problem_).gridGeometry().scvf(scvfIndex);
                std::cout << "scvf at " << scvf.center() << std::endl;
            }

            DUNE_THROW(Dune::InvalidStateException, "size = " << scvfIndices.size() << " at " << controlVolume.geometry().center());
        }
    }

        if (dirIdx != scvf.directionIndex())
            DUNE_THROW(Dune::InvalidStateException, "dirIdx = " << dirIdx << ", scvf.directionIndex() = " << scvf.directionIndex() << ", scvf center = " << scvf.center());

        int nondirIdx = (dirIdx == 0)?1:0;

        unsigned int index = cvLeafGridView.indexSet().index(controlVolume);

        Scalar dirCenter = scvf.center()[dirIdx];
        Scalar nonDirCenter = transitionCV?(.5*(scvf.center()[nondirIdx]+otherFace.center()[nondirIdx])):scvf.center()[nondirIdx];

        Scalar x = (dirIdx == 0)?dirCenter:nonDirCenter;
        Scalar y = (dirIdx == 1)?dirCenter:nonDirCenter;

        std::vector<Scalar> dirValues;
        std::vector<Scalar> nonDirValues;

        for (unsigned int i=0; i < controlVolume.geometry().corners(); ++i)
        {
            const auto& corner = controlVolume.geometry().corner(i);

            dirValues.push_back(corner[dirIdx]);
            nonDirValues.push_back(corner[nondirIdx]);
        }

        std::sort(dirValues.begin(),dirValues.end());
        std::sort(nonDirValues.begin(),nonDirValues.end());

        Scalar lowerDirValue = dirValues[0];
        Scalar higherDirValue = dirValues[3];

        Scalar standardOppoDeltaLower = 2*(dirCenter-lowerDirValue);
        Scalar standardOppoDeltaHigher = 2*(higherDirValue - dirCenter);
        Scalar noStretchingOppoDelta = dofAtHangingNode ? std::max(standardOppoDeltaLower,standardOppoDeltaHigher) : std::min(standardOppoDeltaLower,standardOppoDeltaHigher);

        Scalar standardMidLateralDelta = transitionCV?2.*scvf.area():scvf.area();

        int upOrRightSubFaceIdx;
        int downOrLeftSubFaceIdx;

        const auto& normalFluxCorrectionFace = (*problem_).gridGeometry().scvf(scvf.insideScvIdx(), scvf.pairData(0).localNormalFluxCorrectionIndex);
        if (normalFluxCorrectionFace.unitOuterNormal()[1]==1)
        {
            upOrRightSubFaceIdx = 0;
            downOrLeftSubFaceIdx = 1;
        }
        else
        {
            upOrRightSubFaceIdx = 1;
            downOrLeftSubFaceIdx = 0;
        }

        Scalar nonStandardHigherLateralDelta;
        if (!transitionCV || (scvf.center()[nondirIdx]>otherFace.center()[nondirIdx]))
        {
            nonStandardHigherLateralDelta = scvf.pairData(upOrRightSubFaceIdx).parallelDistances[1];
        }
        else
        {
            nonStandardHigherLateralDelta = otherFace.pairData(upOrRightSubFaceIdx).parallelDistances[1];
        }

        Scalar nonStandardLowerLateralDelta;
        if (!transitionCV || (scvf.center()[nondirIdx]<otherFace.center()[nondirIdx]))
        {
            nonStandardLowerLateralDelta = scvf.pairData(downOrLeftSubFaceIdx).parallelDistances[1];
        }
        else
        {
            nonStandardLowerLateralDelta = otherFace.pairData(downOrLeftSubFaceIdx).parallelDistances[1];
        }

        Scalar standardLowerLateralDelta = (scalarCmp(nonStandardLowerLateralDelta,0.)/*true at the boundary*/)?0.:standardMidLateralDelta;
        Scalar standardHigherLateralDelta = (scalarCmp(nonStandardHigherLateralDelta,0.)/*true at the boundary*/)?0.:standardMidLateralDelta;

        if (scvf.boundary())
        {
            using Indices = typename GetPropType<TypeTag, Properties::ModelTraits>::Indices;

            auto bcTypes = (*problem_).boundaryTypesAtPos(scvf.center());
            if (!bcTypes.isDirichlet(Indices::velocity(scvf.directionIndex())))
                DUNE_THROW(Dune::InvalidStateException, "Do not use the residual calculation with outflow BCs. Not prepared so far.");

            dofOptionBorDStencilPositionsMineResidual[index] = 0.;

//             //gnuplotfileoutput
//             std::vector<Scalar> xValues;
//             std::vector<Scalar> yValues;
//             for (unsigned int i=0; i < controlVolume.geometry().corners(); ++i)
//             {
//                 xValues.push_back(controlVolume.geometry().corner(i)[0]);
//                 yValues.push_back(controlVolume.geometry().corner(i)[1]);
//             }
//
//             typename std::vector<Scalar>::iterator xMinIter = std::min_element(xValues.begin(), xValues.end());
//             typename std::vector<Scalar>::iterator xMaxIter = std::max_element(xValues.begin(), xValues.end());
//             typename std::vector<Scalar>::iterator yMinIter = std::min_element(yValues.begin(), yValues.end());
//             typename std::vector<Scalar>::iterator yMaxIter = std::max_element(yValues.begin(), yValues.end());
//
//             Scalar xMin = *xMinIter;
//             Scalar xMax = *xMaxIter;
//             Scalar yMin = *yMinIter;
//             Scalar yMax = *yMaxIter;
//
//             CVOptionCorDFileStandardStencilPositions << xMin << "," << yMin << "," << xMax << "," << yMax << "," << 0. << std::endl;
//             CVOptionAorBFileStandardStencilPositions << xMin << "," << yMin << "," << xMax << "," << yMax << "," << 0. << std::endl;
            continue;
        }

        Scalar higherLateralDelta;
        Scalar lowerLateralDelta;

        if (!dofAtHangingNode)
        {
            higherLateralDelta = nonStandardHigherLateralDelta;
            lowerLateralDelta = nonStandardLowerLateralDelta;

            dofOptionBorDStencilPositionsMineResidual[index] =
            (dirIdx==0)? momentumXResidual_(possiblyInstationaryAnalyticalSolution, possiblyInstationarySourceAtPos, isStationary, t, tOld, x, y, standardOppoDeltaLower, standardOppoDeltaHigher, higherLateralDelta, standardMidLateralDelta, lowerLateralDelta)
                        :momentumYResidual_(possiblyInstationaryAnalyticalSolution, possiblyInstationarySourceAtPos, isStationary, t, tOld, x, y, standardOppoDeltaLower, standardOppoDeltaHigher, higherLateralDelta, standardMidLateralDelta, lowerLateralDelta);
        }

        Scalar dofOptionAorCStencilPositionsMineResidual =
            (dirIdx==0)? momentumXResidual_(possiblyInstationaryAnalyticalSolution, possiblyInstationarySourceAtPos, isStationary, t, tOld, x, y, noStretchingOppoDelta, noStretchingOppoDelta, higherLateralDelta, standardMidLateralDelta, lowerLateralDelta)
                    :momentumYResidual_(possiblyInstationaryAnalyticalSolution, possiblyInstationarySourceAtPos, isStationary, t, tOld, x, y, noStretchingOppoDelta, noStretchingOppoDelta, higherLateralDelta, standardMidLateralDelta, lowerLateralDelta);

        higherLateralDelta = standardHigherLateralDelta;
        lowerLateralDelta = standardLowerLateralDelta;

        Scalar dofOptionBorDStencilPositionsStandardResidual =
            (dirIdx==0)? momentumXResidual_(possiblyInstationaryAnalyticalSolution, possiblyInstationarySourceAtPos, isStationary, t, tOld, x, y, standardOppoDeltaLower, standardOppoDeltaHigher, higherLateralDelta, standardMidLateralDelta, lowerLateralDelta)
                        :momentumYResidual_(possiblyInstationaryAnalyticalSolution, possiblyInstationarySourceAtPos, isStationary, t, tOld, x, y, standardOppoDeltaLower, standardOppoDeltaHigher, higherLateralDelta, standardMidLateralDelta, lowerLateralDelta);

        Scalar dofOptionAorCStencilPositionsStandardResidual =
            (dirIdx==0)? momentumXResidual_(possiblyInstationaryAnalyticalSolution, possiblyInstationarySourceAtPos, isStationary, t, tOld, x, y, noStretchingOppoDelta, noStretchingOppoDelta, higherLateralDelta, standardMidLateralDelta, lowerLateralDelta)
                        :momentumYResidual_(possiblyInstationaryAnalyticalSolution, possiblyInstationarySourceAtPos, isStationary, t, tOld, x, y, noStretchingOppoDelta, noStretchingOppoDelta, higherLateralDelta, standardMidLateralDelta, lowerLateralDelta);

        Scalar dofOptionGorFX = (dirIdx == 0) ? x + .25*(standardOppoDeltaHigher - standardOppoDeltaLower) : x;
        Scalar dofOptionGorFY = (dirIdx == 1) ? y + .25*(standardOppoDeltaHigher - standardOppoDeltaLower) : y;
        Scalar dofOptionGorFstandardOppoDeltaLower = higherDirValue - lowerDirValue;
        Scalar dofOptionGorFstandardOppoDeltaHigher = higherDirValue - lowerDirValue;

        Scalar dofOptionGOrFStencilPositionsStandardResidual =
            (dirIdx==0)? momentumXResidual_(possiblyInstationaryAnalyticalSolution, possiblyInstationarySourceAtPos, isStationary, t, tOld, dofOptionGorFX, dofOptionGorFY, dofOptionGorFstandardOppoDeltaLower, dofOptionGorFstandardOppoDeltaHigher, higherLateralDelta, standardMidLateralDelta, lowerLateralDelta)
                        :momentumYResidual_(possiblyInstationaryAnalyticalSolution, possiblyInstationarySourceAtPos, isStationary, t, tOld, dofOptionGorFX, dofOptionGorFY, dofOptionGorFstandardOppoDeltaLower, dofOptionGorFstandardOppoDeltaHigher, higherLateralDelta, standardMidLateralDelta, lowerLateralDelta);

            //gnuplotfileoutput
            std::vector<Scalar> xValues;
            std::vector<Scalar> yValues;
            for (unsigned int i=0; i < controlVolume.geometry().corners(); ++i)
            {
                xValues.push_back(controlVolume.geometry().corner(i)[0]);
                yValues.push_back(controlVolume.geometry().corner(i)[1]);
            }

            typename std::vector<Scalar>::iterator xMinIter = std::min_element(xValues.begin(), xValues.end());
            typename std::vector<Scalar>::iterator xMaxIter = std::max_element(xValues.begin(), xValues.end());
            typename std::vector<Scalar>::iterator yMinIter = std::min_element(yValues.begin(), yValues.end());
            typename std::vector<Scalar>::iterator yMaxIter = std::max_element(yValues.begin(), yValues.end());

            Scalar xMin = *xMinIter;
            Scalar xMax = *xMaxIter;
            Scalar yMin = *yMinIter;
            Scalar yMax = *yMaxIter;

            Scalar noStretchingXMin = (dirIdx==0) ? x-.5*noStretchingOppoDelta : xMin;
            Scalar noStretchingXMax = (dirIdx==0) ? x+.5*noStretchingOppoDelta : xMax;
            Scalar noStretchingYMin = (dirIdx==1) ? y-.5*noStretchingOppoDelta : yMin;
            Scalar noStretchingYMax = (dirIdx==1) ? y+.5*noStretchingOppoDelta : yMax;

            CVOptionDFileMyStencilPositions << xMin << "," << yMin << "," << xMax << "," << yMax << "," << std::setprecision(10) << dofOptionBorDStencilPositionsMineResidual[index] << std::endl;
            CVOptionCorDFileStandardStencilPositions << xMin << "," << yMin << "," << xMax << "," << yMax << "," << std::setprecision(10) << dofOptionBorDStencilPositionsStandardResidual << ","  << std::setprecision(10) << dofOptionGOrFStencilPositionsStandardResidual << std::endl;
            CVOptionAorBFileStandardStencilPositions << noStretchingXMin << "," << noStretchingYMin << "," << noStretchingXMax << "," << noStretchingYMax << "," << std::setprecision(10) << dofOptionAorCStencilPositionsStandardResidual <<  std::endl;
            CVOptionAorBFileMyStencilPositions << noStretchingXMin << "," << noStretchingYMin << "," << noStretchingXMax << "," << noStretchingYMax << "," << std::setprecision(10) << dofOptionAorCStencilPositionsMineResidual <<  std::endl;

            GlobalPosition globalPos;
            globalPos[0]=dirCenter;
            globalPos[1]=.5*(yMin+yMax);

            projectionCoarseOnCVOptionDFunc(xMin, xMax, yMin, yMax, globalPos, projectionCoarseOnCVOptionD, dirIdx, coarseToFineInside, coarseToFineLeft, coarseToFineRight, coarseToFineRightEdgeDualGrid, dofOptionGOrFStencilPositionsStandardResidual);
            projectionCVOptionDOnCoarseFunc(xMin, xMax, yMin, yMax, globalPos, projectionCVOptionDOnCoarse, dirIdx, fineToCoarseInside, dofOptionGOrFStencilPositionsStandardResidual);
        }

        CVOptionCorDFileStandardStencilPositions.close();
        CVOptionAorBFileStandardStencilPositions.close();
        CVOptionAorBFileMyStencilPositions.close();
        CVOptionDFileMyStencilPositions.close();
        projectionCoarseOnCVOptionD.close();
        projectionCVOptionDOnCoarse.close();
    }

    /*
    void printResidualsOfGeometry_ (Scalar coarseDeltaX, Scalar coarseDeltaY, Scalar centralX, Scalar centralY) const
    {
    //////
        std::cout << "coarse: " << std::endl;
    //x
    // -------------------------------------
    // |           |           |           |
    // |           |           |           |
    // |           +           +           |
    // |           |           |           |
    // |           |           |           |
    // ------+lllllllllll+rrrrrrrrrrr+------
    // |     l     |     *     |     r     |
    // |     l     |     *     |     r     |
    // +     o     +     o     +     o     +
    // |     l     |     *     |     r     |
    // |     l     |     *     |     r     |
    // ------+lllllllllll+rrrrrrrrrrr+------
    // |           |           |           |
    // |           |           |           |
    // |           +           +           |
    // |           |           |           |
    // |           |           |           |
    // -------------------------------------
    // l: control volume named left
    // r: control volume named right
    // *: overlap of mid/right control volume boundary
    // +: relevant velocity dof positions
    // o: relevant pressure dof positions
        Scalar right = momentumXResidual_(centralX+.5*coarseDeltaX, centralY, coarseDeltaX, coarseDeltaX, coarseDeltaY, coarseDeltaY, coarseDeltaY);
        Scalar left = momentumXResidual_(centralX-.5*coarseDeltaX, centralY, coarseDeltaX, coarseDeltaX, coarseDeltaY, coarseDeltaY, coarseDeltaY);
        std::cout << "momentum x: right " << right << ", left " << left << std::endl;

    //y
        Scalar up = momentumYResidual_(centralX, centralY+.5*coarseDeltaY, coarseDeltaY, coarseDeltaY, coarseDeltaX, coarseDeltaX, coarseDeltaX);
        Scalar down = momentumYResidual_(centralX, centralY-.5*coarseDeltaY, coarseDeltaY, coarseDeltaY, coarseDeltaX, coarseDeltaX, coarseDeltaX);
        std::cout << "momentum y: up " << up << ", down " << down << std::endl;

    /////
        std::cout << "perfect interpolation " << std::endl;
    //x
    // -------------------------------------
    // |           |           |           |
    // |           |           |           |
    // |           |           |           |
    // |           |           |           |
    // |           |           |           |
    // |           +     +     +           |
    // |           |           |           |
    // ------+lulululu+mumum+rurururu+------
    // |     l     |  *  |  *  |     r     |
    // +     o     +  o  +  o  +     o     +
    // |     u     |  *  |  *  |     u     |
    // |     +********+*****+********+     |
    // |     l     |  *  |  *  |     r     |
    // +     o     +  o  +  o  +     o     +
    // |     d     |  *  |  *  |     d     |
    // ------+ldldldld+mdmdm+rdrdrdrd+------
    // |           |           |           |
    // |           +     +     +           |
    // |           |           |           |
    // |           |           |           |
    // |           |           |           |
    // |           |           |           |
    // |           |           |           |
    // -------------------------------------
    // lu: control volume named left up
    // ru: control volume named right up
    // ld: control volume named left down
    // rd: control volume named right down
    // ...
    // *: overlap of mid/left or mid/right control volume boundary
    // +: relevant velocity dof positions
    // o: relevant pressure dof positions
        Scalar rightUp = momentumXResidual_(  centralX+.5*coarseDeltaX, centralY+0.25*coarseDeltaY, .5*coarseDeltaX,    coarseDeltaX, .5*coarseDeltaY, .5*coarseDeltaY, .5*coarseDeltaY);
        Scalar rightDown = momentumXResidual_(centralX+.5*coarseDeltaX, centralY-0.25*coarseDeltaY, .5*coarseDeltaX,    coarseDeltaX, .5*coarseDeltaY, .5*coarseDeltaY, .5*coarseDeltaY);
        Scalar leftUp = momentumXResidual_(   centralX-.5*coarseDeltaX, centralY+0.25*coarseDeltaY,    coarseDeltaX, .5*coarseDeltaX, .5*coarseDeltaY, .5*coarseDeltaY, .5*coarseDeltaY);
        Scalar leftDown = momentumXResidual_( centralX-.5*coarseDeltaX, centralY-0.25*coarseDeltaY,    coarseDeltaX, .5*coarseDeltaX, .5*coarseDeltaY, .5*coarseDeltaY, .5*coarseDeltaY);
        Scalar midUp = momentumXResidual_(    centralX,                 centralY+0.25*coarseDeltaY, .5*coarseDeltaX, .5*coarseDeltaX, .5*coarseDeltaY, .5*coarseDeltaY, .5*coarseDeltaY);
        Scalar midDown = momentumXResidual_(  centralX,                 centralY-0.25*coarseDeltaY, .5*coarseDeltaX, .5*coarseDeltaX, .5*coarseDeltaY, .5*coarseDeltaY, .5*coarseDeltaY);

        std::cout << "momentum x: right up " << rightUp << ", right down = " << rightDown << ", left up " << leftUp  << ", left down = " << leftDown << ", mid up " << midUp << ", mid down = " << midDown << std::endl;
    //y-component of the momentum balance
        Scalar upRight = momentumYResidual_(  centralX+.25*coarseDeltaX, centralY+.5*coarseDeltaY,  .5*coarseDeltaY,    coarseDeltaY, .5*coarseDeltaX, .5*coarseDeltaX, .5*coarseDeltaX);
        Scalar upLeft = momentumYResidual_(   centralX-.25*coarseDeltaX, centralY+.5*coarseDeltaY,  .5*coarseDeltaY,    coarseDeltaY, .5*coarseDeltaX, .5*coarseDeltaX, .5*coarseDeltaX);
        Scalar midRight = momentumYResidual_( centralX+.25*coarseDeltaX, centralY,                  .5*coarseDeltaY, .5*coarseDeltaY, .5*coarseDeltaX, .5*coarseDeltaX, .5*coarseDeltaX);
        Scalar midLeft = momentumYResidual_(  centralX-.25*coarseDeltaX, centralY,                  .5*coarseDeltaY, .5*coarseDeltaY, .5*coarseDeltaX, .5*coarseDeltaX, .5*coarseDeltaX);
        Scalar downRight = momentumYResidual_(centralX+.25*coarseDeltaX, centralY-.5*coarseDeltaY,     coarseDeltaY, .5*coarseDeltaY, .5*coarseDeltaX, .5*coarseDeltaX, .5*coarseDeltaX);
        Scalar downLeft = momentumYResidual_( centralX-.25*coarseDeltaX, centralY-.5*coarseDeltaY,     coarseDeltaY, .5*coarseDeltaY, .5*coarseDeltaX, .5*coarseDeltaX, .5*coarseDeltaX);

        std::cout << "momentum y: up right " << upRight << ", up left = " << upLeft << ", mid right " << midRight << ", mid left " << midLeft << ", downRight " << downRight << ", downLeft " << downLeft << std::endl;

    /////
        std::cout << "grading in x momentumXResiduals: ";
    //x
    // -------------------------------------
    // |           |     |     |           |
    // |           |     |     |           |
    // |           +     +     +           |
    // |           |     |     |           |
    // |           |     |     |           |
    // ------+llllllll+mmmmm+rrrrrrrr+------
    // |     l     |  *  |  *  |     r     |
    // |     l     |  *  |  *  |     r     |
    // +     o     +  o  +  o  +     o     +
    // |     l     |  *  |  *  |     r     |
    // |     l     |  *  |  *  |     r     |
    // ------+llllllll+mmmmm+rrrrrrrr+------
    // |           |     |     |           |
    // |           |     |     |           |
    // |           +     +     +           |
    // |           |     |     |           |
    // |           |     |     |           |
    // -------------------------------------
    // l: control volume named left grading
    // r: control volume named right grading
    // m: control volume named mid grading
    // *: overlap of mid/left or mid/right control volume boundary
    // +: relevant velocity dof positions
    // o: relevant pressure dof positions
        Scalar rightGrading = momentumXResidual_(centralX+.5*coarseDeltaX, centralY, .5*coarseDeltaX, coarseDeltaX, coarseDeltaY, coarseDeltaY, coarseDeltaY);
        Scalar leftGrading = momentumXResidual_(centralX-.5*coarseDeltaX, centralY, coarseDeltaX, .5*coarseDeltaX, coarseDeltaY, coarseDeltaY, coarseDeltaY);
        Scalar midGrading = momentumXResidual_(centralX, centralY, .5*coarseDeltaX, .5*coarseDeltaX, coarseDeltaY, coarseDeltaY, coarseDeltaY);

        std::cout << "momentum x: right " << rightGrading <<  ", left " << leftGrading << ", mid " << midGrading << std::endl;

    //y
    // -------------------------------------
    // |           |     |     |           |
    // |           |     |     |           |
    // |           +luolu+ruoru+           |
    // |           l     *     r           |
    // |           u     *     u           |
    // ------------l-----*-----r------------
    // |           u     *     u           |
    // |           l     *     r           |
    // +           +*****+*****+           +
    // |           l     *     r           |
    // |           d     *     d           |
    // ------------l-----*-----r------------
    // |           d     *     d           |
    // |           l     *     r           |
    // |           +ldold+rdord+           |
    // |           |     |     |           |
    // |           |     |     |           |
    // -------------------------------------
    // lu: control volume named left up grading
    // ru: control volume named right up grading
    // ld: control volume named left down grading
    // rd: control volume named right down grading
    // *: overlap of mid/left or mid/right control volume boundary
    // +: relevant velocity dof positions
    // o: relevant pressure dof positions
        Scalar rightUpGrading = momentumYResidual_(centralX+.25*coarseDeltaX, centralY+.5*coarseDeltaY, coarseDeltaY, coarseDeltaY, coarseDeltaX,  .5*coarseDeltaX,  .5*coarseDeltaX);
        Scalar leftUpGrading = momentumYResidual_(centralX-.25*coarseDeltaX, centralY+.5*coarseDeltaY, coarseDeltaY, coarseDeltaY, .5*coarseDeltaX,  .5*coarseDeltaX,  coarseDeltaX);
        Scalar rightDownGrading = momentumYResidual_(centralX+.25*coarseDeltaX, centralY-.5*coarseDeltaY, coarseDeltaY, coarseDeltaY, coarseDeltaX,  .5*coarseDeltaX,  .5*coarseDeltaX);
        Scalar leftDownGrading = momentumYResidual_(centralX-.25*coarseDeltaX, centralY-.5*coarseDeltaY, coarseDeltaY, coarseDeltaY, .5*coarseDeltaX,  .5*coarseDeltaX,  coarseDeltaX);

        std::cout << "momentum y: right up " << rightUpGrading << ", left up " << leftUpGrading << ", right down " << rightDownGrading << ", left down " << leftDownGrading << std::endl;

    /////
        std::cout << "grading in neighbor in y momentumXResiduals: ";
    //x
        Scalar rightNeighborGrading = momentumXResidual_(centralX+.5*coarseDeltaX, centralY, coarseDeltaX, coarseDeltaX, 2.*coarseDeltaY,  coarseDeltaY,  coarseDeltaY);
        Scalar leftNeighborGrading = momentumXResidual_(centralX-.5*coarseDeltaX, centralY, coarseDeltaX, coarseDeltaX, 2.*coarseDeltaY,  coarseDeltaY,  coarseDeltaY);

        std::cout << "momentum y: rightNeighborGrading " << rightNeighborGrading << ", leftNeighborGrading " << leftNeighborGrading << std::endl;
    /////
        std::cout << "perfect interpolation, everything central and symmetric:";
    //x
        Scalar rightUpPerfectIterpSymm = momentumXResidual_(   centralX + 5./8*coarseDeltaX, centralY+0.25*coarseDeltaY, 0.75*coarseDeltaX, 0.75*coarseDeltaX, .5*coarseDeltaY, .5*coarseDeltaY, .5*coarseDeltaY);
        Scalar rightDownPerfectIterpSymm = momentumXResidual_( centralX + 5./8*coarseDeltaX, centralY-0.25*coarseDeltaY, 0.75*coarseDeltaX, 0.75*coarseDeltaX, .5*coarseDeltaY, .5*coarseDeltaY, .5*coarseDeltaY);
        Scalar leftUpPerfectIterpSymm = momentumXResidual_(    centralX - 5./8*coarseDeltaX, centralY+0.25*coarseDeltaY, 0.75*coarseDeltaX, 0.75*coarseDeltaX, .5*coarseDeltaY, .5*coarseDeltaY, .5*coarseDeltaY);
        Scalar leftDownPerfectIterpSymm = momentumXResidual_(  centralX - 5./8*coarseDeltaX, centralY-0.25*coarseDeltaY, 0.75*coarseDeltaX, 0.75*coarseDeltaX, .5*coarseDeltaY, .5*coarseDeltaY, .5*coarseDeltaY);
        Scalar midUpPerfectIterpSymm = momentumXResidual_(     centralX,                     centralY+0.25*coarseDeltaY, 0.5*coarseDeltaX,  0.5*coarseDeltaX,  .5*coarseDeltaY, .5*coarseDeltaY, .5*coarseDeltaY);
        Scalar midDownPerfectIterpSymm = momentumXResidual_(   centralX,                     centralY-0.25*coarseDeltaY, 0.5*coarseDeltaX,  0.5*coarseDeltaX,  .5*coarseDeltaY, .5*coarseDeltaY, .5*coarseDeltaY);

        std::cout << "momentum x: right up " << rightUpPerfectIterpSymm << ", right down = "  << rightDownPerfectIterpSymm <<  ", left up " << leftUpPerfectIterpSymm << ", left down = " << leftDownPerfectIterpSymm << ", mid up " << midUpPerfectIterpSymm << ", mid down = " << midDownPerfectIterpSymm << std::endl;

    //y
        Scalar upRightPerfectIterpSymm = momentumYResidual_(  centralX+.25*coarseDeltaX, centralY+5./8*coarseDeltaY,  0.75*coarseDeltaY,    0.75*coarseDeltaY, .5*coarseDeltaX, .5*coarseDeltaX, .5*coarseDeltaX);
        Scalar upLeftPerfectIterpSymm = momentumYResidual_(   centralX-.25*coarseDeltaX, centralY+5./8*coarseDeltaY,  0.75*coarseDeltaY,    0.75*coarseDeltaY, .5*coarseDeltaX, .5*coarseDeltaX, .5*coarseDeltaX);
        Scalar midRightPerfectIterpSymm = momentumYResidual_( centralX+.25*coarseDeltaX, centralY,                      .5*coarseDeltaY,      .5*coarseDeltaY, .5*coarseDeltaX, .5*coarseDeltaX, .5*coarseDeltaX);
        Scalar midLeftPerfectIterpSymm = momentumYResidual_(  centralX-.25*coarseDeltaX, centralY,                      .5*coarseDeltaY,      .5*coarseDeltaY, .5*coarseDeltaX, .5*coarseDeltaX, .5*coarseDeltaX);
        Scalar downRightPerfectIterpSymm = momentumYResidual_(centralX+.25*coarseDeltaX, centralY-5./8*coarseDeltaY,  0.75*coarseDeltaY,    0.75*coarseDeltaY, .5*coarseDeltaX, .5*coarseDeltaX, .5*coarseDeltaX);
        Scalar downLeftPerfectIterpSymm = momentumYResidual_( centralX-.25*coarseDeltaX, centralY-5./8*coarseDeltaY,  0.75*coarseDeltaY,    0.75*coarseDeltaY, .5*coarseDeltaX, .5*coarseDeltaX, .5*coarseDeltaX);

        std::cout << "momentum y: up right " << upRightPerfectIterpSymm << ", up left = " << upLeftPerfectIterpSymm << ", mid right " << midRightPerfectIterpSymm << ", mid left " << midLeftPerfectIterpSymm << ", downRight " << downRightPerfectIterpSymm << ", downLeft " << downLeftPerfectIterpSymm << std::endl;

        return;
    }*/

    std::shared_ptr<const Problem> problem_;
};
} // end namespace Dumux

#endif
