// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 3 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \ingroup TwoPModel
 * \brief Performs the transfer of data on a grid from before to after adaptation.
 */

#ifndef DUMUX_TWOP_GRIDDATA_TRANSFER_HH
#define DUMUX_TWOP_GRIDDATA_TRANSFER_HH

#include <memory>

#include <dune/grid/common/partitionset.hh>
#include <dune/grid/utility/persistentcontainer.hh>

#include <dumux/common/properties.hh>

#include <dumux/discretization/method.hh>
#include <dumux/adaptive/griddatatransfer.hh>

namespace Dumux {

/*!
 * \ingroup TwoPModel
 * \brief Class performing the transfer of data on a grid from before to after adaptation.
 */
template<class TypeTag, class SolutionVector>
class FreeFlowGridDataTransfer : public GridDataTransfer<GetPropType<TypeTag, Properties::Grid>>
{
    using Grid = GetPropType<TypeTag, Properties::Grid>;
    using ParentType = GridDataTransfer<Grid>;
    using Scalar = GetPropType<TypeTag, Properties::Scalar>;
    using Problem = GetPropType<TypeTag, Properties::Problem>;
    using GridGeometry = GetPropType<TypeTag, Properties::GridGeometry>;
    using GridView = typename GridGeometry::GridView;
    using FVElementGeometry = typename GridGeometry::LocalView;
    using SubControlVolume = typename FVElementGeometry::SubControlVolume;
    using GridVariables = GetPropType<TypeTag, Properties::GridVariables>;
    using VolumeVariables = GetPropType<TypeTag, Properties::VolumeVariables>;
    using PrimaryVariables = GetPropType<TypeTag, Properties::PrimaryVariables>;
    using Element = typename Grid::template Codim<0>::Entity;
    using FluidSystem = GetPropType<TypeTag, Properties::FluidSystem>;
    using ModelTraits = GetPropType<TypeTag, Properties::ModelTraits>;
    using Indices = typename ModelTraits::Indices;

public:
    /*!
     * \brief Constructor
     *
     * \param problem The DuMuX problem to be solved
     * \param gridGeometry The finite volume grid geometry
     * \param gridVariables The secondary variables on the grid
     * \param sol The solution (primary variables) on the grid
     */
    FreeFlowGridDataTransfer(std::shared_ptr<const Problem> problem,
                         std::shared_ptr<GridGeometry> gridGeometry,
                         std::shared_ptr<const GridVariables> gridVariables,
                         SolutionVector& sol)
    : ParentType()
    , problem_(problem)
    , gridGeometry_(gridGeometry)
    , gridVariables_(gridVariables)
    , sol_(sol)
    {}

    /*!
     * \brief Stores primary variables and additional data
     *
     * To reconstruct the solution in father elements, problem properties might
     * need to be accessed. From upper level on downwards, the old solution is stored
     * into a container object, before the grid is adapted. Father elements hold averaged
     * information from the son cells for the case of the sons being coarsened.
     */
    void store(const Grid& grid) override
    {}

    /*!
     * \brief Reconstruct missing primary variables (where elements are created/deleted)
     *
     * To reconstruct the solution in father elements, problem properties might
     * need to be accessed.
     * Starting from the lowest level, the old solution is mapped on the new grid:
     * Where coarsened, new cells get information from old father element.
     * Where refined, a new solution is reconstructed from the old father cell,
     * and then a new son is created. That is then stored into the general data
     * structure (AdaptedValues).
     */
    void reconstruct(const Grid& grid) override
    {}

private:
    std::shared_ptr<const Problem> problem_;
    std::shared_ptr<GridGeometry> gridGeometry_;
    std::shared_ptr<const GridVariables> gridVariables_;
    SolutionVector& sol_;
};

} // end namespace Dumux

#endif /* DUMUX_TWOP_GRIDDATA_TRANSFER_HH */
