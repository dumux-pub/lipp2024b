// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \ingroup StaggeredDiscretization
 */
#ifndef DUMUX_RES_CALC_GENERIC_METHODS_HH
#define DUMUX_RES_CALC_GENERIC_METHODS_HH

#include <algorithm>
#include <type_traits>

#include <dumux/discretization/staggered/freeflow/mystaggeredgeometryhelper.hh>

namespace Dumux {
    void printDirNondir(std::ofstream& stream, double dir1, double nonDir1, double dir2, double nonDir2, double value, unsigned int dirIdx)
    {
        if (dirIdx == 0)
            stream << dir1 << "," << nonDir1 << "," << dir2 << "," << nonDir2 << "," << std::setprecision(10) << value << std::endl;
        else
            stream << nonDir1 << "," << dir1 << "," << nonDir2 << "," << dir2 << "," << std::setprecision(10) << value << std::endl;
    }

    void printPrimal(std::ofstream& stream, double dir1, double nonDir1, double dir2, double nonDir2, double value)
    {
        stream << dir1 << "," << nonDir1 << "," << dir2 << "," << nonDir2 << "," << std::setprecision(10) << value << std::endl;
    }

    template<class GlobalPosition, class LambdaBC>
    void projectionCVOptionDOnCoarseFunc (double xMin,
        double xMax,
        double yMin,
        double yMax,
        const GlobalPosition& globalPos,
        std::ofstream& stream,
        unsigned int dirIdx,
        const LambdaBC& fineToCoarseInside,
        double value)
    {
        double dirMin, dirMax, nonDirMin, nonDirMax;

        if (dirIdx == 0)
        {
            dirMin = xMin;
            dirMax = xMax;
            nonDirMin = yMin;
            nonDirMax = yMax;
        }
        else
        {
            dirMin = yMin;
            dirMax = yMax;
            nonDirMin = xMin;
            nonDirMax = xMax;
        }

        if (fineToCoarseInside(dirIdx, globalPos, dirMin, dirMax) && (scalarCmp(std::fmod(0.5*(dirMin+dirMax),2.*(dirMax-dirMin)),dirMax-dirMin) || scalarCmp(std::fmod(0.5*(dirMin+dirMax),2.*(dirMax-dirMin)),-dirMax+dirMin)))
        {
            printDirNondir(stream,dirMin,nonDirMin,.5*(dirMin+dirMax),nonDirMax,value,dirIdx);
            printDirNondir(stream,.5*(dirMin+dirMax),nonDirMin,dirMax,nonDirMax,value,dirIdx);
        }
        else
        {
            printDirNondir(stream,dirMin,nonDirMin,dirMax,nonDirMax,value,dirIdx);
        }
    }

    template<class GlobalPosition, class LambdaC, class LambdaD, class LambdaE, class LambdaF>
    void projectionCoarseOnCVOptionDFunc(double xMin,
        double xMax,
        double yMin,
        double yMax,
        const GlobalPosition& globalPos,
        std::ofstream& stream,
        unsigned int dirIdx,
        const LambdaC& coarseToFineInside,
        const LambdaD& coarseToFineLeft,
        const LambdaE& coarseToFineRight,
        const LambdaF& coarseToFineRightEdgeDualGrid,
        double value)
    {
        double dirMin, dirMax, nonDirMin, nonDirMax;

        if (dirIdx == 0)
        {
            dirMin = xMin;
            dirMax = xMax;
            nonDirMin = yMin;
            nonDirMax = yMax;
        }
        else
        {
            dirMin = yMin;
            dirMax = yMax;
            nonDirMin = xMin;
            nonDirMax = xMax;
        }

        if (coarseToFineLeft(dirIdx, globalPos, dirMin, dirMax))//dirMax, yC im refinementbereich; dirMin, yC nicht
        {
            //left or down transitional
            printDirNondir(stream,dirMin,nonDirMin,dirMin+0.75*(dirMax-dirMin),.5*(nonDirMin+nonDirMax),value,dirIdx);
            printDirNondir(stream,dirMin,.5*(nonDirMin+nonDirMax),dirMin+0.75*(dirMax-dirMin),nonDirMax,value,dirIdx);
            printDirNondir(stream,dirMin+0.75*(dirMax-dirMin),nonDirMin,dirMax,.5*(nonDirMin+nonDirMax),value,dirIdx);
            printDirNondir(stream,dirMin+0.75*(dirMax-dirMin),.5*(nonDirMin+nonDirMax),dirMax,nonDirMax,value,dirIdx);
        }
        else if (coarseToFineInside(dirIdx, globalPos, dirMin, dirMax) || coarseToFineRightEdgeDualGrid(dirIdx, globalPos, dirMin, dirMax))//dirMin,yC und dirMax, yC im refinementbereich
        {
            //standard
            printDirNondir(stream,dirMin,nonDirMin,dirMin+0.25*(dirMax-dirMin),.5*(nonDirMin+nonDirMax),value,dirIdx);
            printDirNondir(stream,dirMin,.5*(nonDirMin+nonDirMax),dirMin+0.25*(dirMax-dirMin),nonDirMax,value,dirIdx);
            printDirNondir(stream,dirMin+0.25*(dirMax-dirMin),nonDirMin,dirMin+0.75*(dirMax-dirMin),.5*(nonDirMin+nonDirMax),value,dirIdx);
            printDirNondir(stream,dirMin+0.25*(dirMax-dirMin),.5*(nonDirMin+nonDirMax),dirMin+0.75*(dirMax-dirMin),nonDirMax,value,dirIdx);
            printDirNondir(stream,dirMin+0.75*(dirMax-dirMin),nonDirMin,dirMax,.5*(nonDirMin+nonDirMax),value,dirIdx);
            printDirNondir(stream,dirMin+0.75*(dirMax-dirMin),.5*(nonDirMin+nonDirMax),dirMax,nonDirMax,value,dirIdx);

            if (coarseToFineRightEdgeDualGrid(dirIdx, globalPos, dirMin, dirMax))
            {
                printDirNondir(stream,dirMax,nonDirMin,dirMax+0.25*(dirMax-dirMin),.5*(nonDirMin+nonDirMax),0.,dirIdx);
                printDirNondir(stream,dirMax,.5*(nonDirMin+nonDirMax),dirMax+0.25*(dirMax-dirMin),nonDirMax,0.,dirIdx);
            }
        }
        else if (coarseToFineRight(dirIdx, globalPos, dirMin, dirMax))//dirMin, yC im refinementbereich; dirMax, yC nicht
        {
            //right transitional
            printDirNondir(stream,dirMax-(dirMax-dirMin),nonDirMin,dirMax-0.75*(dirMax-dirMin),.5*(nonDirMin+nonDirMax),value,dirIdx);
            printDirNondir(stream,dirMax-(dirMax-dirMin),.5*(nonDirMin+nonDirMax),dirMax-0.75*(dirMax-dirMin),nonDirMax,value,dirIdx);
            printDirNondir(stream,dirMax-0.75*(dirMax-dirMin),nonDirMin,dirMax,.5*(nonDirMin+nonDirMax),value,dirIdx);
            printDirNondir(stream,dirMax-0.75*(dirMax-dirMin),.5*(nonDirMin+nonDirMax),dirMax,nonDirMax,value,dirIdx);
        }
        else
        {
            printDirNondir(stream,dirMin,nonDirMin,dirMax,nonDirMax,value,dirIdx);
        }
    }

    template<class GlobalPosition, class LambdaC>
    void projectionCoarseOnFineFunc(double xMin,
        double xMax,
        double yMin,
        double yMax,
        const GlobalPosition& globalPos,
        std::ofstream& stream,
        const LambdaC& coarseToFineInside,
        double value)
    {
        if (coarseToFineInside(globalPos, xMin, xMax))
        {
            printPrimal(stream,xMin,yMin,.5*(xMin+xMax),.5*(yMin+yMax),value);
            printPrimal(stream,.5*(xMin+xMax),yMin,xMax,.5*(yMin+yMax),value);
            printPrimal(stream,xMin,.5*(yMin+yMax),.5*(xMin+xMax),yMax,value);
            printPrimal(stream,.5*(xMin+xMax),.5*(yMin+yMax),xMax,yMax,value);
        }
        else
        {
            printPrimal(stream,xMin,yMin,xMax,yMax,value);
        }
    }
}  // namespace Dumux
#endif
