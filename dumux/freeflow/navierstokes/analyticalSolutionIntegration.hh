// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 3 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \ingroup NavierStokesModel
 */
#ifndef DUMUX_NAVIERSTOKES_ANALYTICAL_SOLUTION_INTEGRATION_HH
#define DUMUX_NAVIERSTOKES_ANALYTICAL_SOLUTION_INTEGRATION_HH

#include <dune/geometry/quadraturerules.hh>
#include <dumux/io/grid/controlvolumegrids.hh>

namespace Dumux {
template<class TypeTag, class MLGTraits>
class AnalyticalSolutionIntegration
{
    using GridGeometry = GetPropType<TypeTag, Properties::GridGeometry>;
    using GridView = typename GridGeometry::GridView;

    using Scalar = GetPropType<TypeTag, Properties::Scalar>;

    using FVElementGeometry = typename GridGeometry::LocalView;
    using SubControlVolume = typename FVElementGeometry::SubControlVolume;
    using SubControlVolumeFace = typename FVElementGeometry::SubControlVolumeFace;

    enum {
        dimWorld = GridView::dimensionworld
      };

    using GlobalPosition = typename SubControlVolumeFace::GlobalPosition;

public:
    /*!
     * \brief The Constructor
     */
    AnalyticalSolutionIntegration(unsigned int quadOrder)
    : quadOrder_(quadOrder)
    {}

    template<class Lambda, class Geometry>
    Scalar lineIntegral (const Geometry& geo, const Lambda& solLambdaFunction) const
    {
        return integral_<Dune::QuadratureRules<Scalar, 1/*line*/>>(geo, solLambdaFunction);
    }

    template<class Lambda, class Geometry>
    Scalar rectangleIntegral (const Geometry& geo, const Lambda& solLambdaFunction) const
    {
        return integral_<Dune::QuadratureRules<Scalar, 2/*rectangle*/>>(geo, solLambdaFunction);
    }

    auto getLineGeometry(const SubControlVolumeFace& scvf, const GridGeometry& gridGeometry) const
    {
        if(containerCmp(scvf.center(), scvf.corner(0)))
            return getGeometryAndVolumeVirtualParallelFaceDofPos_(scvf, scvf.corner(0), gridGeometry);
        else if (containerCmp(scvf.center(), scvf.corner(1)))
            return getGeometryAndVolumeVirtualParallelFaceDofPos_(scvf, scvf.corner(1), gridGeometry);
        else
            return scvf.geometry();
    }

    auto getLineGeometry(const std::array<GlobalPosition, 2>& corners) const
    {
        using LineCorners = typename MLGTraits::template CornerStorage<1/*line dimension*/, dimWorld/*coordinate dimension*/>::Type;

        LineCorners corners_;
        if (corners_.size() != (1<<(1/*line dimension*/)))
            DUNE_THROW(Dune::InvalidStateException, "");

        corners_[0] = corners[0];
        corners_[1] = corners[1];

        const auto& lineGeometryType = Dune::GeometryTypes::simplex(1);
        using LineGeometry = typename Dune::MultiLinearGeometry<Scalar, 1/*line dimension*/, dimWorld/*cooordinate dimension*/, MLGTraits>;

        return LineGeometry(lineGeometryType, corners_);
    }

    auto getRectangleGeometry(const SubControlVolumeFace& scvf, const GridGeometry& gridGeometry) const
    {
        const auto& averagingVolumeCorners = localCVVertices<GlobalPosition>(scvf, gridGeometry, false/*hangingNodesAreCVCenters*/);

        using RectangleCorners = typename MLGTraits::template CornerStorage<2/*rectangle dimension*/, dimWorld/*coordinate dimension*/>::Type;

        RectangleCorners corners_;
        if (corners_.size() != (1<<(2/*rectangle dimension*/)))
            DUNE_THROW(Dune::InvalidStateException, "");

        for (unsigned int i = 0; i < corners_.size(); ++i)
            corners_[i] = averagingVolumeCorners[i];

        const auto& rectangleGeometryType = Dune::GeometryTypes::cube(2);
        using RectangleGeometry = typename Dune::MultiLinearGeometry<Scalar, 2/*rectangle dimension*/, dimWorld/*cooordinate dimension*/, MLGTraits>;

        return RectangleGeometry(rectangleGeometryType, corners_);
    }

    static auto getRectangleGeometry(const SubControlVolume& scv, const GridGeometry& gridGeometry)
    {
        return scv.geometry();
    }

private:
    template<class QuadratureRuleType, class Lambda, class Geometry>
    auto integral_ (const Geometry& geo, const Lambda& solLambdaFunction) const
    {
        auto volume = geo.volume();

        Scalar retVal = 0.0;
        const auto& quad = QuadratureRuleType::rule(geo.type(), quadOrder_);
        for (auto&& qp : quad)
        {
            const auto w = qp.weight()*geo.integrationElement(qp.position());
            const auto globalPos = geo.global(qp.position());
            const auto sol = solLambdaFunction(globalPos);
            retVal += sol*w;
        }
        retVal /= volume;
        return retVal;
    }

    auto getGeometryAndVolumeVirtualParallelFaceDofPos_ (const SubControlVolumeFace& scvf, const GlobalPosition& thisCorner, const GridGeometry& gridGeometry) const
    {
        unsigned int dirIdx = scvf.directionIndex();
        unsigned int nonDirIdx = (dirIdx == 0) ? 1 : 0;

        SubControlVolumeFace normalFaceThisCorner = scvf;

        const SubControlVolumeFace& normalFacePair0 = gridGeometry.scvf(scvf.insideScvIdx(), scvf.localNormalFaceBuildingIdx(0));
        const SubControlVolumeFace& normalFacePair1 = gridGeometry.scvf(scvf.insideScvIdx(), scvf.localNormalFaceBuildingIdx(1));

        if (scalarCmp(thisCorner[nonDirIdx], normalFacePair0.center()[nonDirIdx]))
        {
            normalFaceThisCorner = normalFacePair0;
        }
        else if (scalarCmp(thisCorner[nonDirIdx], normalFacePair1.center()[nonDirIdx]))
        {
            normalFaceThisCorner = normalFacePair1;
        }
        else
        {
            DUNE_THROW(Dune::InvalidStateException, "");
        }

        std::array<GlobalPosition,4> boundaryFullCVCorners = localCVVertices<GlobalPosition>(normalFaceThisCorner, gridGeometry, false/*hangingNodesAreCVCenters*/);

        using LineCorners = typename MLGTraits::template CornerStorage<1/*line dimension*/, dimWorld/*coordinate dimension*/>::Type;

        LineCorners corners_;
        if (corners_.size() != (1<<1/*line dimension*/))
            DUNE_THROW(Dune::InvalidStateException, "");

        bool alreadySetFirstLineForAveragingCornersEntry = false;
        for (const auto& corner : boundaryFullCVCorners)
        {
            if (scalarCmp(corner[dirIdx],scvf.center()[dirIdx]))
            {
                if (!alreadySetFirstLineForAveragingCornersEntry)
                {
                    corners_[0]=corner;
                    alreadySetFirstLineForAveragingCornersEntry = true;
                }
                else
                {
                    corners_[1]=corner;
                }
            }
        }

        const auto& lineGeometryType = Dune::GeometryTypes::simplex(1);
        using LineGeometry = typename Dune::MultiLinearGeometry<Scalar, 1/*line dimension*/, dimWorld/*cooordinate dimension*/, MLGTraits>;

        return LineGeometry(lineGeometryType, corners_);
    }

    unsigned int quadOrder_;
};
} // end namespace Dumux

#endif
