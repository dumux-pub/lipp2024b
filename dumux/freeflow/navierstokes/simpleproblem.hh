// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 3 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \ingroup NavierStokesModel
 * \copydoc Dumux::SimpleNavierStokesProblem
 */
#ifndef DUMUX_NAVIERSTOKES_SIMPLE_PROBLEM_HH
#define DUMUX_NAVIERSTOKES_SIMPLE_PROBLEM_HH

#include <dune/common/exceptions.hh>
#include <dune/common/typetraits.hh>
#include <dumux/common/properties.hh>
#include <dumux/common/staggeredfvproblem.hh>
#include <dumux/discretization/method.hh>
#include <dumux/discretization/staggered/freeflow/simplesubcontrolvolumeface.hh>
#include <dumux/freeflow/navierstokes/problem.hh>

#include <dumux/freeflow/navierstokes/analyticalSolutionIntegration.hh>

namespace Dumux {
/*!
 * \ingroup NavierStokesModel
 * \brief Navier-Stokes problem base class.
 *
 * This implements gravity (if desired) and a function returning the temperature.
 * Includes a specialized method used only by the staggered grid discretization.
 *
 */
template<class TypeTag>
class SimpleNavierStokesProblem : public NavierStokesParentProblem<TypeTag>
{
    using ParentType = NavierStokesParentProblem<TypeTag>;
    using Implementation = GetPropType<TypeTag, Properties::Problem>;

    using ModelTraits = GetPropType<TypeTag, Properties::ModelTraits>;
    using GridGeometry = GetPropType<TypeTag, Properties::GridGeometry>;
    using GridView = typename GridGeometry::GridView;
    using Element = typename GridView::template Codim<0>::Entity;
    using IndexType = typename GridView::IndexSet::IndexType;

    using GridVariables = GetPropType<TypeTag, Properties::GridVariables>;
    using GridFaceVariables = typename GridVariables::GridFaceVariables;
    using ElementFaceVariables = typename GridFaceVariables::LocalView;
    using FaceVariables = typename GridFaceVariables::FaceVariables;
    using GridVolumeVariables = typename GridVariables::GridVolumeVariables;
    using ElementVolumeVariables = typename GridVolumeVariables::LocalView;
    using Scalar = GetPropType<TypeTag, Properties::Scalar>;

    using FVElementGeometry = typename GridGeometry::LocalView;
    using SubControlVolume = typename FVElementGeometry::SubControlVolume;
    using SubControlVolumeFace = typename FVElementGeometry::SubControlVolumeFace;
    using PrimaryVariables = GetPropType<TypeTag, Properties::PrimaryVariables>;
    using Indices = typename GetPropType<TypeTag, Properties::ModelTraits>::Indices;
    using SolutionVector = GetPropType<TypeTag, Properties::SolutionVector>;
    using FaceSolutionVector = GetPropType<TypeTag, Properties::FaceSolutionVector>;
    using CellCenterSolutionVector = GetPropType<TypeTag, Properties::CellCenterSolutionVector>;

    enum {
        dim = GridView::dimension,
        dimWorld = GridView::dimensionworld
      };

    using GlobalPosition = typename SubControlVolumeFace::GlobalPosition;
    using VelocityVector = Dune::FieldVector<Scalar, dimWorld>;
    using GravityVector = Dune::FieldVector<Scalar, dimWorld>;

    static constexpr unsigned int upwindSchemeOrder = GridGeometry::upwindSchemeOrder;
    using MLGTraits = typename SimpleFreeFlowStaggeredDefaultScvfGeometryTraits<GridView, upwindSchemeOrder>::template ScvfMLGTraits<Scalar> ;

public:
    std::vector<IndexType> dirichletBoundaryScvfsIndexSet() const
    {
        std::vector<IndexType> vec;
        const auto boundaryScvfsIndexSet = (this->gridGeometry()).boundaryScvfsIndexSet();
        for (auto& scvfIdx : boundaryScvfsIndexSet)
        {
            const auto scvf = (this->gridGeometry()).boundaryScvf(scvfIdx);
            const auto bcTypes = asImp_().boundaryTypesAtPos(scvf.center());
            if (bcTypes.isDirichlet(Indices::velocity(scvf.directionIndex())))
            {
                vec.push_back(scvfIdx);
            }
        }
        return vec;
    }

    std::vector<IndexType> fixedPressureScvsIndexSet() const
    {
        std::vector<IndexType> vec;

        const auto boundaryScvsIndexSet = (this->gridGeometry()).boundaryScvsIndexSet();
        for (auto& scvIdx : boundaryScvsIndexSet)
        {
            const auto scv = (this->gridGeometry()).scv(scvIdx);
            const auto& element = (this->gridGeometry()).element(scv.elementIndex());
            auto fvGeometry = localView(this->gridGeometry());
            fvGeometry.bindElement(element);

            if (asImp_().isDirichletCell(element, fvGeometry, scv, Indices::pressureIdx))
            {
                vec.push_back(scv.dofIndex());
            }
        }

        return vec;
    }

    /*!
     * \brief The constructor
     * \param gridGeometry The finite volume grid geometry
     * \param paramGroup The parameter group in which to look for runtime parameters first (default is "")
     */
    SimpleNavierStokesProblem(std::shared_ptr<const GridGeometry> gridGeometry, const std::string& paramGroup = "")
    : ParentType(gridGeometry, paramGroup)
    , gravity_(0.0)
    {
        if (getParamFromGroup<bool>(paramGroup, "Problem.EnableGravity"))
            gravity_[dim-1]  = -9.81;

        enableInertiaTerms_ = getParamFromGroup<bool>(paramGroup, "Problem.EnableInertiaTerms");
        useScvfLineAveraging_ = getParam<bool>("Problem.UseScvfLineAveraging", false);
        useScvfCVAveraging_ = getParam<bool>("Problem.UseScvfCVAveraging", false);
        useScvVolumeAveraging_ = getParam<bool>("Problem.UseScvVolumeAveraging", false);
        useContiSourceAveraging_ = getParam<bool>("Problem.UseContiSourceAveraging", false);
        quadOrder_ = getParam<unsigned int>("Problem.QuadratureOrder", 3);
    }

    bool useScvfLineAveraging() const
    {
        return useScvfLineAveraging_;
    }

    bool useScvfCVAveraging() const
    {
        return useScvfCVAveraging_;
    }

    bool useScvVolumeAveraging() const
    {
        return useScvVolumeAveraging_;
    }

    bool useContiSourceAveraging() const
    {
        return useContiSourceAveraging_;
    }

    unsigned int quadOrder() const
    {
        return quadOrder_;
    }

    /*!
     * \brief Returns the temperature \f$\mathrm{[K]}\f$ at a given global position.
     *
     * This is not specific to the discretization. By default it just
     * calls temperature().
     *
     * \param globalPos The position in global coordinates where the temperature should be specified.
     */
    Scalar temperatureAtPos(const GlobalPosition &globalPos) const
    { return asImp_().temperature(); }

   /*!
     * \brief Return the analytical solution of the problem at a given position
     *
     * \param globalPos The global position
     */
    Scalar instationaryAnalyticalPressureSolutionAtPos(const SubControlVolume& scv, const Scalar time) const
    {
        AnalyticalSolutionIntegration<TypeTag, MLGTraits> analyticalSolInt(quadOrder_);
        auto rectangleGeo = analyticalSolInt.getRectangleGeometry(scv, this->gridGeometry());

        if (useScvVolumeAveraging_)
            return analyticalSolInt.rectangleIntegral(rectangleGeo, [&](const GlobalPosition& globalPos) { return asImp_().instationaryAnalyticalSolutionAtPos(globalPos, time)[Indices::pressureIdx]; });
        else
            return asImp_().instationaryAnalyticalSolutionAtPos(scv.dofPosition(), time)[Indices::pressureIdx];
    }

    /*!
     * \brief Return the analytical solution of the problem at a scvf
     */
    Scalar instationaryAnalyticalVelocitySolutionAtPos(const SubControlVolumeFace& scvf, const Scalar time) const
    {
        unsigned int dirIdx = scvf.directionIndex();
        return instationaryAnalyticalVelocitySolutionAtPos_(scvf, time, dirIdx);
    }

    /*!
     * \brief Return the analytical solution of the problem at a scvf
     */
    Scalar instationaryAnalyticalNonDirVelocitySolutionAtPos(const SubControlVolumeFace& scvf, const Scalar time) const
    {
        unsigned int nonDirIdx = (scvf.directionIndex() == 0)?1:0;
        return instationaryAnalyticalVelocitySolutionAtPos_(scvf, time, nonDirIdx);
    }

    /*!
     * \brief Return the analytical solution of the problem at a given position
     *
     * \param globalPos The global position
     */
    Scalar analyticalPressureSolutionAtPos(const SubControlVolume& scv) const
    {
        AnalyticalSolutionIntegration<TypeTag, MLGTraits> analyticalSolInt(quadOrder_);
        auto rectangleGeo = analyticalSolInt.getRectangleGeometry(scv, this->gridGeometry());
        if (useScvVolumeAveraging_)
            return analyticalSolInt.rectangleIntegral(rectangleGeo, [&](const GlobalPosition& globalPos) { return asImp_().analyticalSolutionAtPos(globalPos)[Indices::pressureIdx]; });
        else
            return asImp_().analyticalSolutionAtPos(scv.dofPosition())[Indices::pressureIdx];
    }

    /*!
     * \brief Return the analytical solution of the problem at a given position
     *
     * \param globalPos The global position
     */
    Scalar analyticalVelocitySolutionAtPos(const SubControlVolumeFace& scvf) const
    {
        unsigned int dirIdx = scvf.directionIndex();
        return analyticalVelocitySolutionAtPos_(scvf, dirIdx);
    }

    /*!
     * \brief Return the analytical solution of the problem at a given position
     *
     * \param globalPos The global position
     */
    Scalar analyticalNonDirVelocitySolutionAtPos(const SubControlVolumeFace& scvf) const
    {
        unsigned int nonDirIdx = (scvf.directionIndex() == 0)?1:0;
        return analyticalVelocitySolutionAtPos_(scvf, nonDirIdx);
    }

    /*!
     * \brief Returns the temperature within the domain.
     *
     * This method MUST be overwritten by the actual problem.
     */
    Scalar temperature() const
    { DUNE_THROW(Dune::NotImplemented, "temperature() method not implemented by the actual problem"); }

    /*!
     * \brief Returns the acceleration due to gravity.
     *
     * If the <tt>Problem.EnableGravity</tt> parameter is true, this means
     * \f$\boldsymbol{g} = ( 0,\dots,\ -9.81)^T \f$, else \f$\boldsymbol{g} = ( 0,\dots, 0)^T \f$
     */
    const GravityVector& gravity() const
    { return gravity_; }

    /*!
     * \brief Returns whether interia terms should be considered.
     */
    bool enableInertiaTerms() const
    { return enableInertiaTerms_; }

    //! Applys the initial face solution (velocities on the faces). Specialization for staggered grid discretization.
    template <class SolutionVector, class G = GridGeometry>
    typename std::enable_if<G::discMethod == DiscretizationMethods::staggered, void>::type
    applyInitialFaceSolution(SolutionVector& sol,
                             const SubControlVolumeFace& scvf,
                             const PrimaryVariables& initSol) const
    {
        sol[GridGeometry::faceIdx()][scvf.dofIndex()][0] = initSol[Indices::velocity(scvf.directionIndex())];
    }

    /*!
     * \brief An additional drag term can be included as source term for the momentum balance
     *        to mimic 3D flow behavior in 2D:
     *  \f[
     *        f_{drag} = -(8 \mu / h^2)v
     *  \f]
     *  Here, \f$h\f$ corresponds to the extruded height that is
     *  bounded by the imaginary walls. See Flekkoy et al. (1995) \cite flekkoy1995a<BR>
     *  A value of 8.0 is used as a default factor, corresponding
     *  to the velocity profile at  the center plane
     *  of the virtual height (maximum velocity). Setting this value to 12.0 corresponds
     *  to an depth-averaged velocity (Venturoli and Boek, 2006) \cite venturoli2006a.
     */
    Scalar pseudo3DWallFriction(const Scalar velocity,
                                const Scalar viscosity,
                                const Scalar height,
                                const Scalar factor = 8.0) const
    {
        static_assert(dim == 2, "Pseudo 3D wall friction may only be used in 2D");
        return -factor * velocity * viscosity / (height*height);
    }

    //! Convenience function for staggered grid implementation.
    template <class ElementVolumeVariables, class ElementFaceVariables, class G = GridGeometry>
    typename std::enable_if<G::discMethod == DiscretizationMethods::staggered, Scalar>::type
    pseudo3DWallFriction(const SubControlVolumeFace& scvf,
                         const ElementVolumeVariables& elemVolVars,
                         const ElementFaceVariables& elemFaceVars,
                         const Scalar height,
                         const Scalar factor = 8.0) const
    {
        const Scalar velocity = elemFaceVars[scvf].velocitySelf();
        const Scalar viscosity = elemVolVars[scvf.insideScvIdx()].effectiveViscosity();
        return pseudo3DWallFriction(velocity, viscosity, height, factor);
    }

    /*!
     * \brief Returns the intrinsic permeability of required as input parameter for the Beavers-Joseph-Saffman boundary condition
     *
     * This member function must be overloaded in the problem implementation, if the BJS boundary condition is used.
     */
    Scalar permeability(const Element& element, const SubControlVolumeFace& scvf) const
    {
        DUNE_THROW(Dune::NotImplemented, "When using the Beavers-Joseph-Saffman boundary condition, the permeability must be returned in the acutal problem");
    }

    /*!
     * \brief Returns the alpha value required as input parameter for the Beavers-Joseph-Saffman boundary condition
     *
     * This member function must be overloaded in the problem implementation, if the BJS boundary condition is used.
     */
    Scalar alphaBJ(const SubControlVolumeFace& scvf) const
    {
        DUNE_THROW(Dune::NotImplemented, "When using the Beavers-Joseph-Saffman boundary condition, the alpha value must be returned in the acutal problem");
    }

    /*!
     * \brief Returns the beta value which is the alpha value divided by the square root of the (scalar-valued) interface permeability.
     */
    Scalar betaBJ(const Element& element, const SubControlVolumeFace& scvf, const GlobalPosition& tangentialVector) const
    {
        const Scalar interfacePermeability = interfacePermeability_(element, scvf, tangentialVector);
        using std::sqrt;
        return asImp_().alphaBJ(scvf) / sqrt(interfacePermeability);
    }

    /*!
     * \brief Returns the beta value which is the alpha value divided by the square root of the (scalar-valued) interface permeability.
     */
    [[deprecated("Use betaBJ with tangential vector instead. Will be removed after 3.3")]]
    Scalar betaBJ(const Element& element, const SubControlVolumeFace& scvf) const
    {
        using std::sqrt;
        return asImp_().alphaBJ(scvf) / sqrt(asImp_().permeability(element, scvf));
    }

    /*!
     * \brief Returns the velocity in the porous medium (which is 0 by default according to Saffmann).
     */
    VelocityVector porousMediumVelocity(const Element& element, const SubControlVolumeFace& scvf) const
    {
        return VelocityVector(0.0);
    }


    /*!
     * \brief Returns the slip velocity at a porous boundary based on the Beavers-Joseph(-Saffman) condition.
     */
    const Scalar beaversJosephVelocity(const Element& element,
                                       const SubControlVolume& scv,
                                       const SubControlVolumeFace& ownScvf,
                                       const SubControlVolumeFace& faceOnPorousBoundary,
                                       const Scalar velocitySelf,
                                       const Scalar tangentialVelocityGradient) const
    {
        // create a unit normal vector oriented in positive coordinate direction
        GlobalPosition orientation = ownScvf.unitOuterNormal();
        orientation[ownScvf.directionIndex()] = 1.0;

        // du/dy + dv/dx = alpha/sqrt(K) * (u_boundary-uPM)
        // beta = alpha/sqrt(K)
        const Scalar betaBJ = asImp_().betaBJ(element, faceOnPorousBoundary, orientation);
        const Scalar distanceNormalToBoundary = (faceOnPorousBoundary.center() - scv.center()).two_norm();

        return (tangentialVelocityGradient*distanceNormalToBoundary
              + asImp_().porousMediumVelocity(element, faceOnPorousBoundary) * orientation * betaBJ * distanceNormalToBoundary
              + velocitySelf) / (betaBJ*distanceNormalToBoundary + 1.0);
    }

    void createAnalyticalSolution(Scalar time)
    {
        auto instationaryAnalyticalVelocitySolutionAtPosLambdaFunction = [&](const SubControlVolumeFace& scvf) { return asImp_().instationaryAnalyticalVelocitySolutionAtPos(scvf, time); };
        createAnalyticalSolution_(
            [&](const SubControlVolume& scv) { return asImp_().instationaryAnalyticalPressureSolutionAtPos(scv, time); },
            instationaryAnalyticalVelocitySolutionAtPosLambdaFunction,
            [&](const FVElementGeometry& fvGeometry) { return possiblyInstationaryAnalyticalVectorialVelocitySolutionAtGridCellCenterLambdaFunction_(fvGeometry, instationaryAnalyticalVelocitySolutionAtPosLambdaFunction); });
    }

    void createAnalyticalSolution()
    {
        auto analyticalVelocitySolutionAtPosLambdaFunction = [&](const SubControlVolumeFace& scvf) { return asImp_().analyticalVelocitySolutionAtPos(scvf); };
        createAnalyticalSolution_(
            [&](const SubControlVolume& scv) { return asImp_().analyticalPressureSolutionAtPos(scv); },
            analyticalVelocitySolutionAtPosLambdaFunction,
            [&](const FVElementGeometry& fvGeometry) { return possiblyInstationaryAnalyticalVectorialVelocitySolutionAtGridCellCenterLambdaFunction_(fvGeometry, analyticalVelocitySolutionAtPosLambdaFunction); });
    }

    auto getAnalyticalSolution(Scalar time) const
    {
        auto instationaryAnalyticalVelocitySolutionAtPosLambdaFunction = [&](const SubControlVolumeFace& scvf) { return asImp_().instationaryAnalyticalVelocitySolutionAtPos(scvf, time); };
        return getAnalyticalSolution_(
            [&](const SubControlVolume& scv) { return asImp_().instationaryAnalyticalPressureSolutionAtPos(scv, time); },
            instationaryAnalyticalVelocitySolutionAtPosLambdaFunction,
            [&](const FVElementGeometry& fvGeometry) { return possiblyInstationaryAnalyticalVectorialVelocitySolutionAtGridCellCenterLambdaFunction_(fvGeometry, instationaryAnalyticalVelocitySolutionAtPosLambdaFunction); });
    }

    auto getAnalyticalSolution() const
    {
        auto analyticalVelocitySolutionAtPosLambdaFunction = [&](const SubControlVolumeFace& scvf) { return asImp_().analyticalVelocitySolutionAtPos(scvf); };
        return getAnalyticalSolution_(
            [&](const SubControlVolume& scv) { return asImp_().analyticalPressureSolutionAtPos(scv); },
            analyticalVelocitySolutionAtPosLambdaFunction,
            [&](const FVElementGeometry& fvGeometry) { return possiblyInstationaryAnalyticalVectorialVelocitySolutionAtGridCellCenterLambdaFunction_(fvGeometry, analyticalVelocitySolutionAtPosLambdaFunction); });
    }

    /*!
     * \brief Returns the analytical solution for the pressure
     */
    auto& getAnalyticalPressureSolution() const
    {
        return analyticalPressure_;
    }

   /*!
     * \brief Returns the analytical solution for the velocity
     */
    auto& getAnalyticalVelocitySolution() const
    {
        return analyticalVelocity_;
    }

   /*!
     * \brief Returns the analytical solution for the velocity at the faces
     */
    auto& getAnalyticalVelocitySolutionOnFace() const
    {
        return analyticalVelocityOnFace_;
    }

    auto& getAnalyticalFaceSolutionVector() const
    {
        return analyticalFaceSolutionVector_;
    }

private:
    template <class Lambda>
    VelocityVector possiblyInstationaryAnalyticalVectorialVelocitySolutionAtGridCellCenterLambdaFunction_(const FVElementGeometry& fvGeometry, const Lambda& possiblyInstationaryAnalyticalVelocitySolutionAtPosLambdaFunction) const
    {
        VelocityVector analyticalVelocitySolAtCC;
        VelocityVector scvfAreas;

        // velocities on faces
        for (auto&& scvf : scvfs(fvGeometry))
        {
            analyticalVelocitySolAtCC[Indices::velocity(scvf.directionIndex())] += possiblyInstationaryAnalyticalVelocitySolutionAtPosLambdaFunction(scvf)*scvf.area();
            scvfAreas[Indices::velocity(scvf.directionIndex())] += scvf.area();
        }

        for(int dirIdx = 0; dirIdx < dimWorld; ++dirIdx)
            analyticalVelocitySolAtCC[dirIdx] /= scvfAreas[dirIdx];

        return analyticalVelocitySolAtCC;
    }

    /*!
     * \brief Return the analytical solution of the problem at a given position
     *
     * \param globalPos The global position
     */
    Scalar analyticalVelocitySolutionAtPos_(const SubControlVolumeFace& scvf, unsigned int dirIdx) const
    {
        AnalyticalSolutionIntegration<TypeTag, MLGTraits> analyticalSolInt(quadOrder_);
        if (useScvfLineAveraging_ || (useScvfCVAveraging_ && scvf.boundary()))
        {
            auto lineGeo = analyticalSolInt.getLineGeometry(scvf, this->gridGeometry());
            return analyticalSolInt.lineIntegral(lineGeo, [&](const GlobalPosition& globalPos) { return asImp_().analyticalSolutionAtPos(globalPos)[Indices::velocity(dirIdx)]; });
        }
        else if (useScvfCVAveraging_ && !scvf.boundary())
        {
            auto rectangleGeo = analyticalSolInt.getRectangleGeometry(scvf, this->gridGeometry());
            return analyticalSolInt.rectangleIntegral(rectangleGeo, [&](const GlobalPosition& globalPos) { return asImp_().analyticalSolutionAtPos(globalPos)[Indices::velocity(dirIdx)]; });
        }
        else
            return asImp_().analyticalSolutionAtPos(scvf.center())[Indices::velocity(dirIdx)];
    }

    Scalar instationaryAnalyticalVelocitySolutionAtPos_(const SubControlVolumeFace& scvf, const Scalar time, unsigned int dirIdx) const
    {
        AnalyticalSolutionIntegration<TypeTag, MLGTraits> analyticalSolInt(quadOrder_);
        if (useScvfLineAveraging_ || (useScvfCVAveraging_ && scvf.boundary()))
        {
            auto lineGeo = analyticalSolInt.getLineGeometry(scvf, this->gridGeometry());
            return analyticalSolInt.lineIntegral(lineGeo, [&](const GlobalPosition& globalPos) { return asImp_().instationaryAnalyticalSolutionAtPos(globalPos, time)[Indices::velocity(dirIdx)]; });
        }
        else if (useScvfCVAveraging_ && !scvf.boundary())
        {
            auto rectangleGeo = analyticalSolInt.getRectangleGeometry(scvf, this->gridGeometry());
            return analyticalSolInt.rectangleIntegral(rectangleGeo, [&](const GlobalPosition& globalPos) { return asImp_().instationaryAnalyticalSolutionAtPos(globalPos, time)[Indices::velocity(dirIdx)]; });
        }
        else
            return asImp_().instationaryAnalyticalSolutionAtPos(scvf.center(), time)[Indices::velocity(dirIdx)];
    }

    /*!
     * \brief Adds additional VTK output data to the VTKWriter. Function is called by the output module on every write.
     */
    template <class LambdaA, class LambdaB, class LambdaC>
    void createAnalyticalSolution_(const LambdaA& instationaryAnalyticalPressureSolutionAtPosLambdaFunction,
                                  const LambdaB& instationaryAnalyticalVelocitySolutionAtPosLambdaFunction,
                                  const LambdaC& instationaryAnalyticalVectorialVelocitySolutionAtGridCellCenterLambdaFunction)
    {
        analyticalPressure_.resize(this->gridGeometry().numCellCenterDofs());
        analyticalVelocity_.resize(this->gridGeometry().numCellCenterDofs());
        analyticalVelocityOnFace_.resize(this->gridGeometry().numFaceDofs());
        analyticalFaceSolutionVector_.resize(this->gridGeometry().numFaceDofs());

        for (const auto& element : elements(this->gridGeometry().gridView()))
        {
            auto fvGeometry = localView(this->gridGeometry());
            fvGeometry.bindElement(element);
            for (auto&& scv : scvs(fvGeometry))
            {
                // velocities on faces
                for (auto&& scvf : scvfs(fvGeometry))
                {
                    const auto analyticalSolutionAtFace = instationaryAnalyticalVelocitySolutionAtPosLambdaFunction(scvf);
                    analyticalVelocityOnFace_[scvf.dofIndex()][scvf.directionIndex()] = analyticalSolutionAtFace;
                    analyticalFaceSolutionVector_[scvf.dofIndex()] = analyticalSolutionAtFace;
                }

                analyticalPressure_[scv.dofIndex()] = instationaryAnalyticalPressureSolutionAtPosLambdaFunction(scv);

                analyticalVelocity_[scv.dofIndex()] = instationaryAnalyticalVectorialVelocitySolutionAtGridCellCenterLambdaFunction(fvGeometry);
            }
        }
     }

     /*!
     * \brief Adds additional VTK output data to the VTKWriter. Function is called by the output module on every write.
     */
    template <class LambdaA, class LambdaB, class LambdaC>
    auto getAnalyticalSolution_(const LambdaA& instationaryAnalyticalPressureSolutionAtPosLambdaFunction,
                                  const LambdaB& instationaryAnalyticalVelocitySolutionAtPosLambdaFunction,
                                  const LambdaC& instationaryAnalyticalVectorialVelocitySolutionAtGridCellCenterLambdaFunction) const
    {
        CellCenterSolutionVector analyticalPressure;
        std::vector<VelocityVector> analyticalVelocity;
        std::vector<VelocityVector> analyticalVelocityOnFace;
        FaceSolutionVector analyticalFaceSolutionVector;

        analyticalPressure.resize(this->gridGeometry().numCellCenterDofs());
        analyticalVelocity.resize(this->gridGeometry().numCellCenterDofs());
        analyticalVelocityOnFace.resize(this->gridGeometry().numFaceDofs());
        analyticalFaceSolutionVector.resize(this->gridGeometry().numFaceDofs());

        for (const auto& element : elements(this->gridGeometry().gridView()))
        {
            auto fvGeometry = localView(this->gridGeometry());
            fvGeometry.bindElement(element);
            for (auto&& scv : scvs(fvGeometry))
            {
                // velocities on faces
                for (auto&& scvf : scvfs(fvGeometry))
                {
                    const auto analyticalSolutionAtFace = instationaryAnalyticalVelocitySolutionAtPosLambdaFunction(scvf);
                    analyticalVelocityOnFace[scvf.dofIndex()][scvf.directionIndex()] = analyticalSolutionAtFace;
                    analyticalFaceSolutionVector[scvf.dofIndex()] = analyticalSolutionAtFace;
                }

                analyticalPressure[scv.dofIndex()] = instationaryAnalyticalPressureSolutionAtPosLambdaFunction(scv);

                analyticalVelocity[scv.dofIndex()] = instationaryAnalyticalVectorialVelocitySolutionAtGridCellCenterLambdaFunction(fvGeometry);
            }
        }

        return std::make_tuple(analyticalPressure, analyticalVelocity, analyticalVelocityOnFace, analyticalFaceSolutionVector);
     }

    //! Returns a scalar permeability value at the coupling interface
    Scalar interfacePermeability_(const Element& element, const SubControlVolumeFace& scvf, const GlobalPosition& tangentialVector) const
    {
        const auto& K = asImp_().permeability(element, scvf);

        // use t*K*t for permeability tensors
        if constexpr (Dune::IsNumber<std::decay_t<decltype(K)>>::value)
            return K;
        else
            return vtmv(tangentialVector, K, tangentialVector);
    }

    //! Returns the implementation of the problem (i.e. static polymorphism)
    Implementation &asImp_()
    { return *static_cast<Implementation *>(this); }

    //! \copydoc asImp_()
    const Implementation &asImp_() const
    { return *static_cast<const Implementation *>(this); }

    GravityVector gravity_;
    bool enableInertiaTerms_;

    CellCenterSolutionVector analyticalPressure_;
    std::vector<VelocityVector> analyticalVelocity_;
    std::vector<VelocityVector> analyticalVelocityOnFace_;
    FaceSolutionVector analyticalFaceSolutionVector_;

    bool useScvfLineAveraging_;
    bool useScvfCVAveraging_;
    bool useScvVolumeAveraging_;
    bool useContiSourceAveraging_;
    unsigned int quadOrder_;
};

} // end namespace Dumux

#endif
