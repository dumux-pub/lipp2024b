// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \ingroup TwoPModel
 * \brief Class defining a standard, analytical solution dependent indicator for grid adaptation
 */

#ifndef DUMUX_FREEFLOW_MY_ADAPTION_INDICATOR_HH
#define DUMUX_FREEFLOW_MY_ADAPTION_INDICATOR_HH

#include <memory>
#include <dune/common/exceptions.hh>
#include <dune/grid/common/partitionset.hh>

#include <dumux/common/properties.hh>
#include <dumux/common/parameters.hh>
#include <dumux/discretization/elementsolution.hh>
#include <dumux/discretization/staggered/freeflow/mystaggeredgeometryhelper.hh>

namespace Dumux {

/*!\ingroup TwoPModel
 * \brief  Class defining a standard, analytical solution dependent indicator for grid adaptation
 */
template<class TypeTag>
class MyFreeflowGridAdaptIndicator
{
    using GridGeometry = GetPropType<TypeTag, Properties::GridGeometry>;
    using GridView = typename GetPropType<TypeTag, Properties::GridGeometry>::GridView;
    using Element = typename GridView::template Codim<0>::Entity;
    using Scalar = GetPropType<TypeTag, Properties::Scalar>;
    using Indices = typename GetPropType<TypeTag, Properties::ModelTraits>::Indices;
    using SolutionVector = GetPropType<TypeTag, Properties::SolutionVector>;
    using CellCenterPrimaryVariables = GetPropType<TypeTag, Properties::CellCenterPrimaryVariables>;
    using FVElementGeometry = typename GridGeometry::LocalView;
    using Problem = GetPropType<TypeTag, Properties::Problem>;
    using GlobalPosition = typename Element::Geometry::GlobalCoordinate;

    static constexpr int dimWorld = GridView::dimensionworld;

    static constexpr auto pressureIdx = Indices::pressureIdx;

    static constexpr auto cellCenterIdx = GridGeometry::cellCenterIdx();
    static constexpr auto faceIdx = GridGeometry::faceIdx();

public:
    /*! \brief The Constructor
     *
     * \param gridGeometry The finite volume grid geometry
     * \param paramGroup The parameter group in which to look for runtime parameters first (default is "")
     *
     *  Note: refineBound_, coarsenBound_, relativeError_, & maxVelocityDelta_ are chosen
     *        in a way such that the indicator returns false for all elements
     *        before having been calculated.
     */
    MyFreeflowGridAdaptIndicator(std::shared_ptr<const Problem> problem, const std::string& paramGroup = "")
    : problem_(problem)
    , refineBound_(std::numeric_limits<Scalar>::max())
    , relativeError_(problem_->gridGeometry().numCellCenterDofs(), 0.0)
    , minLevel_(getParamFromGroup<std::size_t>(paramGroup, "Adaptive.MinLevel", 0))
    , maxLevel_(getParamFromGroup<std::size_t>(paramGroup, "Adaptive.MaxLevel", 0))
    {}

    /*!
     * \brief Function to set the minimum allowed level.
     *
     * \param minLevel The minimum level
     */
    void setMinLevel(std::size_t minLevel)
    {
        minLevel_ = minLevel;
    }

    /*!
     * \brief Function to set the maximum allowed level.
     *
     *\param maxLevel The maximum level
     */
    void setMaxLevel(std::size_t maxLevel)
    {
        maxLevel_ = maxLevel;
    }

    /*!
     * \brief Function to set the minumum/maximum allowed levels.
     *
     * \param minLevel The minimum level
     * \param maxLevel The maximum level
     */
    void setLevels(std::size_t minLevel, std::size_t maxLevel)
    {
        minLevel_ = minLevel;
        maxLevel_ = maxLevel;
    }

    /*!
     * \brief Calculates the indicator used for refinement/coarsening for each grid cell.
     *
     * \param sol The solution vector
     * \param refineTol The refineTol worst percent of the elements are refined
     *
     *  This standard two-phase indicator is based on the saturation gradient.
     */
    void calculatePressureError(const SolutionVector& sol,
                   Scalar refineTol = 0.05,
                   Scalar coarsenTol = 0.001)
    {
        assert(0 < refineTol && refineTol < 1);

        //! reset the indicator to a state that returns false for all elements
        refineBound_ = std::numeric_limits<Scalar>::max();
        relativeError_.assign(problem_->gridGeometry().gridView().size(0), 0.0);

        //! maxLevel_ must be higher than minLevel_ to allow for refinement
        if (minLevel_ >= maxLevel_)
            return;

        //! check for inadmissible tolerance combination
        if (coarsenTol > refineTol)
            DUNE_THROW(Dune::InvalidStateException, "Refine tolerance must be higher than coarsen tolerance");

//         //! variables to hold the max/mon saturation values on the leaf
//         Scalar globalMax = std::numeric_limits<Scalar>::lowest();
//         Scalar globalMin = std::numeric_limits<Scalar>::max();

        //! Calculate minimum and maximum saturation
        for (const auto& element : elements(problem_->gridGeometry().gridView()))
        {
            //! index of the current leaf-element
            const auto globalIdx = problem_->gridGeometry().elementMapper().index(element);

//             //! obtain the saturation at the center of the element
//             const auto geometry = element.geometry();
//
//             using PrimaryVariables = typename VolumeVariables::PrimaryVariables;
//             const auto& cellCenterPriVars = sol[cellCenterIdx][globalIdx];
//             constexpr auto offset = PrimaryVariables::dimension - CellCenterPrimaryVariables::dimension;
//             PrimaryVariables priVars[pressureIdx + offset] = makePriVarsFromCellCenterPriVars<PrimaryVariables>(cellCenterPriVars);
//             const auto elemSol = elementSolution<FVElementGeometry>(std::move(priVars));
//
//             const Scalar pressure = evalSolution(element, geometry, *gridGeometry_, elemSol, geometry.center())[pressureIdx];

            const Scalar pressure = sol[cellCenterIdx][globalIdx][pressureIdx];

//             //! maybe update the global minimum/maximum
//             using std::min;
//             using std::max;
//             globalMin = min(pressure, globalMin);
//             globalMax = max(pressure, globalMax);

            //! calculate delta in pressure for this cell
            //! obtain analytic pressure
            const Scalar pressureExact = problem_->getAnalyticalPressureSolution()[globalIdx];

            if (pressureExact < std::numeric_limits<Scalar>::lowest())
            {
                relativeError_[globalIdx] = std::abs(pressure - pressureExact); // use the absolute pressure instead of dividing by zero
            }
            else
            {
                relativeError_[globalIdx] = std::abs((pressure - pressureExact) / pressureExact);
            }
        }

        std::vector<Scalar> data(relativeError_.begin(), relativeError_.end());
        const auto midIterator = data.end() - ceil(data.size() * refineTol);
        std::nth_element(data.begin(), midIterator, data.end());
        refineBound_ = data[midIterator - data.begin()];

        //! check if neighbors have to be refined too
        for (const auto& element : elements(problem_->gridGeometry().gridView(), Dune::Partitions::interior))
        {
            if (this->operator()(element) > 0)
            {
                checkNeighborsRefine_(element);
            }

            // we want to have a uniform refinement at the FF-PM interface, otherwise adaptive coupling stencils and refinement in the PM would be necessary
            addInterfaceRefine_(element);
        }
    }


    /*!
     * \brief Calculates the indicator used for refinement/coarsening for each grid cell.
     *
     * \param sol The solution vector
     * \param refineTol The refineTol worst percent of the elements are refined
     *
     *  This standard two-phase indicator is based on the saturation gradient.
     */
    template <class VelocitySolutionVector>
    void calculateVelocityError(const SolutionVector& sol,
                                const VelocitySolutionVector& analyticalVelocitySolution,
                   Scalar refineTol = 0.05,
                   Scalar coarsenTol = 0.001)
    {
        assert(0 < refineTol && refineTol < 1);

        //! reset the indicator to a state that returns false for all elements
        refineBound_ = std::numeric_limits<Scalar>::max();
        relativeError_.assign(problem_->gridGeometry().gridView().size(0), 0.0);

        //! maxLevel_ must be higher than minLevel_ to allow for refinement
        if (minLevel_ >= maxLevel_)
            return;

        //! check for inadmissible tolerance combination
        if (coarsenTol > refineTol)
            DUNE_THROW(Dune::InvalidStateException, "Refine tolerance must be higher than coarsen tolerance");

//         //! variables to hold the max/mon saturation values on the leaf
//         Scalar globalMax = std::numeric_limits<Scalar>::lowest();
//         Scalar globalMin = std::numeric_limits<Scalar>::max();

        //! Calculate minimum and maximum saturation
        for (const auto& element : elements(problem_->gridGeometry().gridView()))
        {
            //! index of the current leaf-element
            const auto globalIdx = problem_->gridGeometry().elementMapper().index(element);

            FVElementGeometry fvGeometry(problem_->gridGeometry());
            fvGeometry.bind(element);

            std::array<Scalar, dimWorld> weightedVelocities = {};
            std::array<Scalar, dimWorld> sumsOfWeights = {};

            for (const auto& scvf : scvfs(fvGeometry))
            {
                weightedVelocities[scvf.directionIndex()] += sol[faceIdx][scvf.dofIndex()] * scvf.area();
                sumsOfWeights[scvf.directionIndex()] += scvf.area();
            }

            Scalar squareMeanVelocity = 0.;

            for (int i=0; i < dimWorld; ++i)
            {
                Scalar meanVelocityInDirection = weightedVelocities[i] / sumsOfWeights[i];
                squareMeanVelocity += meanVelocityInDirection * meanVelocityInDirection;
            }

            Scalar meanVelocity = sqrt(squareMeanVelocity);

            //! calculate delta in velocity for this cell
            //! obtain analytic velocity
            const auto velocityExactDimVector = analyticalVelocitySolution[globalIdx];

            Scalar squareVelocityExact = 0.;

            for(unsigned int i=0; i < dimWorld; ++i)
            {
                squareVelocityExact += velocityExactDimVector[i] * velocityExactDimVector[i];
            }

            const Scalar velocityExact = sqrt(squareVelocityExact);

            if (velocityExact < std::numeric_limits<Scalar>::lowest())
            {
                relativeError_[globalIdx] = std::abs(meanVelocity - velocityExact); // use the absolute velocity error instead of dividing by zero
            }
            else
            {
                relativeError_[globalIdx] = std::abs((meanVelocity - velocityExact) / velocityExact);
            }
        }

        std::vector<Scalar> data(relativeError_.begin(), relativeError_.end());
        const auto midIterator = data.end() - ceil(data.size() * refineTol);
        std::nth_element(data.begin(), midIterator, data.end());
        refineBound_ = data[midIterator - data.begin()];

        //! check if neighbors have to be refined too
        for (const auto& element : elements(problem_->gridGeometry().gridView(), Dune::Partitions::interior))
        {
            if (this->operator()(element) > 0)
            {
                checkNeighborsRefine_(element);
            }

            // we want to have a uniform refinement at the FF-PM interface, otherwise adaptive coupling stencils and refinement in the PM would be necessary
            addInterfaceRefine_(element);
        }
    }

    void prepare(Scalar refineTol = 0.05,
                   Scalar coarsenTol = 0.001)
    {
        assert(0 < refineTol && refineTol < 1);

        //! reset the indicator to a state that returns false for all elements
        refineBound_ = std::numeric_limits<Scalar>::max();
        relativeError_.assign(problem_->gridGeometry().gridView().size(0), 0.0);

        //! maxLevel_ must be higher than minLevel_ to allow for refinement
        if (minLevel_ >= maxLevel_)
            return;

        //! check for inadmissible tolerance combination
        if (coarsenTol > refineTol)
            DUNE_THROW(Dune::InvalidStateException, "Refine tolerance must be higher than coarsen tolerance");

        //! Calculate minimum and maximum saturation
        for (const auto& element : elements(problem_->gridGeometry().gridView()))
        {
            //! index of the current leaf-element
            const auto globalIdx = problem_->gridGeometry().elementMapper().index(element);

            std::vector<Scalar> cornerXCoords;
            cornerXCoords.resize(4);
            std::vector<Scalar> cornerYCoords;
            cornerYCoords.resize(4);

            for (unsigned int i = 0; i < 4; ++i)
            {
                cornerXCoords[i] = element.geometry().corner(i)[0];
                cornerYCoords[i] = element.geometry().corner(i)[1];
            }

            std::sort(cornerXCoords.begin(), cornerXCoords.end());
            std::sort(cornerYCoords.begin(), cornerYCoords.end());

            Scalar xLeft = cornerXCoords[0];
            Scalar xRight = cornerXCoords[3];
            Scalar yDown = cornerYCoords[0];

            Scalar pore1Left = -0.001;
            Scalar pore1Right = 0.001;
            Scalar pore2Left = 0.004;
            Scalar pore2Right = 0.006;
            Scalar pore3Left = 0.009;
            Scalar pore3Right = 0.011;

            if ((xRight > pore1Left && xLeft < pore1Right && yDown < (1e-2 + 1e-10)) ||
                (xRight > pore2Left && xLeft < pore2Right && yDown < (1e-2 + 1e-10)) ||
                (xRight > pore3Left && xLeft < pore3Right && yDown < (1e-2 + 1e-10)))
                relativeError_[globalIdx] = std::numeric_limits<Scalar>::max();
            else
                relativeError_[globalIdx] = std::numeric_limits<Scalar>::min();
        }

        refineBound_ = 1;

        //! check if neighbors have to be refined too
        for (const auto& element : elements(problem_->gridGeometry().gridView(), Dune::Partitions::interior))
        {
            if (this->operator()(element) > 0)
            {
                checkNeighborsRefine_(element);
            }

            // we want to have a uniform refinement at the FF-PM interface, otherwise adaptive coupling stencils and refinement in the PM would be necessary
//             addInterfaceRefine_(element);
        }
    }

    void prepareCenterRefine(Scalar refineTol = 0.05,
                   Scalar coarsenTol = 0.001)
    {
        assert(0 < refineTol && refineTol < 1);

        //! reset the indicator to a state that returns false for all elements
        refineBound_ = std::numeric_limits<Scalar>::max();
        relativeError_.assign(problem_->gridGeometry().gridView().size(0), 0.0);

        //! maxLevel_ must be higher than minLevel_ to allow for refinement
        if (minLevel_ >= maxLevel_)
            return;

        //! check for inadmissible tolerance combination
        if (coarsenTol > refineTol)
            DUNE_THROW(Dune::InvalidStateException, "Refine tolerance must be higher than coarsen tolerance");

        //! Calculate minimum and maximum saturation
        for (const auto& element : elements(problem_->gridGeometry().gridView()))
        {
            //! index of the current leaf-element
            const auto globalIdx = problem_->gridGeometry().elementMapper().index(element);

            auto x = element.geometry().center()[0];
            auto y = element.geometry().center()[1];

            Scalar innerCircle = getParam<Scalar>("Adaptivity.InnerCircle");
            Scalar outerCircle = getParam<Scalar>("Adaptivity.OuterCircle");

            if ((x*x+y*y) > innerCircle*innerCircle && (x*x+y*y < outerCircle*outerCircle))
                relativeError_[globalIdx] = std::numeric_limits<Scalar>::max();
            else
                relativeError_[globalIdx] = std::numeric_limits<Scalar>::min();
        }

        refineBound_ = 1;

        //! check if neighbors have to be refined too
        for (const auto& element : elements(problem_->gridGeometry().gridView(), Dune::Partitions::interior))
        {
            if (this->operator()(element) > 0)
            {
                checkNeighborsRefine_(element);
            }

            // we want to have a uniform refinement at the FF-PM interface, otherwise adaptive coupling stencils and refinement in the PM would be necessary
//             addInterfaceRefine_(element);
        }
    }

    /*! \brief function call operator to return mark
     *
     *  \return  1 if an element should be refined
     *          -1 if an element should be coarsened
     *           0 otherwise
     *
     *  \param element A grid element
     */
    int operator() (const Element& element) const
    {
        if (element.level() < maxLevel_
                 && relativeError_[problem_->gridGeometry().elementMapper().index(element)] > refineBound_)
        {
            return 1;
        }
        else
            return 0;
    }

private:
    /*!
     * \brief Method ensuring the refinement ratio of 2:1
     *
     *  For any given element, a loop over the neighbors checks if the
     *  entities refinement would require that any of the neighbors has
     *  to be refined, too. This is done recursively over all levels of the grid.
     *
     * \param element Element of interest that is to be refined
     * \param level level of the refined element: it is at least 1
     * \return true if everything was successful
     */
    bool checkNeighborsRefine_(const Element &element, std::size_t level = 1)
    {
        std::vector<Element> surroundingElements;
        std::vector<GlobalPosition> surroundingElementCenters;

        for (const auto& intersection : intersections(problem_->gridGeometry().gridView(), element))
        {
            if(!intersection.neighbor())
                continue;

            //direct neighbor
            surroundingElements.push_back(intersection.outside());

            //diagonal neighbor
            for (const auto& outsideIntersection : intersections(problem_->gridGeometry().gridView(), intersection.outside()))
            {
                if (!outsideIntersection.neighbor())
                    continue;

                if (haveCommonCorner(outsideIntersection,element) && !containerCmp(outsideIntersection.geometry().center(),intersection.geometry().center()))
                {
                    if(containerFind(surroundingElementCenters.begin(), surroundingElementCenters.end(), outsideIntersection.outside().geometry().center()) == surroundingElementCenters.end())
                    {
                        surroundingElements.push_back(outsideIntersection.outside());
                        surroundingElementCenters.push_back(outsideIntersection.outside().geometry().center());
                    }
                }
            }
        }

        for (const auto& surroundingElement : surroundingElements)
        {
            // only mark non-ghost elements
            if (surroundingElement.partitionType() == Dune::GhostEntity)
                continue;

            if (surroundingElement.level() < maxLevel_ && surroundingElement.level() < element.level()/*is not yet refined but will be*/)
            {
                // ensure refinement for surroundingElement element
                relativeError_[problem_->gridGeometry().elementMapper().index(surroundingElement)] = std::numeric_limits<Scalar>::max();
                if(level < maxLevel_)
                    checkNeighborsRefine_(surroundingElement, ++level);
            }
        }

        return true;
    }

    bool addInterfaceRefine_(const Element& element)
    {
        FVElementGeometry fvGeometry(problem_->gridGeometry());
        fvGeometry.bind(element);

        for (const auto& scvf : scvfs(fvGeometry))
        {
            const auto bcTypes = problem_->boundaryTypes(element, scvf);

            if (scvf.boundary() && bcTypes.isBeaversJoseph(Indices::momentumXBalanceIdx))
            {
                relativeError_[problem_->gridGeometry().elementMapper().index(element)] = std::numeric_limits<Scalar>::max();
                break;
            }
        }

        return true;
    }

    std::shared_ptr<const Problem> problem_;

    Scalar refineBound_;
    std::vector< Scalar > relativeError_;
    std::size_t minLevel_;
    std::size_t maxLevel_;
};

} // end namespace Dumux

#endif
