SUMMARY
=======
This is the DuMuX module containing the code for producing the results for the journal paper "Capturing local details in fluid-flow simulations: options, challenges and applications using marker-and-cell schemes" of Melanie Lipp, Martin Schneider and Rainer Helmig.

Installation
============

The easiest way to install this module and its dependencies is to create a new
directory and clone this module:

```
mkdir New_Folder
cd New_Folder
git clone https://git.iws.uni-stuttgart.de/dumux-pub/lipp2024b.git
```

After that, execute the file [installLipp2024b.sh](https://git.iws.uni-stuttgart.de/dumux-pub/lipp2024b/-/blob/next/installLipp2024b.sh)

```
cd lipp2024b
git checkout next
cd ..
chmod u+x lipp2024b/installLipp2024b.sh
./lipp2024b/installLipp2024b.sh
```

This should automatically download all necessary modules and check out the correct versions.

Applications
============


__Building from source__:

In order to produce all results navigate to the folder

```bash
cd lipp2024b/build-cmake/appl/
```

And run:

```bash
bash ./produce_data_and_tex_files.sh
```

Results can then be found in the folder `datafiles`.

Particularly, 
- Fig. 5a plots the absolute values of column 5 of `datafiles/localTruncErrExplanation_MomX_WithConserv.csv`
- Fig. 5b plots the absolute values of column 6 of `datafiles/localTruncErrExplanation_MomX_CV_D_StandStenc_NoInterp.csv`
- Fig. 6a plots the absolute values of column 5 of `datafiles/localTruncErrExplanation_MomX_10x10Grid.csv`
- Fig. 6b plots the absolute values of column 5 of `datafiles/localTruncErrExplanation_MomX_13x20Grid.csv`
- Fig. 6c plots the absolute values of column 5 of `datafiles/localTruncErrExplanation_MomX_20x20Grid.csv`
- Fig. 7 plots the absolute values of column 5 of `datafiles/localTruncErrExplanation_MomX_CV_D_StandStenc_NoInterp.csv` minus the absolute values of column 6 of `datafiles/localTruncErrExplanation_MomX_CV_D_StandStenc_NoInterp.csv`
- Fig. 8a plots the positive results of the absolute values of column 5 of `datafiles/localTruncErrExplanation_MomX_CV_D_MyStenc_NoInterp.csv` minus the absolute values of column 5 of `datafiles/localTruncErrExplanation_MomX_CV_D_StandStenc_NoInterp.csv`
- Fig. 8b plots the negative results of the absolute values of column 5 of `datafiles/localTruncErrExplanation_MomX_CV_D_MyStenc_NoInterp.csv` minus the absolute values of column 5 of `datafiles/localTruncErrExplanation_MomX_CV_D_StandStenc_NoInterp.csv` multiplied by minus one
- Fig. 9a plots the positive results of the absolute values of column 5 of `datafiles/localTruncErrExplanation_MomX_WithoutConserv.csv` minus the absolute values of column 5 of `datafiles/localTruncErrExplanation_MomX_CV_D_MyStenc_NoInterp.csv`
- Fig. 9b plots the negative results of the absolute values of column 5 of `datafiles/localTruncErrExplanation_MomX_WithoutConserv.csv` minus the absolute values of column 5 of `datafiles/localTruncErrExplanation_MomX_CV_D_MyStenc_NoInterp.csv` multiplied by minus one
- Fig. 10a plots the positive results of the absolute values of column 5 of `datafiles/localTruncErrExplanation_MomX_WithConserv.csv` minus the absolute values of column 5 of `datafiles/localTruncErrExplanation_MomX_WithoutConserv.csv`
- Fig. 10b plots the negative results of the absolute values of column 5 of `datafiles/localTruncErrExplanation_MomX_WithConserv.csv` minus the absolute values of column 5 of `datafiles/localTruncErrExplanation_MomX_WithoutConserv.csv` multiplied by minus one
- Fig. 11a plots the negative results of the absolute values of column 5 of `datafiles/localTruncErrExplanation_MomX_projCoarseOnCVOptionD_WithConserv.csv` minus the absolute values of column 5 of `datafiles/localTruncErrExplanation_MomX_projectionCoarseOnCVOptionD.csv` multiplied by minus one
- the Fig.-11a caption uses datafiles/Fig11aL1norm.tex
- Fig. 11b plots the positive results of the absolute values of column 5 of `datafiles/localTruncErrExplanation_MomX_projCoarseOnCVOptionD_WithConserv.csv` minus the absolute values of column 5 of `datafiles/localTruncErrExplanation_MomX_projectionCoarseOnCVOptionD.csv`
- the Fig.-11b caption uses datafiles/Fig11bL1norm.tex
- Fig. 12a plots the negative results of the absolute values of column 5 of `datafiles/localTruncErrExplanation_MomX_projCoarseOnCVOptionD_WithConserv_0307.csv` minus the absolute values of column 5 of `datafiles/localTruncErrExplanation_MomX_projectionCoarseOnCVOptionD_0307.csv` multiplied by minus one
- the Fig.-12a caption uses datafiles/Fig12aL1norm.tex
- Fig. 12b plots the positive results of the absolute values of column 5 of `datafiles/localTruncErrExplanation_MomX_projCoarseOnCVOptionD_WithConserv_0307.csv` minus the absolute values of column 5 of `datafiles/localTruncErrExplanation_MomX_projectionCoarseOnCVOptionD_0307.csv`
- the Fig.-12b caption uses datafiles/Fig12bL1norm.tex
- Fig. 13a plots the negative results of the absolute values of column 5 of `datafiles/localTruncErrExplanation_MomX_projCoarseOnCVOptionD_WithConserv_rightFine.csv` minus the absolute values of column 5 of `datafiles/localTruncErrExplanation_MomX_projectionCoarseOnCVOptionD_rightFine.csv` multiplied by minus one
- the Fig.-13a caption uses datafiles/Fig13aL1norm.tex
- Fig. 13b plots the positive results of the absolute values of column 5 of `datafiles/localTruncErrExplanation_MomX_projCoarseOnCVOptionD_WithConserv_rightFine.csv` minus the absolute values of column 5 of `datafiles/localTruncErrExplanation_MomX_projectionCoarseOnCVOptionD_rightFine.csv`
- the Fig.-13b caption uses datafiles/Fig13bL1norm.tex
- Fig. 14a plots the negative results of the absolute values of column 5 of `datafiles/localTruncErrExplanation_MomX_projCoarseOnCVOptionD_WithConserv_100x100.csv` minus the absolute values of column 5 of `datafiles/localTruncErrExplanation_MomX_projectionCoarseOnCVOptionD_100x100.csv` multiplied by minus one
- the Fig.-14a caption uses datafiles/Fig14aL1norm.tex
- Fig. 14b plots the positive results of the absolute values of column 5 of `datafiles/localTruncErrExplanation_MomX_projCoarseOnCVOptionD_WithConserv_100x100.csv` minus the absolute values of column 5 of `datafiles/localTruncErrExplanation_MomX_projectionCoarseOnCVOptionD_100x100.csv`
- the Fig.-14b caption uses datafiles/Fig14bL1norm.tex
- Fig. 15a plots the negative results of the absolute values of column 5 of `datafiles/localTruncErrExplanation_MomX_projCoarseOnCVOptionD_WithConserv_gauss.csv` minus the absolute values of column 5 of `datafiles/localTruncErrExplanation_MomX_projectionCoarseOnCVOptionD_gauss.csv` multiplied by minus one
- the Fig.-15a caption uses datafiles/Fig15aL1norm.tex
- Fig. 15b plots the positive results of the absolute values of column 5 of `datafiles/localTruncErrExplanation_MomX_projCoarseOnCVOptionD_WithConserv_gauss.csv` minus the absolute values of column 5 of `datafiles/localTruncErrExplanation_MomX_projectionCoarseOnCVOptionD_gauss.csv`
- the Fig.-15b caption uses datafiles/Fig15bL1norm.tex
- Fig. 16 plots the absolute values of the difference of column 5 of `datafiles/solErr_u.csv` minus column 5 of `datafiles/solErr_uExact.csv` 
- Fig. 17a plots the negative results of the absolute values of the difference of column 5 of `datafiles/solErr_donea_U_10x10_projCVOptDOnCoarse_0406refined.csv` minus column 5 of `datafiles/solErr_donea_Uexact_10x10_projCVOptDOnCoarse_0406refined.csv` minus the absolute values of the difference of column 5 of `datafiles/solErr_donea_U_10x10_projCoarseOnCVOptD_0406unrefined.csv` minus column 5 of `datafiles/solErr_donea_Uexact_10x10_projCoarseOnCVOptD_0406unrefined.csv` multiplied by minus one
- the Fig.-17a caption uses `datafiles/Fig17aL1norm.tex`
- Fig. 17b plots the positive results of the absolute values of the difference of column 5 of `datafiles/solErr_donea_U_10x10_projCVOptDOnCoarse_0406refined.csv` minus column 5 of `datafiles/solErr_donea_Uexact_10x10_projCVOptDOnCoarse_0406refined.csv` minus the absolute values of the difference of column 5 of `datafiles/solErr_donea_U_10x10_projCoarseOnCVOptD_0406unrefined.csv` minus column 5 of `datafiles/solErr_donea_Uexact_10x10_projCoarseOnCVOptD_0406unrefined.csv`
- the Fig.-17b caption uses `datafiles/Fig17bL1norm.tex`
- the additional note at the very end of Chaption 3.2 uses `100x100_decrease_L1norm.tex` and `100x100_increase_L1norm.tex`
- Fig. 18 uses `datafiles/pnm_unrefined_velocities_stokes.csv` and `datafiles/pnm_unrefined_velocities_pnm.csv` for the plotted data (square roots of the sums of column 5 squared and column 6 squared) and `datafiles/pnm_unrefined_pressures.csv` and `gridPlotfileB='datafiles/pnm_pore_grid.csv` for the plotted grids
- Fig. 19 uses `datafiles/pnm_refined_velocities_stokes.csv` and `datafiles/pnm_refined_velocities_pnm.csv` for the plotted data (square roots of the sums of column 5 squared and column 6 squared) and `datafiles/pnm_refined_pressures.csv` and `gridPlotfileB='datafiles/pnm_pore_grid.csv` for the plotted grids
- Fig. 20 uses `datafiles/localTruncErrExplanation_Conti_proj.csv`
- Tab. 1 uses `Tab1.tex`
- Tab. 2 uses `Tab2.tex`
- Tab. 3 uses `Tab3.tex`